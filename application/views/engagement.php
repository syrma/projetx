<!DOCTYPE html>
<?php include 'header.php';  ?>
<script src="<?php echo base_url();?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/autoNumeric.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($) {
    $('.auto').autoNumeric('init');
});
</script>

    <div class="container">
     	  <div>
     	  	<h3 class="page-header">Les Engagements</h3>
     	  </div>	
 	          <?php include 'menu_engagement.php';  ?>			
			<div class="col-md-9"> 	
			     <div class="panel panel-success ">
				        <div class="panel-heading">
					           <h3 class="panel-title">Budget Initial</h3>
				        </div>
				     <div class="panel-body">
                            <div class="row">

                                <div class="col-md-8 col-md-offset-2">
                                    <?php if ($etat==null ||$etat[0]->budget_initial=="0000" ) { ?>
                                    <form action="Engagement/init" method="post" accept-charset="utf-8">

                                        <div class="input-group">
                                            <span class="input-group-addon">Budget initial</span>
                                            <input type="text" class="form-control auto" name="budget" id="budget" name ="BI" placeholder="budget initial" data-a-sep=" " data-a-dec="," data-a-sign="D.A ">
                                        </div>
                                        <button type="submit" class="btn btn-top btn-success col-md-6 col-md-offset-3 btn-flat">Entrer</button>
                                    </form>
                                    <?php } else {?>
                                    <!-- form -->
                                    <form action="Engagement/modifier" method="post" accept-charset="utf-8">
                                    
					                   <div class="input-group">
                                            <span class="input-group-addon">Budget initial</span>
                                            <input type="text" class="form-control auto" name="budget" id="budget" name ="BI" value="<?php if ($etat!=null) echo $etat[0]->budget_initial ?>" data-a-sep=" " data-a-dec="," data-a-sign="D.A ">
                                        </div>
                                    <button type="submit" class="btn btn-top btn-success col-md-6 col-md-offset-3 btn-flat">modifier</button>
                                    </form>
                                    <?php } ?>
                                    <!-- /form --> 
                                    </div>
                              </div>
                                    <!-- progressbar -->
                                <?php if($etat!=null) {?>
                                <div class="row">
                                      <div class="col-md-3">
                                           
                                           <span for="rs"> Budget initial : </span> <span id="initial" class="label label-default"><?php echo $etat[0]->budget_initial?></span>
                                           <span for="rs"> Budget reste :</span> <span id="final" class="label label-success"><?php echo $budget ?></span>

                                       </div>  
                                     <div class="col-md-9">
                                             <div id="progress-bar"  class="progress">
                                                    <div id="myBar" class="progress-bar progress-bar-success progress-bar-striped " role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style='...'>
                                                        <span id="label" class="bb">90%</span>
                                                    </div>       
                                            </div>
                                     </div>  
                              </div>
                              <!-- /progressbar -->
                         <?php } ?>

			          </div>
     	  	       </div>
            </div>
    <?php
    $msg = $this->session->flashdata('msg')['msg'];
    echo $msg ?>
        </div>




    <?php include 'footer.php';
    ?>

