<!DOCTYPE html>
<?php include 'header.php';  ?>
<div class="container">
    <div>
        <h3 class="page-header">Gestion des Chapitres et Articles</h3>
    </div>
    <?php include 'menu_engagement.php';  ?>
    <script src="<?php echo base_url();?>js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/autoNumeric.js" type="text/javascript"></script>
    <script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init');
    });
    </script>
    <div class="col-md-9">
        <div class="panel panel-success ">
            <div class="panel-heading">
                <h3 class="panel-title">Gestion des Articles</h3>
            </div>
            <div class="panel-body">
                <h3>Ajouter Article :</h3>
                <div class="row">
                    <form action="ajouterarticle/ajouter?idc=<?php echo $idc ?>" method="post" accept-charset="utf-8">
                        <div class="col-md-3">
                            <div class="input-group ">
                                <span class="input-group-addon">Code</span>
                                <input type="nom" class="form-control" id="ida" name="ida" placeholder="code ">
                            </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group input-group-right ">
                                    <span class="input-group-addon">Budget</span>
                                    <input type="text" class="form-control auto" id="budget" name="budget" placeholder="budget de l'article " data-a-sep=" " data-a-dec="," data-a-sign="D.A ">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Nom</span>
                                    <input type="BI" class="form-control" id="nom" name="nom" placeholder="Nom de L'article">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button onclick="f()" type="submit" class="btn btn-success btn-flat ">Ajouter Article</button>
                            </div>
                    </form>
                    </div>
                    <!-- row/ -->
                </div>
                <!-- panel-body/ -->
            </div>
            <!-- success/ -->
        </div>
        <?php
    $msg = $this->session->flashdata('msg')['msg'];
    echo $msg ?>
            <!-- 9/ -->
 </div>
 
<!-- container/ -->
<script src="<?php echo base_url();?>js/ajoutarticle.js"></script>
<?php include 'footer.php';
?>
