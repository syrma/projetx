<!DOCTYPE html>
<?php include 'header.php';  ?>

<div class="container">
    <div>
        <h3 class="page-header">Modifier Fournisseur</h3>
        <div class=" col-md-8 col-md-offset-2">
            <form role="form" method="post" action="modifierfournisseur/modifier?id=<?php echo $id ?>">

                <div class="form-group">
                    <label for="nom">Nom:</label>
                    <input type="text" class="form-control" name="nom" id="nom" value="<?php echo $fournisseur[0]->nom?>">
                </div>
                <div class="form-group">
                    <label for="prenom">Adresse:</label>
                    <input type="text" class="form-control" name="adr" id="adresse" value="<?php echo $fournisseur[0]->adr?>">
                </div>
                <div class="form-group">
                    <label for="nomb">Nom de la Banque:</label>
                    <input type="text" class="form-control" name="nomb" id="nomb" value="<?php echo $fournisseur[0]->nom_banque?>">

                </div>
                <div class="form-group">
                    <label for="numb">Numero Banque:</label>
                    <input type="text" class="form-control" name="numb" id="numb" value="<?php echo $fournisseur[0]->numero_banque?>">

                </div>
                <div class="form-group">
                    <label for="nom">Type:</label>
                    <select  class="form-control" name="type" id="type">
                        <?php if($fournisseur[0]->nom==0) { ?>
                        <option >Personne</option>
                        <option >Societe</option>
                        <?php } ?>
                        <?php if($fournisseur[0]->nom==1) { ?>
                            <option >Societe</option>
                            <option >Personne</option>
                        <?php } ?>
                    </select>

                </div>
                <div class="form-group">
                    <label for="nom">RIB:</label>
                    <input type="text" class="form-control" name="rib" id="rib" value="<?php if($fournisseur[0]->rib_fournisseur==0)echo "/";else echo $fournisseur[0]->rib_fournisseur ; ?>">
                </div>
                <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Modifier</button>
            </form>
        </div>
    </div>
    <?php
    $msg = $this->session->flashdata('msg')['msg'];
    echo $msg ?>

</div>

<?php include 'footer.php';
?>

    
    
