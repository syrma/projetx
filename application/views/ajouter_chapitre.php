<!DOCTYPE html>
<?php include 'header.php';  ?>
  <div class="container">
  	 		<div>
     	  	<h3 class="page-header">Gestion des Chapitres et Articles</h3>
     	  </div>
        <?php include 'menu_engagement.php';  ?>
      <div class="col-md-9">
        <div class="panel panel-success ">
          <div class="panel-heading">
          <h3 class="panel-title">Gestion des Chapitres</h3>
          </div>
          <div class="panel-body">  
            <h3>Ajouter Chapitre :</h3>
            <div class="row">
              <form action="ajouterchapitre/ajouter" method="post" accept-charset="utf-8">
                  <div class="col-md-3">
                      <div class="input-group input-group-right ">
                          <span class="input-group-addon">Section</span>
                          <select class="form-control" id="sec" name="sec">
                              <option>1</option>
                              <option>2</option>
                              </select>

                      </div>
                  </div>
                         <div class="col-md-5">
                                  <div class="input-group input-group-right ">
                                    <span class="input-group-addon">Code</span>
                                    <input type="nom" class="form-control" id="id" name="id" placeholder="code du chapitre">

                                  </div>
                            </div>
                            <div class="col-md-4">             
                                      <div class="input-group">
                                        <span class="input-group-addon">Nom</span>
                                        <input type="nom" class="form-control" id="nom" name="nom" placeholder="Nom du chapitre">
                                      </div>
                            </div>

                            <div class="col-md-3 ">
                                <br>
                              <button type="submit" class="btn btn-success btn-flat ">Ajouter chapitre</button>
                            </div>
              </form>
            </div>
            <!-- row/ -->
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      <?php
$msg = $this->session->flashdata('msg')['msg'];
echo $msg ?>
      </div>

      <!-- 9/ -->
  </div>
  <!-- container/ -->
 <?php include 'footer.php';
 ?>
