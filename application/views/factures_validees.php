<!DOCTYPE html>
<?php include 'header.php';  ?>
<div class="container">
    <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

        <div>
            <h3 class="page-header">Facturation</h3>
        </div>
    <?php } ?>
    <div class="col-md-10 col-md-offset-1">


        <div>
            <h3 class="page-header">Factures validées</h3>
        </div>

        <p></p>
        <p></p>
        <?php  if($records!=null){?>
        <div class="panel panel-success ">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des Factures</h3>
            </div>
            <div class="panel-body panel-bodyy">
                <table class="table table-hover" >

                    <thead>
                    <tr>
                        <th>code facture</th>
                        <th>code chapitre</th>
                        <th>Date</th>
                        <th>Montant</th>
                        <th>Fournisseur</th>
                        <th>Detail</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($records as $rec) { ?>
                        <tr>
                            <td><?php echo $rec->id ; ?></td>
                            <td><?php echo $rec->id_chapitre ; ?></td>
                            <td><?php echo $rec->date ; ?></td>
                            <td><?php echo $rec->montant ; ?></td>
                            <td><?php echo $rec->fournisseur ; ?></td>
                            <td><a href="detailfacture?id=<?php echo $rec->num; ?>"type="submit" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Detail</a></td>


                        </tr>
                    <?php } ?>
                    </tbody>
                </table>


            </div>
        </div>
        <?php
        $msg = $this->session->flashdata('msg')['msg'];

        echo $msg;
        ?>
    </div>
    <?php } else { ?>
</div>
<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">il n y a aucune facture à afficher </div>
<?php }?>
</div>
<?php  include 'footer.php';
?>
