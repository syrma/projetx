<?php include 'header.php';  ?>
<div class="container">
	<div class="row">
		<div class="col-md-12 ">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<ul class="list-items">
						<?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>
						<li class="list text-center">
							<a href="#" class="circ">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img  src="<?php echo base_url();?>assets/img/flat-icons/question.png" alt="">
							</a>
							<h3 class="">Section 1</h3>
						</li>

						<li class="list text-center">
							<a href="engagement" class="circ">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img  src="<?php echo base_url();?>assets/img/flat-icons/business.png" alt="">
							</a>
							<h3 class="">Engagements</h3>
						</li>
						<?php }?>
						<?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>
						<div class="row col-md-1 col-md-offset-0">

							<?php } else{ ?>
						<div class="row col-md-1 col-md-offset-2">

							<?php }?>
						<li class="list text-center">
							<a href="facturation" class="circ">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img src="<?php echo base_url();?>assets/img/flat-icons/bill.png" alt="">
							</a>
							<h3 class="">Facturation</h3>
						</li>
							</div>
							<?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

							<div class="row col-md-0 col-md-offset-9">


							<?php } else{ ?>
							<div class="row col-md-1 col-md-offset-3">

									<?php }?>
						<li class="list text-center">
							<a href="dateetatpaiement" class="circ" onclick="window.open('datesituationfinanciere');
						 return true;">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img  src="<?php echo base_url();?>assets/img/flat-icons/buildings.png" alt="">
							</a>
							<h3 class="">Situation Financiere</h3>
						</li>
						</div>

						<h3  class="page-header col-md-8 col-md-offset-2"> </h3>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 ">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<ul class="list-items  center-block">
						<?php if($this->session->userdata('categorie')!="Agent"){?>
						<li class="list text-center">
							<a href="utilisateur" class="circ">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img  src="<?php echo base_url();?>assets/img/flat-icons/users.png" alt="">
							</a>
							<h3 class="">Gestion utilisateurs</h3>
						</li>
						<?php }if($this->session->userdata('categorie')=="Agent"){ ?>
						<div class=" col-md-offset-1">
						<?php }?>
						<li class="list text-center">
							<a href="gestionchapitres?en=0" class="circ">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img  src="<?php echo base_url();?>assets/img/flat-icons/money.png" alt="">
							</a>
							<h3 class="">Gestion chapitres et articles</h3>
						</li>
							<?php if($this->session->userdata('categorie')=="Agent"){ ?>
							</div>
								<?php }?>

						<li class="list text-center">
							<a href="impressions" class="circ">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img  src="<?php echo base_url();?>assets/img/flat-icons/technology.png" alt="">
							</a>
							<h3 class="">Impressions</h3>
						</li>
						<li class="list text-center">
							<a href="archivage" class="circ">
								<!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
								<i class="circ pulse"></i>
								<img  src="<?php echo base_url();?>assets/img/flat-icons/folder.png" alt="">
							</a>
							<h3 class="">Archive</h3>
						</li>

						<h3  class="page-header col-lg-10 col-lg-offset-1"> </h3>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php
	$msg = $this->session->flashdata('msg')['msg'];
	echo $msg ?>

</div>
<?php include 'footer.php';
?>
