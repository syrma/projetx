﻿
<?php include 'header.php';  ?>

<div class="container">
    <div>
        <h3 class="page-header" href="facturation"> Fiche D'engagement provisionnel</h3>
    </div>
    <div class="col-md-6 col-md-offset-3">

        <form action="FicheEngagProvi/imprimerFEP?sem=<?php echo $sem ?>" method="post" accept-charset="utf-8">

               <div class="form-group">
			    <?php  if(($recordsc!=null)&&($recordsa!=null)){?>
                <label for="chapitre">Chapitre:</label>
                <select class="form-control" name="chapitre" id="chapitre">
                    <?php
                    foreach ($recordsc as $rec){ ?>
                        <option>   <?php echo $rec->id?></option>
                    <?php } ?>

                </select>

            </div>
            <div class="form-group">
                <label for="article">Article:</label>
                <select class="form-control" name="article" id="article">
                    <?php
                    foreach ($recordsa as $rec){ ?>
                        <option>   <?php echo $rec->id?></option>
                    <?php } ?>

                </select>

            </div>
            <button type="submit" class="btn btn-top btn-success col-md-8 col-md-offset-2 btn-flat">Imprimer</button>
				
        </form>
   
				<?php  }else { ?>

<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">il n y a aucune facture à afficher </div>
<?php }?>

     <?php
        $msg = $this->session->flashdata('msg')['msg'];

        echo $msg;
        ?>
    </div>

</div>


 <script src="<?php echo base_url();?>js/jquery.min.js"></script>
 <script type = "text/javascript"> 
 $('#chapitre').change(function() {  
           var $chapitre = $('#chapitre').val();
           $.ajax({
             url: 'articlesajax?idc='+$chapitre,
             type: 'GET',
             dataType: 'json',
             success: function(result) {
                 var $article = $('#article');
                 var records = result.recordsa;
                 var options_str = "";
                 records.forEach( function(rec) {
                     options_str += '<option value="' + rec.id + '">' + rec.id +
 '</option>';
                 });
 
                 $article.html(options_str);
                 }
                 });
 });
 </script>


<?php include 'footer.php';
?>
