<?php 

/**
* Header generator
*/
class HeaderFactory
{
	/**
	 * String of HTML
	 * @var string
	 */
	public static $headerHTML ='<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8">';
	/**
	 * set The title of the page
	 * @param String $title title in the header
	 */
	static function setTitle($title){
		self::$headerHTML .='<title>'.$title.'</title>';
	}
	/**
	 * adding CSS link to the page
	 * @param String $path cdn or the relative link
	 */
	static function addCSS($path){
		self::$headerHTML .= '<link rel="stylesheet" type="text/css" href="'.$path.'">';
	}
	/**
	 * addi scripts to the page
	 * @param String $script linking the javascript files by the given path , CDN or relative links.
	 */
	static function addScript($script){
		self::$headerHTML .= '<script type="text/javascript">'.$script.'</script>';
	}
	/**
	 * method that returns the HTML structure of the header
	 * @param  string $title title of the page
	 * @return String HTML        the HTML generated.
	 */
	static function get_header($title='Page'){
		self::setTitle($title);
		self::$headerHTML .= '</head><body>';
		return self::$headerHTML;
	}
}