<?php include 'header.php';  ?>
    <div class="container">
        <div>
            <h3 class="page-header">Impressions</h3>
        </div>
        <!--<a href="ajouteravis" class="btn btn-top btn-success col-md-2 col-md-offset-0 btn-flat">Ajouter virement</a>-->

        <div class="col-md-12">
            <div class="panel panel-success panel-successs  ">
                <div class="panel-heading">
                    <h3 class="panel-title">Fiches vides</h3>
                </div>

                <div class="panel-body panel-bodyy">

                    <table class="table table-hover" >
                        <thead>
                        <tr>
                            <th>Nom de La fiche</th>
                            <th>impression</th>

                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>CH50</td>
                            <td><a href="CH50" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                        </tr>
						  <tr>
                            <td>Mandat</td>

                            <td><a href="FMandat/mandatVide" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>


                        </tr>
						  <tr>
                            <td>Avis de virement</td>
                            <td><a href="FAvisDeVirement/imprimerAV" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="panel panel-success panel-successs  ">
                <div class="panel-heading">
                    <h3 class="panel-title">Fiches semestrielles</h3>
                </div>
                <div class="panel-body panel-bodyy">

                    <table class="table table-hover" >
                        <thead>
                        <tr>
                            <th>Nom de La fiche</th>
                            <th>impression</th>

                        </tr>
                        </thead>
                        <tbody>
                            <?php if($etat[0]->date_fin_annee!=null){?>
                            <tr>
                                <td>Fiche de régularisation premier semestre</td>
                                <td><a href="FicheRegularisation" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                            </tr>
                                <tr>
                                    <td>Fiche de régularisation deuxieme semestre semestre</td>
                                    <td><a href="FicheRegularisationS2" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                                </tr>
                            <?php }if($etat[0]->date_fin_annee==null&&$etat[0]->semestre==2){ ?>
                                <tr>
                                    <td>Fiche de régularisation premier semestre</td>
                                    <td><a href="FicheRegularisation" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                                </tr>

                            <?php } ?>
                            <?php if($etat[0]->semestre==2){?>
                                <tr>
                                    <td>Fiche d'engagement provisionnel premier semestre</td>
                                    <td><a href="F_EProvi?sem=1"  class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                                </tr>
                                <tr>
                                    <td>Fiche d'engagement provisionnel deuxieme semestre semestre</td>
                                    <td><a href="F_EProvi?sem=2" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                                </tr>
                            <?php }?>

                            <?php if($etat[0]->semestre==1){ ?>
                                <tr>
                                    <td>Fiche d'engagement provisionnel premier semestre</td>
                                    <td><a href="F_EProvi?sem=1" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                                </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>



        <div class="col-md-12">
            <div class="panel panel-success panel-successs  ">
                <div class="panel-heading">
                    <h3 class="panel-title">Fiches trimestrielles</h3>
                </div>
                <div class="panel-body panel-bodyy">

                    <table class="table table-hover" >
                        <thead>
                        <tr>
                            <th>Nom de La fiche</th>
                            <th>impression</th>

                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>Situation d'engagement</td>
                            <td><a href="datesituationengagement" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php if($articles!=null) {?>
        <div class="col-md-12">
            <div class="panel panel-success panel-successs  ">
                <div class="panel-heading">
                    <h3 class="panel-title">Fiches par article</h3>
                </div>
                <div class="panel-body panel-bodyy">

                    <table class="table table-hover" >
                        <thead>
                        <tr>
                            <th>Nom de La fiche</th>
                            <th>impression</th>

                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>Fiche de rattachement</td>
                            <td><a href="FRattachement" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                        </tr>
                        <tr>
                            <td>Fiche de transfert</td>
                            <td><a href="FTransfert" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                        </tr>

                        <tr>
                            <td>Etat de paiement</td>
                            <td><a href="dateetatpaiement" class="btn btn-info btn-flat btn-sm" target = "_blank"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> imprimer</a></td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php }?>
        <!-- 9/ -->
    </div>
<?php include 'footer.php';
?>
