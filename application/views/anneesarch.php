<!DOCTYPE html>
<?php include 'header.php';  ?>
  <div class="container">
    <div>
      <h3 class="page-header">Archives</h3>
    </div>
    <div class="panel-body">
      <?php if($archives != null){ ?>
      <h5>Sélectionnez l'année des archives à consulter : </h5>
      <form action="chargerarchive" method="post" accept-charset="utf-8">
        <div class="col-md-6 col-md-offset-3">
          <select class="form-control" id="annee" name="annee">
          <?php
            foreach ($archives as $annee => $arch){ ?>
              <option value="<?php echo $arch ?>"> <?php echo $annee?></option>
             <?php } ?>
           </select>
        </div> 
        <button type="submit" class="btn btn-top btn-success col-md-4 col-md-offset-4 btn-flat">Charger l'archive</button>
      </form>
    </div>
           <?php }else{ ?>
           <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">
           Il n'y a pas d'archives. 
           </div>
           <?php } ?>                              
  </div>
       <?php
$msg = $this->session->flashdata('msg')['msg'];
echo $msg ?>
 <?php include 'footer.php';
 ?>
