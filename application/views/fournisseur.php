  <!DOCTYPE html>
<?php include 'header.php';  ?> 
 <div class="container">

       
     	   <div class="col-md-10 col-md-offset-1">

     	   <div>
     	  	<h3 class="page-header">Fournisseurs</h3>
     	  </div>

     	   <p></p>
     	   <p></p>
			   <?php  if($records!=null){?>
     	   <div class="panel panel-success ">
				<div class="panel-heading">
					<h3 class="panel-title">Liste des Fournisseurs</h3>
				</div>
				<div class="panel-body panel-bodyy">
     	   	<table class="table table-hover" >
     	   		
     	   		<thead>
     	   			<tr>
     	   				<th>Nom</th>
     	   				<th>Adresse</th>
     	   				<th>Nom_Banque</th>
						<th>N° Banque</th>
						<th>RIB</th>
						<th>Type</th>
						<?php if(!isset($arch) && ($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent")) {?>

						<th>Modifier</th>
<?php } ?>

     	   			</tr>
     	   		</thead>
     	   		<tbody>
				<?php foreach ($records as $row) {
     	   			echo "<tr>";
     	   				echo "<td>".$row->nom."</td>";
     	   				echo "<td>".$row->adr ."</td>";
                         echo "<td>".$row->nom_banque."</td>";
                          echo"<td>".$row->numero_banque."</td>";
                       echo"<td>".$row->rib_fournisseur."</td>";
                            if($row->type_fournisseur==0)echo"<td>"."Personne"."</td>";
                             else echo"<td>"."Société"."</td>";?>
				<?php if (!isset($arch) && ($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent")){?>

     	   				<td><a href="modifierfournisseur?id=<?php echo $row->id?>"type="submit" class="btn btn-primary btn-flat btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modifier</a></td>
     	   			<?php } ?>
     	   			</tr>
     	   			<?php } ?>
     	   		</tbody>
     	   	</table>
     	   	</div>
     	   </div>
	  <?php } else { ?>
		  <div class="alert alert-danger col-md-12 col-md-offset-1" role="alert">il n y a aucun fournisseur a afficher </div>
	  <?php }?>
 </div>
 </div>
  <p></p>
  <p></p>
  <p></p>
 <?php include 'footer.php';
 ?>
