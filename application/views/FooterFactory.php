<?php 

/**
* Footer generator
*/
class FooterFactory
{
	/**
	 * String of HTML
	 * @var string
	 */
	public static $footerHTML ='</body></html>';
	/**
	 * adding scripts to the page
	 * @param String $script linking the javascript files
	 */
	static function addScript($script){
		self::$footerHTML .= '<script type="text/javascript">'.$script.'</script>';
	}
	/**
	 * reutrns the footer of the page
	 * @return HTML of the pages footer
	 */
	static function get_footer(){
		return self::$footerHTML;
	}
}


