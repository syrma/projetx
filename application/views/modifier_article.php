<?php include 'header.php';  ?>
<script src="<?php echo base_url();?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/autoNumeric.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($) {
    $('.auto').autoNumeric('init');
});
</script>
<script type="text/javascript">
jQuery(function($) {
    $('.auto2').autoNumeric('init');
});
</script>
	<div class="container">
				<h3 class="page-header">Modifier Article</h3>
		 <div class=" col-md-6 col-md-offset-3">
			<h5 class="well ">Article <?php  echo $records[0]->id." ".$records[0]->nom; ?></h5>

     	  	  	  <form action="modifierarticle/modifier" method="post" role="form">
                    
                    <div class="form-group">
                            <label for="nom">Nom:</label>
                            <input type="text" class="form-control" name="nom" id="nom"  value="<?php echo $records[0]->nom; ?>">
                    
                    </div>
                    <div class="form-group">
                            <label for="code">Code de l'article:</label>
                            <input type="text" class="form-control" name="code" id="code"  value="<?php echo $records[0]->id ;?>">
                    
                    </div>
                      <div class="form-group">
                          <label for="code">budget complementaire:</label>
                          <input type="text" class="form-control auto" name="montantc" id="montantc"  data-a-sep=" " data-a-dec="," data-a-sign="D.A ">
                      </div>

                    <div class="form-group">
                            <label for="code">Budget initial:</label>
                            <?php $val = $records[0]->budget_initial;
                             if($val == 0)
                                 $val = ""; ?>
                            <input type="text" class="form-control auto2" name="montant" id="montant" data-a-sep=" " value="<?php echo $val;?>" data-a-dec="," data-a-sign="D.A ">

                        <input type="hidden" value="<?php  echo  $records[0]->id ?>" name="idb" >
                            <input type="hidden" value="<?php  echo $idc ?>" name="idc" >
                            <input type="hidden" value=" <?php  echo $records[0]->budget_initial; ?>" name="budg" >



                    </div>
                   
                    
                        <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Confirmer</button>
                </form>
          </div>
        <?php
        $msg = $this->session->flashdata('msg')['msg'];

        echo $msg;
        ?>

	</div>
<?php include 'footer.php';
?>
