<!DOCTYPE html>
<html>
<head>
    <title></title>
     	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/test.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.datePicker.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrapValidator.min.css">
</head>
<body>
<nav class="navbar ">
    <div class="center-logo text-center">
        <a href="accueil"> <img src="<?php echo base_url();?>assets/img/logo.png" alt ="..." class="img-circle" height="100"/> </a>

        <h5>Université Abdelhamid Mehri Constantine 2 </h5>
        <h5>Faculté des Nouvelles Technologies de l’Information et de la Communication </h5>


    </div>
    <div class="container-fluid">

        <div class="navbar-header">
            <a class="navbar-brand" href="accueil">Accueil</a>

        </div>


        <ul class="nav navbar-nav navbar-right">
            <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){
            ?>
            <li class="dropdown"><a class="dropdown-toggle " data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-th-large"></span> Sections <span class="caret"></span></a>
                <ul class="dropdown-menu ">
                    <li class="dropdown-submenu" ><a tabindex="-1" href="engagement"><span class=""> Les Engagements</a>
                        <ul class="dropdown-menu">
                            <li><a  href="transfert">Transferts</a></li>
                            <li class="dropdown-submenu" >
                                <a tabindex="-1" href="gestionchapitres">Chapitre</a>
                                <ul class="dropdown-menu">
                                    <li><a href="ajouterchapitre">Ajouter Chapitre</a></li>

                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li><a href="facturation"><span class=""> Facturation</a></li>
                    <li><a href="situationfinanciere"><span class=""> Situation Financiere</a></li>
                </ul>

            </li>
            <?php }
            ?>
            <?php if($this->session->userdata('categorie')!="Agent"){?>
            <li class="dropdown"><a class="dropdown-toggle " data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> Utilisateurs <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="utilisateur"><span class=""> Liste des Utilisateurs</a></li>
                    <li><a href="ajouterutilisateur"><span class=""> Ajouter Utilisateur</a></li>
                </ul>
            </li>

            <?php }if($this->session->userdata('categorie')=="Responsable"){?>

            <li><a href="parametres"><span class="glyphicon glyphicon-cog"></span> Parametres</a></li>
            <?php } ?>
            <li class="dropdown "><a class="dropdown-toggle " data-toggle="dropdown" ><?php echo $this->session->userdata('nom')." ".$this->session->userdata('prenom') ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="profil"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="deconnexion"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
                </ul>
            </li>

        </ul>
    </div>

</nav> 

