﻿<div style="height:=100vh;">
	<h4>Etablisement :</h4>
	<h2 align="center">MANDAT DE PAIEMENT N°</h2>
	<table width="100" border="1" align="right" style="border-collapse: collapse;">
										<tbody>
										
											<tr><?php 
											$mandat = str_split($mandat);
											foreach($mandat as $char){ ?>
											<td><?php echo $char; ?></td>
											<?php } ?>
											</tr>
										</tbody>
  </table>
	<div style="width:50%;font-size: small;">
		<p>UNIVERSITE CONSTANTINE 02<br>
			ABDELHAMID MEHRI<br  />
		FACULTE DES NOUVELLES TECHNOLOGIES DE L'INFORMATION ET DE LA COMMUNICATION </p>
	</div>

	<h4>Exercice : <?php echo $annee; ?></h4>
  <div>
		<table border="1" style="width:100%;border-collapse: collapse;">
			<tr>
				<td rowspan="2" style="width:15%;">CHAPITRE</td>
				<td colspan="3"><div align="center">IMPUTATION</div></td>
  
			</tr>
			<tr>
				<td style="15%;"><table style="border-collapse: collapse;width:100%;" border="1">
  <tr>
  <?php
  $int = strlen($chapitre) - strlen($article); 
  $chapi = str_split($chapitre);
  $arti = str_split($article);
  foreach($chapi as $chap){?>
    <td><?php echo $chap; ?></td>
  <?php } ?>
  </tr>
  <tr>
  <?php while($int > 0){ ?>
      <td>&nbsp;</td>
  <?php $int -= 1;
  } ?>
  
  <?php foreach($arti as $art){ ?>
    <td><?php echo $art ?></td>
   <?php } ?>
  </tr>

</table>
</td>
				<td><?php echo $articlen; ?></td>
				<td><?php echo $montantf; ?></td>
    
			</tr>
		</table>
		<p>En vertu des crédits ouvert au budget de l'Etablissement,le comptable est chargé d'exécuter ce mandat de paiement,conformément aux indications suivantes:</p>
		<table  border="1" style="width:100%;border-collapse: collapse;">
			<tr>
				<td>Nom ou raison sociale</td>
				<td>Domicile</td>
				<td>N° compte </td>
				<td>Objet du paiement</td>
				<td>Montant</td>
			</tr>
			<tr>
				<td><p> <?php echo $nomfournisseur." ".$prenomfournisseur; ?> </p>
				<p> <?php echo $adressef; ?> </p>
				</td>
				<td><?php echo $banquef ?></td>
				<td><?php echo $ribf ?></td>
				<td><p>Règlement facture</p>
				<p> <?php echo $n_fact." du ".$datef;?></p> </td>
				<td><?php echo $montantf ?></td>
			</tr>
			<tr>
				<td colspan="3"><div align="center">PARTIE RESERVEE AU COMPTABLE</div></td>
				<td rowspan="4">&nbsp;</td>
				<td rowspan="3">&nbsp;</td>
   
  
			</tr>
			<tr>
				<td rowspan="2">Admission en dépense</td>
				<td colspan="2"><div align="center">Rejet provisoire/définitif</div></td>
 
			</tr>
			<tr>
				<td colspan="2"><div align="center">Motif du rejet</div></td>
  
			</tr>
			<tr>
				<td><span align="center">Le :<br> Le comptable</span></td>
				<td colspan="2">   ........................................<br>  
      ........................................<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Le Comptable,</td>
				<td>&nbsp;</td>
    
			</tr>
		</table>
		<p>Le présent mandat de paiement est arrêté à la somme brute de:</p>
		<div style="border:1px solid black;">
		  <p> <?php echo $montantl ; ?></p>
		</div><br>
		<div style="width:100%;">
			
				<div style="float:left;width:45%">
					<table border="1"  style="border-collapse: collapse;">
						<tr>
							<td width="512">&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
			</div>
			<div style="width:51%">
				<div style="float:left;width:35%"><br>L'ordonnateur</div>
				<div style="float:right;width:64%"> Le
					<table border="1" style="border-collapse: collapse;">
						<tr>
							<td width="508" height="69">&nbsp;</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

<br>
<table border="1" style="width:100%;border-collapse: collapse;">
  <tr>
    <td width="25%">Compte Trésor </td>
    <td width="25%">Compte chèque Postal </td>
    <td width="25%">Caisse Principale </td>
    <td width="25%">Caisse Auxiliaire </td>
  </tr>
  <tr>
    <td><div align="left">ChèqueN°............du........<br>
      VirementN°.........du........<br>
      ........................................<br>  
      ........................................<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Le Comptable,</div></td>
    <td><div align="left">ChèqueN°...........du........<br>
     ........................................<br>
      ........................................<br>  
      ........................................<br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Le Comptable,</div></td>
    <td><div align="left">ReçuN°..............du...........<br>
     <br> <br>  <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Le Comptable,</div></td>
    <td><div align="center">Vèrifié et pris en Compte<br>
         <br> 
      <br>  
      <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Le Comptable,</div></td>
  </tr>
</table>
<pre>Réf.De Comtabilisation :
JCN........Folio N.............Mois de ...........N°d'ordre............</pre>
</div>

</div>
