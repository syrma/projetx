<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Situation des engagements</title>
</head>

<body>
<div style="padding-left:20px">
UNIVERSITE DE CONSTANTINE 02
<br>ABDELHAMID MEHRI
<br>FACULTE DES NOUVELLES TECHNOLOGIES DE L'INFORMATION ET DE LA COMMUNICATION
<br><br>
</div>
<p align="center">SECTION 1</p>

<table width="100%" height="310" border="1" style="border-collapse: collapse;">
  <tbody>
    <tr>
      <th width="50" scope="row">CHAP</th>
      <td width="35">ART</td>
      <td width="153">LIBELLE DES CHAP/ART</td>
      <td width="110">CREDIT ALLOUE</td>
      <td width="104">CREDIT REVISE</td>
      <td width="70">TRANS+</td>
      <td width="66">TRANS-</td>
      <td width="125"><p>CREDIT DEFINITIF</p>
      <p>&nbsp;</p></td>
      <td width="124">ENGAGEMENT</td>
      <td width="58">SOLDE</td>
      <td width="62">TAUX%</td>
    </tr>
   
	 <?php

                   foreach ($chapitre1 as $rec){ ?>
					 <tr>
					 <?php $nb=2;
						foreach ($article as $row){ 
							if($rec->id==$row->id_chapitre){ $nb=$nb+1; }}?>
                    <th scope="row" rowspan=<?php echo $nb;?> >  <?php echo $rec->id?></th>
					
					<td colspan="2"><?php echo $rec->nom;?></td>
					<td></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					  </tr>
      
  <?php
   foreach ($article as $row){ 
   if($rec->id==$row->id_chapitre){?>
    <tr>
      <td><?php echo $row->id ; ?></td>
      <td><?php echo $row->nom;?></td>
      <td><?php echo $row->budget_initial;?></td>
      <td><?php echo $row->budget_complementaire;?></td>
      <td><?php echo $row->transc;?></td>
      <td><?php echo $row->transs;?></td>
      <td><?php echo $row->definitif;?></td>
      <td><?php echo $row->definitif;?></td>
      <td><?php echo $row->solde;?></td>
      <td><?php echo $row->taux;?></td>
    </tr>
   <?php } }?>
   
   <tr bgcolor="#dcdcdc">
    <td>&nbsp;</td>
   <td >TOTAL DU CHAPITRE <?php echo $rec->id?></td>
					
					<td><?php echo $rec->budget_initial?></td>
					<td><?php echo $rec->budget_complementaire?></td>
					<td><?php echo $rec->transc?></td>
					<td><?php echo $rec->transs?></td>
					<td><?php echo $rec->definitif?></td>
					<td><?php echo $rec->definitif?></td>
					<td><?php echo $rec->solde?></td>
					<td><?php echo $rec->taux?></td>
   </tr>

	 <?php } ?>
    <tr bgcolor="#dcdcdc">
        <td>&nbsp;</td>
        <td colspan="2">TOTAL SECTION I</td>
        <td><?php echo $section1->budget_initial ?></td>
        <td><?php echo $section1->budget_complementaire?></td>
        <td><?php echo $section1->transc?></td>
        <td><?php echo $section1->transs?></td>
        <td><?php echo $section1->definitif?></td>
        <td><?php echo $section1->definitif?></td>
        <td><?php echo $section1->solde?></td>
        <td><?php echo $section1->taux?></td>
    </tr>

    <?php

    foreach ($chapitre2 as $rec){ ?>
        <tr>
            <?php $nb=2;
            foreach ($article as $row){
                if($rec->id==$row->id_chapitre){ $nb=$nb+1; }}?>
            <th scope="row" rowspan=<?php echo $nb;?> >  <?php echo $rec->id?></th>

            <td colspan="2"><?php echo $rec->nom;?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

        <?php
        foreach ($article as $row){
            if($rec->id==$row->id_chapitre){?>
                <tr>
                    <td><?php echo $row->id ; ?></td>
                    <td><?php echo $row->nom;?></td>
                    <td><?php echo $row->budget_initial;?></td>
                    <td><?php echo $row->budget_complementaire;?></td>
                    <td><?php echo $row->transc;?></td>
                    <td><?php echo $row->transs;?></td>
                    <td><?php echo $row->definitif;?></td>
                    <td><?php echo $row->definitif;?></td>
                    <td><?php echo $row->solde;?></td>
                    <td><?php echo $row->taux;?></td>

                </tr>
            <?php } }?>

        <tr bgcolor="#dcdcdc">
            <td>&nbsp;</td>
            <td >TOTAL DU CHAPITRE <?php echo $rec->id?></td>


            <td><?php echo $rec->budget_initial?></td>
            <td><?php echo $rec->budget_complementaire?></td>
            <td><?php echo $rec->transc?></td>
            <td><?php echo $rec->transs?></td>
            <td><?php echo $rec->definitif?></td>
            <td><?php echo $rec->definitif?></td>
            <td><?php echo $rec->solde?></td>
            <td><?php echo $rec->taux?></td>
        </tr>

    <?php } ?>
    <tr bgcolor="#dcdcdc">
        <td>&nbsp;</td>
        <td colspan="2">TOTAL SECTION II</td>
        <td><?php echo $section2->budget_initial ?></td>
        <td><?php echo $section2->budget_complementaire?></td>
        <td><?php echo $section2->transc?></td>
        <td><?php echo $section2->transs?></td>
        <td><?php echo $section2->definitif?></td>
        <td><?php echo $section2->definitif?></td>
        <td><?php echo $section2->solde?></td>
        <td><?php echo $section2->taux?></td>

    </tr>
    <tr bgcolor="#dcdcdc">

        <td colspan="3">TOTAL SECTION I-II</td>
        <td><?php echo $section1->budget_initial+ $section2->budget_initial?></td>
        <td><?php echo $section1->budget_complementaire+ $section2->budget_complementaire?></td>
        <td><?php echo $section1->transc+ $section2->transc?></td>
        <td><?php echo $section1->transs+$section2->transs?></td>
        <td><?php echo $section1->definitif+$section2->definitif?></td>
        <td><?php echo $section1->definitif+$section2->definitif?></td>
        <td><?php echo $section1->solde+$section2->solde?></td>
        <td><?php echo $section1->taux+$section2->taux?></td>


    </tr>
  </tbody>
    </br></br>
    <p>Arr�t� le pr�sent �tat de paiement � la somme de :  <?php echo $montantL ?> </p>
    </br></br>
    <div><p style="text-align: right;padding-right: 150px">L'ORDONNATEUR</p>
    </div>
</table>
</body>
</html>
