﻿<!DOCTYPE  HTML>
<html >
<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>ETAT DE PAIEMENT</title>
</head>

<body>

<div style="text-align:center">
<p>الجمهورية الجزائرية الديمقراطية الشعبية<br>
La République Algérienne Démocratique et Populaire<br>
وزارة التعليم العالي والبحث العلمي<br>
Ministère de L'enseignement Supérieur et de la Recherche Scientifique</p>
</div>

<div style="width:50%;">
<p>UNIVERSITE CONSTANTINE 02<br>
ABDELHAMID MEHRI</p>
<p>FACULTE DES NOUVELLES TECHNOLOGIES DE L'INFORMATION ET DE LA COMMUNICATION </p>

</div>
<br>
<h2 align="center"><strong><em>ETAT DE PAIEMENT.......SEMESTRE EXERCICE <?php foreach ($annee as $a){echo $a->annee;}?></em></strong></h2>

<div>
<p>CHAPITRE  <?php echo $chap;?><br>
ARTICLE <?php echo $art;?></p> 
</div>

<table  border="1" style="width:100%;border-collapse: collapse;">
  <tr>
    <td rowspan="2">FOURNISSEUR</td>
    <td colspan="2">MANDAT</td>
    <td rowspan="2">MONTANT</td>
    <td colspan="2">FACTURE</td>
   
  </tr>
  <tr>

    <td>N</td>
    <td>DATE</td>
    <td>N</td>
    <td>DATE</td>
  </tr>

    <?php foreach($paiements as $row){?>
	  <tr>
    <td><?php echo $row->fournisseur;?></td>
    <td><?php echo $row->nummandat;?></td>
    <td><?php echo $row->datemandat;?></td>
    <td><?php echo $row->montant;?></td>
      <td><?php echo $row->num;?></td>
      <td><?php echo $row->datefacture;?></td>
	   </tr>
    <?php } ?>
 
  <tr>
    <td><?php echo '';?></td>
    <td><?php echo '';?></td>
    <td>TOTAL</td>
    <td><?php echo $total ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div><p>Arrêté le présent état de paiement à la somme de :<?php echo '';?></p>
<p>....<?php echo $montantL ?>...</p></div>
<br>
	<pre>				LE DOYEN  			 L'AGENT COMPTABLE		</pre>

</body>
</html>
