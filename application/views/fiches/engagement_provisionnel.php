<!DOCTYPE html>
<html>
<head>
    <title>Fiche</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>
<div class="Container">
    <div id="maDivGauche" style="width:42%;float:left">
        <h5 class="col-md-offset-4" align="center">DETAIL DES ENGAGEMENTS</h5>
        <div class="col-md-6">

            <table border="1" style="width:100%;border-collapse: collapse;" >
                <thead>
                <tr><th> NATURE DES ENGAGEMENTS</th><th>MONTANTS</th> </tr>
                </thead>
                <tfoot>
                <tr><td align="right">TOTAL ..............</td>
                    <td><?php  echo $budget_initial;?></td>
                </tr>
                </tfoot>
                <tbody>
                <tr><td style="height:530px" >ENGAGEMENT PROVISIONNEL
                            <br></td><td><?php  echo $budget_initial;?>
                            <br></td>
                </tr>
                </tbody>
            </table>
            <p>TOTAL en Lettres :<br>
                <?php echo $montantL ;?> </p>
        </div>
    </div>
    <div id="maDivDroite" style="width:54%;float:right;">


        <h6>FACULTE DES NOUVELLES TECHNOLOGIES DE L'INFORMATION ET DE LA COMMUNICATION</h6>
        <h4 class="col-md-offset-4" align="center" >BUDGET DE L'ETAT</h4>

        <div class="head" >
            <table width="100%"><tr>
                    <td>
                        <table border="1"  height="70" style=" border-collapse: collapse;">
                            <thead>
                            <tr><th COLSPAN=2>VISA DU CONTROLE FINANCIER</th></tr>
                            </thead>
                            <tbody>
                            <tr><td>N°</td><td width="176">&nbsp;  </td></tr>
                            <tr><td>DU</td><td>&nbsp; </td></tr>
                            </tbody>
                        </table>
                    </td>
                    <td  width="120" align="center">
                        MINISTERE
                        <br><br>SERVICE
                    </td>
                    <td width="120">
                        <table width="100" border="1" style="border-collapse: collapse;">
                            <tbody>
                            <tr><td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <table width="100" border="1" style="border-collapse: collapse;">
                            <tbody>
                            <tr> <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <table  border="1"   height="80"  width="130" style=" border-collapse: collapse;">
                            <tr><td >ANNEE</td><td> <?php echo $annee ?></td></tr>
                            <tr><td>FICHE N°</td><td> <?php echo $nFiche ?> </td></tr>
                        </table>
                    </td>
                </tr></table>

        </div>
        <br>
        <div class="DivSuite" style="border: 1px solid black;" >

            <div >
                <p align="center">LA DEPENCE (1)</p>
                <table align="center" width="50" border="1" style="border-collapse: collapse;">
                    <tbody>
                    <tr><th scope="row">&nbsp;</th></tr>
                    </tbody>
                </table>
                <p align="center">OBJET DE :</p>
                <p align="center">&nbsp;&nbsp;L'ECONOMIE (2)</p>
                <table width="50" border="1" align="center" style="border-collapse: collapse;">
                    <tbody>
                    <tr> <th scope="row">&nbsp;</th></tr>
                    </tbody>
                </table>
            </div>
            <div>
                <p align=center>SECTION   &nbsp;&nbsp;0<?php echo $section; ?>  &nbsp;&nbsp;   Fiche De <?php echo $nom ?>  &nbsp; &nbsp; <?php echo $annee ?></p>

                <table  border="1"  style=" width:100%;border-collapse: collapse;">
                    <tr><td>CHAPITRE</td>
                        <td> ART  </td>
                        <td>ANCIEN SOLDE</td>
                        <td>MONTANT DE L'OPERATION</td>
                        <td>NOUVEAU SOLDE</td>
                    </tr>
                    <tr height="130" >
                        <td><?php echo $id_chapitre; ?></td>
                        <td><?php echo $id_article; ?></td>
                        <td><?php echo $budget_initial;  ?>
                        </td>
                        <td>...</td>
                        <td><?php echo $budget_initial;?></td>
                    </tr>
                </table>
            </div>
            <div>

                <p align="center"><U>OBSERVATIONS DU SERVICE</U></p>


                    CHAP/ART:&nbsp;&nbsp;<?php echo $id_chapitre; ?> &nbsp; &nbsp;<?php echo $id_article; ?>&nbsp;&nbsp;&nbsp;<?php echo $nomart; ?>
                <br><br><br><br>

            </div>
        </div>
        <div class="DivFin" >
            <br>
            <pre>L'ORDONNATEUR  		 CONSTANTINE LE:		</pre>


        </div>
    </div>

</body>
</html>
