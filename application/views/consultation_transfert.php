<?php include 'header.php';  ?>
    <div class="container">
        <div>
            <h3 class="page-header">Transferts</h3>
        </div>
        <div class="list-group col-md-3 ">
            <a href="gestionchapitres?en=0" class="list-group-item">Gestion des Chapitre et articles</a>

        </div>
        <?php if($transfert!=null){?>

        <div class="col-md-12">

            <div class="panel panel-success panel-successs  ">
                <div class="panel-heading">
                    <h3 class="panel-title">Transferts</h3>
                </div>

                <div class="panel-body panel-bodyy">

                    <table class="table table-hover" >
                        <thead>
                        <tr>
                            <th>Chapitre source</th>
                            <th>Article source</th>
                            <th>Chapitre cible</th>
                            <th>Article cible</th>
                            <th>Montant</th>
                            <th>Date</th>
                            <th>Auteur</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($transfert as $row) {?>
                            <tr>
                                <td><?php echo $row->chapitre_source ?></td>
                                <td><?php echo $row->article_source ?></td>
                                <td><?php echo $row->chapitre_cible ?></td>
                                <td><?php echo $row->article_cible ?></td>
                                <td><?php echo $row->montant_transfert ?></td>
                                <td><?php echo $row->date_transfert ?></td>
                                <td><?php echo $row->nom." ".$row->prenom ?></td>

                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <?php } else{ ?>
            <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">il n y a aucun transfert a afficher</div>

        <?php  }?>
        <!-- 9/ -->
    </div>
<?php include 'footer.php';
?>
