<!DOCTYPE html>
<html>
<head>
  <title>Mot de passe oublie</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
</head>
<body>
<div class="container">
  <a href="#" data-target="#pwdModal" data-toggle="modal">Mot de Passe Oublié ?</a>
</div>

<!--modal-->
<div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center">Mot de Passe Oublié ?</h1>
      </div>
      <div class="modal-body">
          <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                          
                          <p>Entrez Adresse E-mail.</p>
                            <div class="panel-body">
                                <fieldset>
                                    <form action="mdpoublie" method="post">
                                    <div class="form-group">
                                        <input class="form-control input-lg" placeholder="Adresse E-mail" name="email" type="email">
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" value="Modifier mot de passe" type="submit">
                                    </form>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Annuler</button>
      </div>  
      </div>
  </div>
  </div>
</div>

  <script src="<?php echo base_url(); ?>js/jquery-2.2.1.js"></script>
   <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
</body>
</html>
