  <!DOCTYPE html>
<?php include 'header.php';  ?> 
 <div class="container">
 	      <div>
     	  	<h3 class="page-header">Gestion des Utilisateurs</h3>
     	  </div>

     	   <div class="col-md-8 col-md-offset-2">
     	   <form method="LINK" action="ajouterutilisateur">
     	   <button type="submit" class="btn btn-success btn-flat">Ajouter Utilisateur</button>
			</form>
     	   <div>
     	  	<h3 class="page-header">Utilisateurs</h3>
     	  </div>

     	   <p></p>
     	   <p></p>
			   <?php if($records!=null) {?>
     	   <div class="panel panel-success ">
				<div class="panel-heading">
					<h3 class="panel-title">Liste des Utilisateurs autorisés</h3>
				</div>
				<div class="panel-body panel-bodyy">
     	   	<table class="table table-hover" >
     	   		
     	   		<thead>
     	   			<tr>
     	   				<th>Nom</th>
						<th>Prenom</th>
     	   				<th>Email</th>
     	   				<th>Type</th>
     	   				<th>Modifier</th>
     	   				<th>Bloquer</th>
     	   			</tr>
     	   		</thead>
     	   		<tbody>
				<?php
				foreach ($records as $rec){
					echo "<tr>";
					echo "<td>".$rec->nom."</td>";
					echo "<td>".$rec->prenom."</td>"; ?>
					 <td><?php echo $rec->email ?></td>
					<?php echo "<td>".$rec->categorie."</td>";?>
   	   				<td><a href="modifierutilisateur?id=<?php echo $rec->email ?>"type="submit" class="btn btn-primary btn-flat btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modifier</a></td>
     	   			<?php if($rec->categorie!="Doyen"){?><td><a type="submit" onclick=" suppr('<?php echo $rec->email ?>')" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Bloquer</a></td><?php } ?>
					</tr>
				<?php
				      					}
				?>

				<script>
					function suppr(e) {

						if(confirm("êtes vous sûr de vouloir bloquer cet utilisateur ?!"))
							document.location = "supprimerutilisateur?id="+e;
					}
				</script>
     	   		</tbody>
     	   	</table>
     	   	</div>
     	   </div>


			   <?php} if($recordss!=null) {?>
			   <div class="panel panel-danger ">
				   <div class="panel-heading">
					   <h3 class="panel-title">Liste des Utilisateurs bloqués</h3>
				   </div>
				   <div class="panel-body panel-bodyy">
					   <table class="table table-hover" >

						   <thead>
						   <tr>
							   <th>Nom</th>
							   <th>Prenom</th>
							   <th>Email</th>
							   <th>Type</th>
							   <th>Débloquer</th>
						   </tr>
						   </thead>
						   <tbody>
						   <?php
						   foreach ($recordss as $recc){
                               echo "<tr>";
                               echo "<td>".$recc->nom."</td>";
                               echo "<td>".$recc->prenom."</td>"; ?>
                                <td><?php echo $recc->email ?></td>
                               <?php echo "<td>".$recc->categorie."</td>";?>
                                <td><a type="submit" onclick=" deb('<?php echo $recc->email ?>')" class="btn btn-success btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Débloquer</a></td>
                               </tr>
                           <?php
                                                     }
						   ?>

						   <script>
							   function deb(e) {

								   if(confirm("êtes vous sûr de vouloir débloquer cet utilisateur ?!"))
									   document.location = "utilisateur/debloquer?id="+e;
							   }
						   </script>
						   </tbody>
					   </table>
				   </div>
			   </div>

			   <?php }?>
     	   </div>
 </div> 
 <?php include 'footer.php';
 ?>
