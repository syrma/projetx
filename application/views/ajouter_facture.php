﻿
<?php include 'header.php';  ?>
<div class="container">
  <script src="<?php echo base_url();?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/autoNumeric.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($) {
    $('.auto').autoNumeric('init');
});
</script>
    <div>
        <h3 class="page-header" href="facturation">Facturation</h3>
    </div>
    <div class="col-md-6 col-md-offset-3">

        <form action="ajouterfacture/ajouter_facture" method="post" accept-charset="utf-8">
            <div class="form-group">
                <label for="startDate">Date : </label>
                <input id="startDate" name="startDate" type="text" class="form-control" />

                <label for="chapitre">Chapitre:</label>
                <select class="form-control" name="chapitre" id="chapitre">
                    <?php
                    foreach ($recordsc as $rec){ ?>
                        <option>   <?php echo $rec->id?></option>
                    <?php } ?>

                </select>

            </div>
            <div class="form-group">
                <label for="article">Article:</label>
                <select class="form-control" name="article" id="article">
                    <?php
                    foreach ($recordsa as $rec){ ?>
                        <option>   <?php echo $rec->id?></option>
                    <?php } ?>

                </select>

            </div>
            <div class = "form-group checkbox well">
                <div class="btn-group" data-toggle="buttons">
                    <label ><input type="radio" style="..." <?php if($recordss==null) { ?>onclick="funct()" <?php } else{?> onclick="pers();"<?php } ?>name="options" id="option1" value="societe"  checked='checked' >Société</label>
                    <label ><input type="radio" style="..."  <?php if($recordss==null && $recordsp!=null) { ?>onclick="mafonction()" <?php } ?> <?php if($recordsp==null) { ?>onclick="funct()" <?php }else{ ?> onclick="pers();" <?php } ?> name="options" id="option2" value="personne"> Personne </label>

                </div>
            </div>
            <div class="form-group">
                <label   id="type" type="text" for="societe">Societé:</label>
                               <?php if($recordss!=null) { ?>
                <select onclick="ch()" class="form-control" name="soc" id="soc">
                    <?php
                    foreach ($recordss as $rec){ ?>
                        <option  value="<?php echo $rec->id ?>" >   <?php echo $rec->nom?></option>
                    <?php } ?>
                    <option >autre</option>
                </select>
                <?php }  ?>
                <label id="typeper"  style="display:none"  for="societe">Personne:</label>
                <?php if($recordsp!=null) { ?>
                <select  onclick="ch()" style="display:none" class="form-control" name="per" id="per">
                    <?php   foreach ($recordsp as $rec){ ?>
                        <option  value="<?php echo $rec->id ?>" >   <?php echo $rec->nom?></option>
                    <?php } ?>
                    <option  >autre</option>
                </select>
                <?php } ?>
                </div>
            <div>
                <input type="text" <?php if($recordss==null && $recordsp!=null) { ?>style="display:block" <?php } ?> <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?>class="form-control" id="monid" name="societe" placeholder="nom">
                <input type="text" style="display:none" id="autresoc" name="autresoc">
            </div>
            <div class="form-group">
                <label for="adrS" style="display:none"<?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> id="prenomlab" >Prenom:</label>
            </div>
            <div>
                <input type="text" style="display:none"  <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> class="form-control" id="prenom" name="prenom" placeholder="prénom">
            </div>
            <div class="form-group">
                <label for="adrS" <?php if($recordss==null && $recordsp!=null) { ?>style="display:block" <?php } ?> <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> id="adr" >Adresse:</label>
                </div>

            <div>
                <input type="text"<?php if($recordss==null && $recordsp!=null) { ?>style="display:block" <?php } ?>  <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> class="form-control" id="adrS" name="adrS" placeholder="Adresse ">
            </div>
            <div class="form-group">
                <label for="compte" <?php if($recordss==null && $recordsp!=null) { ?>style="display:block" <?php } ?> <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> id="cmpt">N°Compte:</label>
                <input type="text" <?php if($recordss==null && $recordsp!=null) { ?>style="display:block" <?php } ?> <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> class="form-control" id="compte" name="compte" placeholder="numero de compte ">

            </div>
            <div class="form-group">
                <label for="nomB" <?php if($recordss==null && $recordsp!=null) { ?>style="display:block" <?php } ?> <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> id="nmb">Nom Banque:</label>
                <input type="text" <?php if($recordss==null && $recordsp!=null) { ?>style="display:block" <?php } ?> <?php if($recordss!=null || $recordsp!=null) { ?>style="display:none" <?php } ?> class="form-control" id="nomB" name="nomB" placeholder="nom de la Banque ">

            </div>
            <div class="form-group">
                <label for="NFacture">N°Facture:</label>
                <input type="num" class="form-control" id="NFacture" name="NFacture" placeholder="numero de facture">

            </div>
            <div class="form-group">
                <label for="montant">Montant de la facture:</label>
                <input type="text" class="form-control auto" id="montant" name="montant" placeholder="montant de la facture" data-a-sep=" " data-a-dec="," data-a-sign="D.A ">
            </div>


            <div class = "form-group checkbox well" id="choixfiche" style="display:block">
                <div class="btn-group" data-toggle="buttons">
                    <label ><input type="radio" style="display:block"name="opt" id="opt2" checked='checked' onclick="fiche()" value="mandat"> mandat de paiment + avis de virement </label>
                    <label ><input type="radio" style="display:block"name="opt" id="opt1" onclick="fiche()" value="ch50"> CH-50</label>

                </div>
            </div>
            <div class="form-group">
                <label type="text" name="mndt" id="mndt" for="mondat">N° Mandat:</label>
                <input type="number" class="form-control" id="mendat" name="mendat" placeholder="numero du mandat">

            </div>
            <div class="form-group">
                <label type="text" name="ribl" id="ribl" for="rib">RIB :</label>
                <input type="text" class="form-control" id="rib" name="rib" placeholder="RIB de la société">

            </div>
            <button type="submit" class="btn btn-top btn-success col-md-8 col-md-offset-2 btn-flat">Enregistrer</button>

        </form>
        <?php
        $msg = $this->session->flashdata('msg')['msg'];

        echo $msg;
        ?>
    </div>

</div>


 <script src="<?php echo base_url();?>js/jquery.min.js"></script>
 <script type = "text/javascript"> 
 $('#chapitre').change(function() {  
           var $chapitre = $('#chapitre').val();
           $.ajax({
             url: 'articlesajax?idc='+$chapitre,
             type: 'GET',
             dataType: 'json',
             success: function(result) {
                 var $article = $('#article');
                 var records = result.recordsa;
                 var options_str = "";
                 records.forEach( function(rec) {
                     options_str += '<option value="' + rec.id + '">' + rec.id +
 '</option>';
                 });
 
                 $article.html(options_str);
                 }
                 });
 });
 </script>





<script src="<?php echo base_url();?>js/facturation.js"></script>
<script src="<?php echo base_url();?>js/perssoc.js"></script>
<script src="<?php echo base_url();?>js/choixfiche.js"></script>
<script src="<?php echo base_url();?>js/mafonction.js"></script>
<script src="<?php echo base_url();?>js/personne.js"></script>
<script src="<?php echo base_url();?>js/personnefacturation.js"></script>



<?php include 'footer.php';
?>
