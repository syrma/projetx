
<?php include 'header.php';  ?>
		<div class="container">
 			<div>
            <h3 class="page-header">Profile</h3>
            </div>
        
        <div class=" col-md-6 col-md-offset-3">

               	<h5 class="well "><?php echo $profil[0]->prenom." ".$profil[0]->nom ?></h5>

     	  	  	  <form action="profil/modifier" method="post" role="form">
                    
                    <div class="form-group">
                            <label for="email">Email: </label>
                            <input type="email" class="form-control" name="email" id="email" value="<?php echo $profil[0]->mail?>">
                    
                    </div>
                      <div class="form-group">
                          <label for="pwd">Question secrète:</label>
                          <input type="text" class="form-control" name="qs" id="qs" value="<?php echo $profil[0]->question?>">

                      </div>
                      <div class="form-group">
                          <label for="pwd">Réponse secrète:</label>
                          <input type="text" class="form-control" name ="rep" id="rep" value="<?php echo $profil[0]->reponse ?>">

                      </div>
                    <div class="form-group">
                            <label for="pwd">nouveau mot de passe:</label>
                            <input type="password" class="form-control" name="mdp1"id="mdp1" placeholder="mot de passe">
                    
                    </div>
                    <div class="form-group">
                            <label for="sq">confirmer mot de passe:</label>
                            <input type="password" class="form-control" name="mdp2"id="mdp2" placeholder="mot de passe">
                    
                    </div>
                    
                        <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Envoyer</button>
                </form>
            <?php
            $msg = $this->session->flashdata('msg')['msg'];
            echo $msg ?>
     	  	  </div>
              </div>
     	  
 


<?php include 'footer.php';
?>
