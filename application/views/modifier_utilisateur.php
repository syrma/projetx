<!DOCTYPE html>
<?php include 'header.php';  ?>

<div class="container">
    <div>
        <h3 class="page-header">Modifier Utilisateur</h3>
        <?php
        $msg = $this->session->flashdata('msg')['msg'];
        echo $msg

        ?>

        <div class=" col-md-8 col-md-offset-2">

            <form role="form" action="modifierutilisateur/modifier?id=<?php echo $id ?>" method="post" accept-charset="utf-8">

                <div class="form-group">
                    <label for="nom">Nom:</label>
                    <input type="nom" class="form-control" id="nom" name="nom"  value="<?php echo $utilisateur[0]->nom ?>">
                </div>
                <div class="form-group">
                    <label for="prenom">Prenom:</label>
                    <input type="prenom" class="form-control" id="prenom" name="prenom"  value="<?php echo $utilisateur[0]->prenom ?>">
                </div>


                <div class="form-group">
                    <label for="pwd">Categorie:</label>

                    <select class="form-control" name="ctg" id="ctg">
                            <option><?php echo $utilisateur[0]->categorie ?></option>
                        <?php if($utilisateur[0]->categorie!="Doyen") ?>  <option >Doyen</option>
                        <?php if($utilisateur[0]->categorie!="Secretaire general") ?>  <option >Secretaire general</option>
                        <?php if($utilisateur[0]->categorie!="Responsable") ?>  <option >Responsable</option>
                        <?php if($utilisateur[0]->categorie!="Agent") ?>  <option >Agent</option>


                    </select>
                </div>

                <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Modifier</button>
            </form>
        </div>

    </div>


</div>

<?php include 'footer.php';
?>

    
    
