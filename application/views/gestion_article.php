<!DOCTYPE html>
<?php include 'header.php';  ?>
  <div class="container">
          <div>
            <h3 class="page-header">Gestion des Articles</h3>
          </div>
    <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

    <?php  if ($en==1||$en==0)
        { include 'menu_engagement.php'; } ?>
                  <div class="col-lg-10 col-lg-offset-0">
                      <a class="btn btn-success btn-flat "href="ajouterarticle?id=<?php echo $id; ?>">Ajouter Article</a>
                  </div>
               <?php } ?>
    <?php if($records!=null){ ?>
      <div class="col-md-12">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title"><?php echo $chapitree[0]->nom ?></h3>
          </div>
          <div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Code de L'article</th>
                              <th>Budget de L'article</th>
                              <th>Detail</th>
                                <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

                                <?php if ($en==1||$en==0)
                                    { ?>
                              <th>Modifier</th>
                              <th>Supprimer</th>
                                <?php } }?>
                              </tr>
                          </thead>
                          <tbody>
                          <?php foreach($records as $row){ ?>
                              <tr>
                              <td><?php echo $row->id ?></td>
                              <td><?php echo $row->budget ?></td>
                              <td><a href="detailarticle?id=<?php echo $row->id ?>&idc=<?php echo $id ?>" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Detail</a></td>
                                  <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

                                  <?php if ($en==1||$en==0)
                                      { ?>
                                  <td><a href="modifierarticle?id=<?php echo $row->id ?>&idc=<?php echo $id ?>" class="btn btn-primary btn-flat btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modifier</a></td>
                              <td><a onclick="deb('<?php echo $row->id ?>&idc=<?php echo $id ?>')" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Supprimer</a></td>
                                  <?php }} ?>
                              </tr>
                          <?php } ?>

                          </tbody>
                      </table>
              <script>
                  function deb(e) {

                      if(confirm("êtes vous sûr de vouloir supprimer cet article ?!"))
                          document.location = "gestionarticles/supprimer?id="+e;
                  }
              </script>
             
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
    <?php } else { ?>
        <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">il n y a aucun article a afficher pour ce chapitre</div>
    <?php }?>
      <!-- 9/ -->
    <?php
    $msg = $this->session->flashdata('msg')['msg'];
    echo $msg ?>
  </div> 
  <!-- container/ -->
 <?php include 'footer.php';
 ?>
