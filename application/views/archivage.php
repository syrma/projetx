<?php include 'header.php';  ?>
 	<div class="container">
 			<div>
     	  	<h3 class="page-header">Archive</h3> 
     	 	</div>
     	 	<div class="col-md-12">
     	 		<h3>Factures</h3> 
     	 	<?php if($factures != null){ ?>
     	 	<div class="panel panel-success panel-successs ">
          	<div class="panel-heading">
          	<h3 class="panel-title">Factures</h3>
                </div>
     	 	<div class="panel-body panel-bodyy">
     	 		<table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Date</th>
                              <th>Montant</th>
                              <th>Fournisseur</th>
                              <th>Detail</th>
                              </tr>
                          </thead>
                          <tbody>
                    <?php foreach($factures as $rec) { ?>
                        <tr>
                            <td><?php echo $rec->date ; ?></td>
                            <td><?php echo $rec->montant ; ?></td>
                            <td><?php echo $rec->fournisseur ; ?></td>
                            <td><a href="detailarchfacture?id=<?php echo $rec->num; ?>"type="submit" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Detail</a></td>
                            <?php } ?>                              
                              </tr>
                          </tbody>
                      </table>   
                      </div> 
                      </div>
                   <?php }else{ ?>
                     <div class="panel-body panel-bodyy">
                     <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert"> Il n'y a aucune facture à afficher.</div>
                     </div>
                   <?php } ?>       

     	  	<h3 class="page-header"></h3> 
     	 	
     	 	<h3>Transferts</h3>
     	 	<?php if($transferts != null){ ?>
     	 	<div class="panel panel-success panel-successs ">
          	<div class="panel-heading">
          	<h3 class="panel-title">Transferts</h3>
          </div>
     	 	<div class="panel-body panel-bodyy">
     	 		<table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Chapitre source</th>
                              <th>Article source</th>
                              <th>Chapitre cible</th>
                              <th>Article cible</th>
                              <th>Montant</th>
                              <th>Date</th>
                              <th>Auteur</th>
                            </tr>
                          </thead>
                          <tbody>
                        <?php foreach($transferts as $row) {?>
                            <tr>
                                <td><?php echo $row->chapitre_source ?></td>
                                <td><?php echo $row->article_source ?></td>
                                <td><?php echo $row->chapitre_cible ?></td>
                                <td><?php echo $row->article_cible ?></td>
                                <td><?php echo $row->montant_transfert ?>.00</td>
                                <td><?php echo $row->date_transfert ?></td>
                                <td><?php echo $row->prenom." ".$row->nom ?></td>
                            </tr>
                        <?php } ?>
                              </tr>
                          </tbody>
                      </table>   
                      </div> 
                      </div>
                  <?php }else{ ?>
                       	 	<div class="panel-body panel-bodyy">
                  <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert"> Il n'y a aucun transfert à afficher.</div>
                  </div>
                  <?php } ?>
                  
                      
                      <?php
                      $msg = $this->session->flashdata('msg')['msg'];
                      echo $msg ?>
                      
<h3 class="page-header"></h3> 
<h3>Situation Financiere</h3>

                      <a href="datesituationfinanciere?arch=1" target ="_blank" class="circ">
                      <i class="circ pulse"></i>
			<img  src="<?php echo base_url();?>assets/img/flat-icons/buildings.png" alt="">
			</a>
     	 	</div>

 	</div>
<?php include 'footer.php';
?>
