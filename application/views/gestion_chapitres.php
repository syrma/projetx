<!DOCTYPE html>
<?php include 'header.php';  ?>
<div class="container">
    <div>
        <h3 class="page-header">Gestion des Chapitres et Articles</h3>
    </div>


    <?php if ($en==1||$en==0)
    {?> <?php }
    ?>
    <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>


        <div class="list-group col-md-3 ">
            <a href="transfert" class="list-group-item">Transferts</a>


        </div> <?php } if($this->session->userdata('categorie')!="Responsable"&&$this->session->userdata('categorie')!="Agent"){
        ?>

        <div class="list-group col-md-3 ">

            <a href="consultertransferts" class="list-group-item">Transferts</a>
        </div>
        <?php
    }  ?>


    <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

        <div class="col-md-3">
            <a class="btn btn-success btn-flat "href="ajouterchapitre">Ajouter Chapitre</a>
        </div>
    <?php } ?>
    <?php if($records!=null){ ?>

        <div class="col-md-9">
            <div class="panel panel-success panel-successs  ">
                <div class="panel-heading">
                    <h3 class="panel-title">Gestion des Chapitres</h3>
                </div>
                <div class="panel-body panel-bodyy">

                    <table class="table table-hover" >
                        <thead>
                        <tr>
                            <th>Code du Chapitre</th>
                            <th>Budget du Chapitre</th>
                            <th>Articles du chapitre</th>
                            <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

                                <?php if ($en==1||$en==0)
                                { ?>
                                    <th>Modifier</th>
                                    <th>Supprimer</th>
                                <?php } }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($records as $row) { ?>
                            <tr>
                            <td><?php echo $row->id ?></td>
                            <td><?php echo $row->budget ?></td>
                            <td><a href="gestionarticles?id=<?php echo $row->id ?>&en=<?php echo $en ?>" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Articles</a></td>
                            <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

                                <?php  if ($en==1||$en==0)
                                { ?>
                                    <td><a href="modifierchapitre?id=<?php echo $row->id ?>" class="btn btn-primary btn-flat btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modifier</a></td>
                                    <td><a onclick="deb('<?php echo $row->id ?>')" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Supprimer</a></td>
                                <?php } ?>

                                </tr><?php } }?>
                        </tbody>
                    </table>
                    <script>
                        function deb(e) {

                            if(confirm("tes vous sr de vouloir supprimer ce chapitre ?!"))
                                document.location = "gestionchapitres/supprimer?id="+e;
                        }
                    </script>

                </div>


                <!-- panel-body/ -->
                <?php
                $msg = $this->session->flashdata('msg')['msg'];
                echo $msg ?>
            </div>
            <?php if ($en==1 && $sem==1)
            { ?>
                <div class="row col-md-12 well">
                    <a class="btn btn-default btn-flat "href="FRattachement"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Rattachement</a>
                    <a class="btn btn-default btn-flat "href="F_EProvi?sem=1"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Engagement Provisionnel 1er semestre</a>
                    <a class="btn btn-default btn-flat "href="FicheRegularisation?sem=1"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Fiche de Regularisation 1er semestre</a>

                </div>
            <?php }
            else{?>
            <?php    if($en==1 && $sem==2){ ?>
                    <div class="row col-md-12 well">
                        <a class="btn btn-default btn-flat "href="FRattachement"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Rattachement</a>
                        <a class="btn btn-default btn-flat "href="F_EProvi?sem=1"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Engagement Provisionnel 1er semestre</a>
                        <a class="btn btn-default btn-flat "href="F_EProvi?sem=2"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Engagement Provisionnel 2eme semestre</a>
                        <a class="btn btn-default btn-flat "href="FicheRegularisation?sem=1"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Fiche de Regularisation 1er semestre</a>
                        <a class="btn btn-default btn-flat "href="FicheRegularisation?sem=2"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Fiche de Regularisation 2eme semestre</a>
                    </div>

                <?php } }?>
            <!-- success/ -->
        </div>
        <!-- 9/ -->
    <?php } else { ?>
        <div class="alert alert-danger col-md-6 col-md-offset-1" role="alert">il n y a aucun chapitre a afficher</div>
    <?php }?>
</div>
<!-- container/ -->
<?php  include 'footer.php';
?>
