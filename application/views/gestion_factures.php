<?php include 'header.php';  ?>

<div class="container">

    <div class="row col-md-8 col-md-offset-2">

        <ul class="list-items">
            <li class="list text-center">
                <a href="facturee" class="circ">
                    <!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
                    <i class="circ pulse" ></i>
                    <img  src="<?php echo base_url();?>assets/img/flat-icons/loading.png" alt="">
                </a>
                <h3 class="">Factures en cours</h3>
            </li>
            <li class="list text-center">
                <a href="facturesconfirmees" class="circ">
                    <!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->

                    <i class="circ pulse"></i>
                    <img  src="<?php echo base_url();?>assets/img/flat-icons/confirm-schedule.png" alt="">
                </a>
                <h3 class="">Factures confirmées</h3>

            </li>
            <li class="list text-center">
                <a href="facturesvalidees" class="circ">
                    <!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->

                    <i class="circ pulse"></i>
                    <img  src="<?php echo base_url();?>assets/img/flat-icons/checked.png" alt="">
                </a>
                <h3 class="">Factures validées</h3>

            </li>




        </ul>



    </div>


</div>
<?php include 'footer.php';
?>
