
<?php include 'header.php';  ?>
<div class="container">
    <div>
        <h3 class="page-header" href="facturation">Facturation</h3>
    </div>
    <div class="col-md-6 col-md-offset-3">

        <form action="ajouteravis/ajouter" method="post" accept-charset="utf-8">
            <div class="form-group">
                <label for="startDate">Date : </label>
                <input id="startDate" name="startDate" type="text" class="form-control" />

                <label for="chapitre">Chapitre:</label>
                <select class="form-control" name="chapitre" id="chapitre">
                    <?php
                    foreach ($recordsc as $rec){ ?>
                        <option>   <?php echo $rec->id?></option>
                    <?php } ?>

                </select>

            </div>
            <div class="form-group">
                <label for="article">Article:</label>
                <select class="form-control" name="article" id="article">
                    <?php
                    foreach ($recordsa as $rec){ ?>
                        <option>   <?php echo $rec->id?></option>
                    <?php } ?>

                </select>

            </div>

            <div class="form-group">

                <label id="typeper"   for="societe">Type:</label>

                    <select  class="form-control" name="per" id="per">

                            <option > Stage</option>
                        <option >Séjour scientifique</option>
                        <option >Remboursement de frais</option>
                        <option >Manifestation scientifique</option>
                        <option >Visa</option>
                        <option >Assurence</option>


                    </select>

            </div>

            <div class="form-group">
                <label for="adrS" style="display:none" id="adr" >Adresse:</label>
            </div>
            <div>
                <input type="text"   class="form-control" id="adrS" name="adrS" placeholder="Adresse de La personne">
            </div>
            <div class="form-group">
                <label for="compte" id="cmpt">N°Compte:</label>
                <input type="text"  class="form-control" id="compte" name="compte" placeholder="numero de compte ">

            </div>
            <div class="form-group">
                <label for="nomB" id="nmb">Nom Banque:</label>
                <input type="text" class="form-control" id="nomB" name="nomB" placeholder="nom de la Banque ">

            </div>

            <div class="form-group">
                <label for="montant">Montant du virement:</label>
                <input type="num" class="form-control" id="montant" name="montant" placeholder="montant de la facture">

            </div>

            <div class="form-group">
                <label type="text" name="ribl" id="ribl" for="rib">RIB :</label>
                <input type="text" class="form-control" id="rib" name="rib" placeholder="RIB de la société">

            </div>
            <button type="submit" class="btn btn-top btn-success col-md-8 col-md-offset-2 btn-flat">Enregistrer</button>

        </form>
        <?php
        $msg = $this->session->flashdata('msg')['msg'];

        echo $msg;
        ?>
    </div>

</div>


<script src="<?php echo base_url();?>js/jquery.min.js"></script>
<script type = "text/javascript">
    $('#chapitre').change(function() {
        var $chapitre = $('#chapitre').val();
        $.ajax({
            url: 'articlesajax?idc='+$chapitre,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                var $article = $('#article');
                var records = result.recordsa;
                var options_str = "";
                records.forEach( function(rec) {
                    options_str += '<option value="' + rec.id + '">' + rec.id +
                        '</option>';
                });

                $article.html(options_str);
            }
        });
    });
</script>





<script src="<?php echo base_url();?>js/facturation.js"></script>
<script src="<?php echo base_url();?>js/perssoc.js"></script>
<script src="<?php echo base_url();?>js/choixfiche.js"></script>
<script src="<?php echo base_url();?>js/mafonction.js"></script>
<script src="<?php echo base_url();?>js/personne.js"></script>
<script src="<?php echo base_url();?>js/personnefacturation.js"></script>



<?php include 'footer.php';
?>
