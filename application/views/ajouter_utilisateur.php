<!DOCTYPE html>
<?php include 'header.php';  ?>

     <div class="container">
     	  <div>
     	  	<h3 class="page-header">Ajouter Utilisateur</h3>
              <?php
              $msg = $this->session->flashdata('msg')['msg'];
              echo $msg

              ?>

              <div class=" col-md-8 col-md-offset-2">

     	  	  	  <form role="form" action="ajouterutilisateur/ajouter" method="post" accept-charset="utf-8">

                    <div class="form-group">
                            <label for="nom">Nom:</label>
                            <input type="nom" class="form-control" id="nom" name="nom"  placeholder="nom">
                    </div>
                    <div class="form-group">
                            <label for="prenom">Prenom:</label>
                            <input type="prenom" class="form-control" id="prenom" name="prenom"  placeholder="prenom">
                    </div>
                    <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email"  placeholder="email">
                    
                    </div>
                    <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" name="pwd" placeholder="password">
                    
                    </div>
                      <div class="form-group">
                          <label for="pwd">Categorie:</label>

                          <select class="form-control" name="ctg" id="ctg">
                              <?php if ($ctg=="Doyen") {?>
                              <option  >Doyen</option>
                              <option >Secretaire general</option><?php } ?>
                              <?php if ($ctg=="Secretaire general"){ ?>
                              <option  >Responsable</option><?php } ?>
                              <?php if ($ctg=="Responsable") {?>
                              <option   >Agent</option> <?php } ?>


                          </select>
                      </div>
                    <div class="form-group">
                            <label for="sq">Question Secrete:</label>
                        <?php if($qsts!=null) {?>
                            <select onclick="fctq()" class="form-control" name="qs" id="qs">
                                <?php
                                foreach ($qsts as $rec){ ?>
                                    <option  value="<?php echo $rec->id ?>" >   <?php echo $rec->enonce?></option>
                                <?php } ?>
                                <option >autre</option>
                            </select>
                        <?php }?>
                            <input style="display:none" type="text" class="form-control" id="qsa" name="qsa"  placeholder="question secrete">
                        <input style="display:none" type="text" class="form-control" id="autre" name="autre"  value="">

                    </div>
                    <div class="form-group">
                            <label for="rs">Reponse Secrete:</label>
                            <input type="rs" class="form-control" id="rs" name="rs" placeholder="reponse secrete">

                    </div>

                    


                        <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Ajouter</button>
                </form>
     	  	  </div>

     	  </div>
     		
     	  	
     </div>
<script src="<?php echo base_url();?>js/questions.js"></script>

  <?php include 'footer.php';
  ?>


    
