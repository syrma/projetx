<?php include 'header.php';  ?>
<div class="container">
			<div>
            <h3 class="page-header">Details Aticle</h3>
            </div> 

			<div class="col-md-12">
       			 <div class="panel panel-success panel-successs  ">
          			<div class="panel-heading">
          				<h3 class="panel-title">Article</h3>
          			</div>
          		<div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Nom de L'article</th>
                              <th>Budget initial</th>
                               <th>Budget courant</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php foreach($details as $row) {?>
                              <tr>
                              <td><?php echo $row->nom ?></td>
                              <td><?php echo $row->budget_initial ?>.00</td>
                              <td><?php echo $row->budget ?></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                    </div>
                    </div>
                    </div>

                    <div class="col-md-12">
       			 <div class="panel panel-success panel-successs  ">
          			<div class="panel-heading">
          				<h3 class="panel-title">Transferts</h3>
          			</div>
          		<div class="panel-body panel-bodyy">

                    <table class="table table-hover" >
                        <thead>
                        <tr>
                            <th>Montant</th>
                            <th>Article source</th>
                            <th>Chapitre source</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($cible as $row) {?>
                        <tr>
                            <td>+ <?php echo $row->montant ?></td>
                            <td><?php echo $row->article ?></td>
                            <td><?php echo $row->chapitre ?></td>

                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    </div>
                    </div>
                    </div>


    <div class="col-md-12">
        <div class="panel panel-danger panel-successs  ">
            <div class="panel-heading">
                <h3 class="panel-title">Transferts</h3>
            </div>
            <div class="panel-body panel-bodyy">

                <table class="table table-hover" >
                    <thead>
                    <tr>
                        <th>Montant</th>
                        <th>Article cible</th>
                        <th>Chapitre cible</th>
                    </tr>
                    </thead>
                    <tbody>
<?php foreach($source as $row) {?>
                    <tr>
                        <td>- <?php echo $row->montant ?></td>
                        <td><?php echo $row->article ?></td>
                        <td><?php echo $row->chapitre ?></td>

                    </tr>
<?php } ?>
                    </tbody>
                </table>
                </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
      <!-- 9/ -->
</div>
<?php include 'footer.php';
?>
