<!DOCTYPE html>
<?php include 'header.php';  ?>
<div class="container">


                <div class="col-md-14">

                    <div class="panel panel-success panel-successs  ">
                        <div class="panel-heading">
                            <h3 class="panel-title">Factures</h3>
                        </div>
                        <div class="panel-body panel-bodyy">
                <table class="table table-hover" >

                    <thead>
                    <tr>
                        <th>Numero</th>
                        <th>Chapitre</th>
                        <th>Article</th>
                        <th>Montant</th>
                        <th>Fournisseur</th>
                        <th>Date</th>
                        <th>Auteur</th>
                        <?php if(!isset($arch)) echo "<th>Details chapitre</th>";?>
                        
                        <th>Details article</th>
                        <th>Details fournisseur</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $contra = "detailarticle";
                        $contrf = "fournisseurs";
                        if(isset($arch) && $arch == 1){
                            $contra = "detailarcharticle";
                            $contrf = "fournisseursarch";
                            }
                    ?>
                    
                    <?php foreach ($records as $row) {
     	   		      echo "<tr>";
     	   		      echo "<td>".$row->num."</td>";
     	   		      echo "<td>".$row->chap ."</td>";
                              echo "<td>".$row->art."</td>";
                              echo "<td>".$row->mont."</td>";
                              echo "<td>".$row->four."</td>";
                              echo "<td>".$row->date."</td>";
                              echo "<td>".$row->auteur."</td>";
                          if(!isset($arch)){
                             ?>
                      <td><a href="gestionarticles?id=<?php echo $row->chap ?>&en=0" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Detail</a></td>
                      <?php } ?>
                     <td><a href="<?php echo $contra ?>?id=<?php echo $row->art ?>&idc=<?php echo $row->chap ?>" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Detail</a></td>
                   <td><a href="<?php echo $contrf?>?id=<?php echo $row->id_fournisseur ?>" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Detail</a></td>

     	              </tr>
     	   			<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';
?>
