<!DOCTYPE html>
<html>

<head>
    <title>Transfert</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
</head>
<body>

    <button onclick="chap()"  data-target="#pwdModal" data-toggle="modal" class="btn btn-top btn-success col-md-6 col-md-offset-3 btn-flat">Transferer</button>
<!--modal-->
<div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1 class="text-center">Confirmer ?</h1>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">

                                <p>Vérifier votre transfert</p>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="panel-body panel-bodyy">

                                            <table class="table table-hover" id="tableau">
                                                <thead>
                                                <tr >
                                                    <th>Chapitre source</th>
                                                    <th>Article source</th>
                                                    <th>Chapitre cible</th>
                                                    <th>Article cible</th>
                                                    <th>Montant</th>


                                                </tr>
                                                </thead>
                                                <tbody >

                                                </tbody>
                                            </table>
                                        </div>
                                        <form action="transfert/transferer" method="post">
                                            <input type="hidden" id="cs" name="idcd">
                                            <input type="hidden" id="as" name="idad">
                                            <input type="hidden" id="cc" name="idca">
                                            <input type="hidden" id="ac" name="idaa">
                                            <input type="hidden" id="mnt"name="montant">
                                        <input class="btn btn-lg btn-success btn-block" value="Transférer" type="submit">
                                        </form>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <form action="transfert" method="post">
                    <button type="submit" class="btn" >Annuler</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>js/jquery-2.2.1.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
</body>
</html>
