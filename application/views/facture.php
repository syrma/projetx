  <!DOCTYPE html>
<?php include 'header.php';  ?> 
 <div class="container">
	  <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

	  <div>
     	  	<h3 class="page-header">Facturation</h3>
     	  </div>
       <?php } ?>
     	   <div class="col-md-10 col-md-offset-1">
			   <?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

     	   <a href="ajouterfacture" class="btn btn-success btn-flat">Ajouter Facture</a>

	  <?php }else{?>

	  <div>
     	  	<h3 class="page-header">Factures</h3>
     	  </div>

     	   <p></p>
     	   <p></p>
	  <?php } if($records!=null){?>
     	   <div class="panel panel-success ">
				<div class="panel-heading">
					<h3 class="panel-title">Liste des Factures</h3>
				</div>
				<div class="panel-body panel-bodyy">
     	   	<table class="table table-hover" >
     	   		
     	   		<thead>
     	   			<tr>
     	   				<th>code facture</th>
     	   				<th>code chapitre</th>
     	   				<th>Date</th>
     	   				<th>Montant</th>
     	   				<th>Fournisseur</th>
                              <th>Detail</th>
						<?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>
                              <th>Modifier</th>
							<th>Confirmer</th>
						<?php } ?>
     	   			</tr>
     	   		</thead>
     	   		<tbody>
				<?php foreach($records as $rec) { ?>
     	   			<tr>
     	   				<td><?php echo $rec->id ; ?></td>
     	   				<td><?php echo $rec->id_chapitre ; ?></td>
                              <td><?php echo $rec->date ; ?></td>
                              <td><?php echo $rec->montant ; ?></td>
                              <td><?php echo $rec->fournisseur ; ?></td>
     	   				<td><a href="detailfacture?id=<?php echo $rec->num; ?>"type="submit" class="btn btn-info btn-flat btn-sm"> <span class="glyphicon glyphicon-folder-open " aria-hidden="true"></span> Detail</a></td>
						<?php if($this->session->userdata('categorie')=="Responsable"||$this->session->userdata('categorie')=="Agent"){?>

						<td><a href="modifierfacture?id=<?php echo $rec->num; ?>" type="submit" class="btn btn-primary btn-flat btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modifier</a></td>
							<td><a  onclick=" conf('<?php echo $rec->num ?>')" type="submit" class="btn btn-success btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Confirmer</a></td>

						<?php } ?>
						<script>
							function conf(e) {

								if(confirm("êtes vous sûr de vouloir confirmer cette facture ?!"))
									document.location = "facturee/confirmer?id="+e;
							}
						</script>

					</tr>
     	   			<?php } ?>
     	   		</tbody>
     	   	</table>


				</div>
     	   </div>
			   <?php
			   $msg = $this->session->flashdata('msg')['msg'];

			   echo $msg;
			   ?>
 </div>
	  <?php } else { ?>
	  </div>
		  <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">il n y a aucune facture à afficher </div>
	  <?php }?>
 </div>
 <?php include 'footer.php';
 ?>
