<!DOCTYPE html>
<?php include 'header.php';  ?>

  <div class="container">
  <script src="<?php echo base_url();?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/autoNumeric.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($) {
    $('.auto').autoNumeric('init');
});
</script>
  	 		  <div>
     	  	   <h3 class="page-header">Les Transferts</h3>
     	    </div>
    <div class="list-group col-md-3 ">
        <a href="gestionchapitres?en=0" class="list-group-item">Gestion des Chapitre et articles</a>

    </div>

 	    <div class="col-md-9">
            <div class="panel panel-success ">
                  <div class="panel-heading">
                        <h3 class="panel-title">Transfert</h3>
                  </div>
                  <div class="panel-body">
                        <form action="transfert/transferer" id = "formT" method="post" accept-charset="utf-8">
                        <div class="  input-group col-md-9 ">
                            <span class="input-group-addon">Montant</span>
                            <input type="text" class="form-control auto" id="montant" name ="montant"placeholder="Entrer montant" data-a-sep=" " data-a-dec="," data-a-sign="D.A ">
                         </div> 
                            <h3>Chapitres :</h3>
           <div class="row">
                <div class="col-md-1">
                            <p>De : </p>
                </div>
                <div class="col-md-4">
                    <select class="form-control" id="idcd" name="idcd">
                        <?php
                        foreach ($chapitres as $chapd){ ?>
                            <option>   <?php echo $chapd->id?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-1">
                              <p>a : </p>
                </div>
                <div class="col-md-4">
                    <select class="form-control" id="idca" name="idca">
                        <?php
                        foreach ($chapitres as $chapa){ ?>
                            <option>   <?php echo $chapa->id?></option>
                        <?php } ?>
                    </select>
            </div>
          </div>
                          <h3>Articles :</h3>
             <div class="row">
                <div class="col-md-1">
                  <p>De : </p>
                </div>
                <div class="col-md-4">     
                    <select class="form-control" id="idad" name="idad">
                        <?php
                        foreach ($articles as $artd){ ?>
                            <option>   <?php echo $artd->id?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-1">
                  <p>a : </p>
                </div>
                <div class="col-md-4">
                    <select class="form-control" id="idaa" name="idaa">

                        <?php

                        foreach ($articles as $arta){ ?>

                            <option>   <?php echo $arta->id?>  </option>
                        <?php } ?>
                    </select>
            </div>
          </div>

        </form>
                      <?php include 'annuler_transfert.php';  ?>
              </div>
                
        </div>
    </div>
       <?php
$msg = $this->session->flashdata('msg')['msg'];
echo $msg ?>


  </div>

<script src="<?php echo base_url();?>js/jquery.min.js"></script>

 <script type = "text/javascript"> 
 $('#idcd').change(function() {  
           var $chapitre = $('#idcd').val();
           $.ajax({
             url: 'articlesajax?idc='+$chapitre,
             type: 'GET',
             dataType: 'json',
             success: function(result) {
                 var $article = $('#idad');
                 var records = result.recordsa;
                 var options_str = "";
                 records.forEach( function(rec) {
                     options_str += '<option value="' + rec.id + '">' + rec.id +
 '</option>';
                 });
 
                 $article.html(options_str);
                 }
                 });
 });
 </script>
 
  <script type = "text/javascript"> 
 $('#idca').change(function() {  
           var $chapitre = $('#idca').val();
           $.ajax({
             url: 'articlesajax?idc='+$chapitre,
             type: 'GET',
             dataType: 'json',
             success: function(result) {
                 var $article = $('#idaa');
                 var records = result.recordsa;
                 var options_str = "";
                 records.forEach( function(rec) {
                     options_str += '<option value="' + rec.id + '">' + rec.id +
 '</option>';
                 });
 
                 $article.html(options_str);
                 }
                 });
 });
 </script>

  </div>
<script src="<?php echo base_url();?>js/transfert.js"></script>

<script type = "text/javascript">
    function chap()
    {

        var tableau = document.getElementById("tableau");

        var ligne = tableau.insertRow(-1);//on a ajouté une ligne

        var colonne1 = ligne.insertCell(0);//on a une ajouté une cellule
        colonne1.innerHTML += document.getElementById("idcd").value;//on y met le contenu de titre

        var colonne2 = ligne.insertCell(1);//on ajoute la seconde cellule
        colonne2.innerHTML += document.getElementById("idad").value;


        var colonne3 = ligne.insertCell(2);
        colonne3.innerHTML += document.getElementById("idca").value;

        var colonne4 = ligne.insertCell(3);
        colonne4.innerHTML += document.getElementById("idaa").value;

        var colonne5 = ligne.insertCell(4);
        colonne5.innerHTML += document.getElementById("montant").value;

        document.getElementById("cs").value = document.getElementById("idcd").value;
        document.getElementById("as").value = document.getElementById("idad").value;
        document.getElementById("cc").value = document.getElementById("idca").value;
        document.getElementById("ac").value = document.getElementById("idaa").value;
        document.getElementById("mnt").value = document.getElementById("montant").value;




        document.getElementById('formT').action = "transfert/transferer";
        if(document.getElementById("idcd").value != document.getElementById("idca").value)
        {
            if(!confirm("vous allez transférer d'un chapitre à  un autre !!"))
            {    document.getElementById('formT').action = "#";
            document.location = "transfert";}
        }
    }

</script>


 <?php include 'footer.php';
 ?>

