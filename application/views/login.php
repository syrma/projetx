<!DOCTYPE html>
<html>
<?php
include 'HeaderFactory.php';
include 'FooterFactory.php';
HeaderFactory::addCSS('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
HeaderFactory::addCSS(base_url().'css/style.css');
HeaderFactory::addScript('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js');
$headerHTML = HeaderFactory::get_header();
FooterFactory::addScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
$footerHTML = FooterFactory::get_footer();
echo $headerHTML;
?>
<div class=" container" >
    <div class=" text-center col-md-6 col-md-offset-3">

        <img src="<?php echo base_url();?>assets/img/logo.png" alt ="..." class="img-circle"/>
    </div>

              
        	<div class="panel panel-default well col-md-6 col-md-offset-3 form-primary"> 
        	  <div class="panel-heading form-header">
        	  	<h2 class="box-title">Connexion</h2>	
        	  </div>
        		<form class="panel-body text-center" action="authentification" method="post" accept-charset="utf-8">
        		    <div class="input-group input-group-top">
        		    	<span class="input-group-addon glyphicon glyphicon-user"></span>
        		    	<input class="form-control" type="text" name="username" value="zaidi.sahnoun@univ-constantine2.dz" placeholder="Nom d'utilisateur">
					
        		    </div>
        		    <div class="input-group input-group-top">
        		    	<span class="input-group-addon glyphicon glyphicon-lock"></span>
        		    	<input id="a" class="form-control" type="password" name="password" placeholder="Password">
        		    </div>
        			<div >
        				
                <button class="btn btn-success pull-right input-group"  type="submit">   Se connecter  </button>
        			
              </div>

        	   </form>
             <?php include 'mdp_o.php';  ?>
        	</div>
          <?php 
          echo $msg;
           ?>
	<?php
	$auth = $this->session->flashdata('errauth')['msg'];
	echo $auth

	?>
        </div>    
              
   <?php echo $footerHTML; ?>
     
 
 
</html>
