
<?php include 'header.php';  ?>
  <script src="<?php echo base_url();?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/autoNumeric.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($) {
    $('.auto').autoNumeric('init');
});
</script>
<div class="container">
    <div>
        <h3 class="page-header" href="facturation">Facturation</h3>
    </div>
    <div class="col-md-6 col-md-offset-3">

        <form action="Modifierfacture/modifier" method="post" accept-charset="utf-8">
            <input type="hidden" value="<?php if($records[0]->fournisseur=="") echo "personne"; else echo "societe" ?>" id="type" name="type">
            <input type="hidden" value="<?php echo $records[0]->montant; ?>" id="montanta" name="montanta">
            <input type="hidden" value="<?php echo $records[0]->id_article; ?>" id="ida" name="ida">
            <input type="hidden" value="<?php echo $records[0]->id_chapitre; ?>" id="idc" name="idc">

            <div class="form-group">
                <?php if(trim($records[0]->fournisseur)!=""){ ?>
                <label  id="type" type="text" for="societe">Societé:</label>
                <select  onclick="ch()" class="form-control" name="soc" id="soc">


                    <option  value="<?php echo $records[0]->id_fournisseur ?>" >   <?php echo $records[0]->fournisseur?></option>
                    <?php
                    foreach ($recordss as $rec){ if($rec->nom!= $records[0]->fournisseur){

                        ?>

                        <option  value="<?php echo $rec->id ?>" >   <?php echo $rec->nom?></option>
                    <?php }} ?>
                    <option >autre</option>
                </select><?php } ?>

                <?php if(trim($records[0]->personne)!=""){ ?>
                <label   id="typeper"  for="societe">Personne:</label>
                <select  onclick="ch()" class="form-control" name="per" id="per">

                    <option  value="<?php echo $records[0]->id_fournisseur ?>" >   <?php echo $records[0]->personne?></option>

                    <?php
                    foreach ($recordsp as $rec){ if($rec->nom!= $records[0]->personne){?>
                        <option  value="<?php echo $rec->id ?>" >   <?php echo $rec->nom?></option>
                    <?php }} ?>
                    <option  >autre</option>
                </select>
                        <?php } ?>
            </div>
            <div>
                <input type="text" <?php if($recordss!=null ||$recordsp!=null) { ?>style="display:none" <?php } ?> class="form-control" id="monid" name="societe" placeholder="autre">
                <input type="text" style="display:none" id="autresoc" name="autresoc">
            </div>
            <div class="form-group">
                <label for="adrS" <?php if($recordss!=null ||$recordsp!=null) { ?>style="display:none" <?php } ?>  id="adr" >Adresse:</label>
            </div>
            <div>
                <input type="text" <?php if($recordss!=null ||$recordsp!=null) { ?>style="display:none" <?php } ?>  class="form-control" id="adrS" name="adrS" placeholder="Adresse ">
            </div>
            <div class="form-group">
                <label for="compte" <?php if($recordss!=null ||$recordsp!=null) { ?>style="display:none" <?php } ?>  id="cmpt">N°Compte:</label>
                <input type="text" <?php if($recordss!=null ||$recordsp!=null) { ?>style="display:none" <?php } ?>  class="form-control" id="compte" name="compte" placeholder="numero de compte ">

            </div>
            <div class="form-group">
                <label for="nomB" <?php if($recordss!=null ||$recordsp!=null) { ?>style="display:none" <?php } ?> id="nmb">Nom Banque:</label>
                <input type="text" <?php if($recordss!=null ||$recordsp!=null) { ?>style="display:none" <?php } ?>  class="form-control" id="nomB" name="nomB" placeholder="nom de la Banque ">

            </div>
            <div class="form-group">
                <label for="NFacture">N°Facture:</label>
                <input type="num" class="form-control" id="NFacture" name="NFacture" value="<?php echo $records[0]->id ?>">

            </div>
            <div class="form-group">
                <label for="montant">Montant de la facture:</label>
                <input type="text" class="form-control auto" id="montant" name="montant" value="<?php echo $records[0]->montant ?>" id="montant" name="montant" placeholder="montant de la facture" data-a-sep=" " data-a-dec="," data-a-sign="D.A ">

            </div>

            <?php if(trim($records[0]->personne)==""){ ?>
            <div class = "form-group checkbox well" id="fiche">
                <div class="btn-group" data-toggle="buttons">
                    <label ><input type="radio" name="opt" id="opt2" <?php if( $records[0]->num_mandat!=0){ ?> checked='checked' <?php } ?>onclick="fiche()" value="mandat"> mandat de paiment + avis de virement </label>
                    <label ><input type="radio" name="opt" id="opt1" <?php if( $records[0]->num_mandat==0){ ?> checked='checked' <?php } ?> onclick="fiche()" value="ch50"> CH-50</label>

                </div>
            </div>

            <div class="form-group">
                <label type="text" <?php if( $records[0]->num_mandat==0){ ?> style="display:none" <?php }?>name="mndt" id="mndt" for="mondat">N° Mandat:</label>
                <input type="text" <?php if( $records[0]->num_mandat==0){ ?> style="display:none" <?php }?>class="form-control" id="mendat" name="mendat" value="<?php echo $records[0]->num_mandat ?>">

            </div>
            <div class="form-group">
                <label type="text" <?php if( $records[0]->num_mandat==0){ ?> style="display:none" <?php }?> name="ribl" id="ribl" for="rib">RIB :</label>
                <input type="text" <?php if( $records[0]->num_mandat==0){ ?> style="display:none" <?php }?> class="form-control" id="rib" name="rib" value="<?php echo $records[0]->rib ?>">

            </div><?php } ?>
            <input type="hidden"  id="id" name="id" value="<?php echo $records[0]->num ?>" >

            <button type="submit" class="btn btn-top btn-success col-md-8 col-md-offset-2 btn-flat">Enregistrer</button>

        </form>
        <?php
        $msg = $this->session->flashdata('msg')['msg'];

        echo $msg;
        ?>
    </div>

</div>



<script src="<?php echo base_url();?>js/facturation.js"></script>
<script src="<?php echo base_url();?>js/choixfiche.js"></script>
<script src="<?php echo base_url();?>js/perssoc.js"></script>



<?php include 'footer.php';
?>
