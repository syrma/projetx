<?php

class Archivage  extends CI_Model {

    private $db_arch;
    
    function __construct()
        {
        parent::__construct();
        $this->db_arch = $this->load->database('archive', TRUE);
        $this->load->helper('file'); 
        $this->load->dbutil();
        }
        
    public function archiver()
        {
        
        #Si on est sous un serveur Linux, cette méthode donne un output plus sécursé
        #Elle permet aussi d'archiver une grande base de données
        #$DBUSER="dbuser";
        #$DBPASSWD="YFht754";
        #$DATABASE="SYST_FINANCES";
        #$EXEC = "mysqldump";
        #$filename = "./assets/archives/backup-" . date("d-m-Y") . ".sql.gz";
        #$cmd = $EXEC." -u $DBUSER --password=$DBPASSWD $DATABASE | gzip --best > ".$filename;   	
	#exec($cmd);
         

        # Création de l'archive
        # Note : S'il y a trop d'enregistrements, cette méthode peut ne pas marcher.
        $prefs = array(
                 'tables'       => array('QuestionS', 'Utilisateur', 'Chapitre', 'Article','Fournisseur', 'Facture', 'Transfert', 'Fiche', 'BudgetComp', 'Etat'),
                 'format'       => 'txt',
                 );
        $backup = $this->dbutil->backup($prefs);
        $filepath = "./assets/archives/backup-" . date("d-m-Y") . ".sql";
        write_file($filepath, $backup);
        
        }
    public function charger($filename)
        {
        $this->db_arch->query('DROP TABLE IF EXISTS Fiche, Transfert, Facture, Fournisseur, Etat, BudgetComp, Article, Chapitre, Utilisateur, QuestionS;');
        $archivepath = "./assets/archives/";
        $script = new SplFileObject($archivepath.$filename);
        if ($script) {
            $query = "";
            while (!$script->eof()) {
                $line = trim($script->fgets());
                if($line !== "" && $line[0] != "#")
                    $query .= $line;
                else
                    continue;

                if(substr($query, -1) === ';'){
                    $this->db_arch->query($query);
                    $query = "";
                    }
                }
            return true;
        } else
           return false;
        }
        
    public function consTransferts()
        {
        $query = $this->db_arch->get('Transfert');
        return $query->result();
        }
        
    public function consFactures()
        {
        $query = $this->db_arch->get('Facture');
        return $query->result();
        }
        
    public function consArticle($id, $idc)
        {
        $this->db_arch->select('nom, budget, budget_initial');
        $this->db_arch->where('id', $id);
        $this->db_arch->where('id_chapitre', $idc);
        $query = $this->db_arch->get('Article');
        return $query->result();
        }
        
    public function consChapitre($id)
        {
        $this->db_arch->select('nom');
        $this->db_arch->where('id', $id);
        $query = $this->db_arch->get('Chapitre');
        return $query->result();
        }

    public function consFournisseur($id)
        {
        $this->db_arch->select('nom');
        $this->db_arch->select('adr');
        $this->db_arch->select('nom_banque');
        $this->db_arch->select('rib_fournisseur');
        $this->db_arch->select('type_fournisseur');
        $this->db_arch->select('numero_banque');
        $this->db_arch->select('id');
        $this->db_arch->where('id', $id);
        $query = $this->db_arch->get('Fournisseur');
        return $query->result();
        }
    
    public function consulterFournisseur()
        {
        $this->db_arch->select('id');
        $this->db_arch->select('nom');
        $this->db_arch->select('adr');
        $this->db_arch->select('nom_banque');
        $this->db_arch->select('rib_fournisseur');
        $this->db_arch->select('type_fournisseur');
        $this->db_arch->select('numero_banque');
        $query = $this->db_arch->get('Fournisseur');
        return $query->result();
        }
        
    public function consFacture($id)
        {
        $this->db_arch->select('n_facture');
        $this->db_arch->select('id_article');
        $this->db_arch->select('date_facture');
        $this->db_arch->select('montant');
        $this->db_arch->select('fournisseur');
        $this->db_arch->select('id_chapitre, auteur');
        $this->db_arch->where('id', $id);
        $query = $this->db_arch->get('Facture');
        return $query->result();
        }
     
    public function consUtilisateur($email){
        $this->db_arch->select('nom, prenom, categorie,reponseS,reponseS');
        $this->db_arch->where('email', $email);
        $query = $this->db_arch->get('Utilisateur');
        return $query->result();
    }
    
    public function consulter_transferts_source($id,$idc)
        {
        $this->db_arch->select('montant_transfert');
        $this->db_arch->select('article_cible');
        $this->db_arch->select('chapitre_cible, auteur');
        $this->db_arch->where('article_source', $id);
        $this->db_arch->where('chapitre_source', $idc);
        $query = $this->db_arch->get('Transfert');
        return $query->result();
        }
    
    public function consulter_transferts_cible($id,$idc)
        {
        $this->db_arch->select('montant_transfert');
        $this->db_arch->select('article_source');
        $this->db_arch->select('chapitre_source, auteur');
        $this->db_arch->where('article_cible', $id);
        $this->db_arch->where('chapitre_cible', $idc);
        $query = $this->db_arch->get('Transfert');
        return $query->result();
        }
    
    public function reinitialiser()
    {
        $this->load->database();
        $this->db->empty_table('BudgetComp');
        $this->db->empty_table('Fiche');
        $this->db->empty_table('Facture');
        $this->db->empty_table('Transfert');
        $this->db->empty_table('Etat');
        $this->db->query('UPDATE Article SET budget = 0, budget_initial =0;');
    }
    public function etat(){
        $this->db->empty_table('Etat');
    }
    
    
    /*  Pour la situation financière */
    public function consulterChapsec1(){
        $this->db_arch->select('id');
        $this->db_arch->select('nom');
        $this->db_arch->where('sect', 1);
        $query = $this->db_arch->get('Chapitre');
        return $query->result();
    }
    public function consultersec2(){
        $this->db_arch->select('id');
        $this->db_arch->select('nom');
        $this->db_arch->where('sect', 2);
        $query = $this->db_arch->get('Chapitre');
        return $query->result();
    }
    
        public function budget_initial_chapitre ($idc){
        $this->db_arch->select('sum(budget_initial) as `s`');
        $this->db_arch->where('id_chapitre', $idc);
        $query = $this->db_arch->get('Article');
        return $query->result()[0]->s;

    }
    
    public function selectchap($id_chapitre,$datedebut,$datefin){

        $this->db_arch->where('id_chapitre', $id_chapitre);
        $this->db_arch->where('date_transaction<=', $datefin);
        $this->db_arch->where('date_transaction>=',$datedebut);
        $query = $this->db_arch->get('BudgetComp');

        return $query->result();

    }
    
    public function selectsourcechap($id_chapitre,$datedebut,$datefin)
    {

        $this->db_arch->where('chapitre_source', $id_chapitre);
        $this->db_arch->where('date_transfert<=', $datefin);
        $this->db_arch->where('date_transfert>=',$datedebut);
        $query= $this->db_arch->get('Transfert');
        return $query->result();
    }
    
    public function selectciblechap($id_chapitre,$datedebut,$datefin)
    {

        $this->db_arch->where('chapitre_cible', $id_chapitre);
        $this->db_arch->where('date_transfert<=', $datefin);
        $this->db_arch->where('date_transfert>=',$datedebut);
        $query= $this->db_arch->get('Transfert');
        return $query->result();
    }
    
    public function paimentchap ($id_chapitre,$datedebut,$datefin)
    {
        $this->db_arch->select('montant');
        $this->db_arch->where('id_chapitre', $id_chapitre);
        $this->db_arch->where('date_facture<=', $datefin);
        $this->db_arch->where('date_facture>=',$datedebut);
        $this->db_arch->where('valide', 1);
        $query = $this->db_arch->get('Facture');
        return $query->result();
    }
    
        public function selectA(){
        $this->db_arch->select('id');
        $this->db_arch->select('nom');
        $this->db_arch->select('id_chapitre');
        $this->db_arch->select('budget');
        $this->db_arch->select('budget_initial');
        $query = $this->db_arch->get('Article');
        return $query->result();
    }
    
    public function selectB($id,$id_chapitre,$datedebut,$datefin){

        $this->db_arch->where('id_article', $id);
        $this->db_arch->where('id_chapitre', $id_chapitre);
        $this->db_arch->where('date_transaction<=', $datefin);
        $this->db_arch->where('date_transaction>=',$datedebut);
        $query = $this->db_arch->get('BudgetComp');

        return $query->result();

    }
    
    public function selectsource($id,$id_chapitre,$datedebut,$datefin)
    {
        $this->db_arch->where('article_source', $id);
        $this->db_arch->where('chapitre_source', $id_chapitre);
        $this->db_arch->where('date_transfert<=', $datefin);
        $this->db_arch->where('date_transfert>=',$datedebut);
        $query= $this->db_arch->get('Transfert');
        return $query->result();
    }
    
    public function selectcible($id,$id_chapitre,$datedebut,$datefin)
    {
        $this->db_arch->where('article_cible', $id);
        $this->db_arch->where('chapitre_cible', $id_chapitre);
        $this->db_arch->where('date_transfert<=', $datefin);
        $this->db_arch->where('date_transfert>=',$datedebut);
        $query= $this->db_arch->get('Transfert');
        return $query->result();
    }
    
    public function paiment ($id,$id_chapitre,$datedebut,$datefin)
      {
          $this->db_arch->select('id');
          $this->db_arch->select('montant');
          $this->db_arch->select('n_facture');
          $this->db_arch->select('date_facture');
          $this->db_arch->select('date_etablissement');
          $this->db_arch->select('fournisseur');
          $this->db_arch->where('id_article', $id);
          $this->db_arch->where('id_chapitre', $id_chapitre);
          $this->db_arch->where('date_facture<=', $datefin);
          $this->db_arch->where('date_facture>=',$datedebut);
          $this->db_arch->where('valide', 1);
          $query = $this->db_arch->get('Facture');
          return $query->result();
    }
}

?>
