<?php
class Chapitre extends CI_Model {
    public function consulter(){
        $this->db->select('id');
        $this->db->select('nom');
        $query = $this->db->get('Chapitre');

        return $query->result();
    }
    public function consultersec1(){
        $this->db->select('id');
        $this->db->select('nom');
        $this->db->where('sect', 1);
        $query = $this->db->get('Chapitre');

        return $query->result();
    }
    public function consultersec2(){
        $this->db->select('id');
        $this->db->select('nom');
        $this->db->where('sect', 2);
        $query = $this->db->get('Chapitre');

        return $query->result();
    }

    public function verifier($id){


        $this->db->select('id');
        $this->db->where('id', $id);
        $query = $this->db->get('Chapitre');
        return $query->result();

    }
    public function ajouter($nom,$id,$sec){

        $this->db->set('sect',$sec);
        $this->db->set('nom',$nom);
        $this->db->set('id',$id);
        $this->db->insert('Chapitre');


    }
    public function consulterunchapitre($id){
        $this->db->select('id');
        $this->db->select('nom');
        $this->db->select('sect');
        $this->db->where('id', $id);
        $query = $this->db->get('Chapitre');

        return $query->result();
    }
    public function modifier($id,$nom,$idc){
        $data = array(
            'id' => $id,
            'nom'=>$nom
        );
        $this->db->where('id', $idc);
        $this->db->update('Chapitre', $data);

        $dataa = array(
            'id_chapitre' => $id

        );
        $this->db->where('id_chapitre', $idc);
        $this->db->update('Article', $dataa);

        $d = array(
            'id_chapitre'=>$id,
        );

        $this->db->where('id_chapitre', $idc);
        $this->db->update('Budgetcomp', $d);

        $this->db->where('id_chapitre', $idc);
        $this->db->update('Facture', $d);
        $data = array(
            'chapitre_source'=>$id,

        );

        $this->db->where('chapitre_source', $idc);
        $this->db->update('Transfert', $data);
        $data = array(
            'chapitre_cible'=>$id,

        );

        $this->db->where('chapitre_cible', $idc);
        $this->db->update('Transfert', $data);



    }
    public function supprimer($id)
    {
        $this->db->delete('Chapitre', array('id' => $id));
    }
}

?>
