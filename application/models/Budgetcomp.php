<?php
class Budgetcomp extends CI_Model {

    public function select($id,$id_chapitre,$datedebut,$datefin){

        $this->db->where('id_article', $id);
        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->where('date_transaction<=', $datefin);
        $this->db->where('date_transaction>=',$datedebut);
        $query = $this->db->get('BudgetComp');

        return $query->result();

    }
    public function selectsem($id,$id_chapitre,$datefin){

        $this->db->where('id_article', $id);
        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->where('date_transaction<=', $datefin);
        $query = $this->db->get('BudgetComp');

        return $query->result();

    }
    public function selectchap($id_chapitre,$datedebut,$datefin){

        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->where('date_transaction<=', $datefin);
        $this->db->where('date_transaction>=',$datedebut);
        $query = $this->db->get('BudgetComp');

        return $query->result();

    }
    public function selectchapf($id_chapitre){

        $this->db->where('id_chapitre', $id_chapitre);
        $query = $this->db->get('BudgetComp');

        return $query->result();

    }
    public function selectf($id,$id_chapitre){

        $this->db->where('id_article', $id);
        $this->db->where('id_chapitre', $id_chapitre);

        $query = $this->db->get('BudgetComp');

        return $query->result();

    }
}

?>
