<?php
/**
 * Created by PhpStorm.
 * User: souad
 * Date: 01/03/2016
 * Time: 23:24
 */

class Utilisateurs extends CI_Model {

public function trouver_utilisateurs($ctg){

    $this->db->select('email, nom, prenom, categorie');
    $where="categorie!='x'";
    if($ctg=="Doyen")
    { $where = "bloque='0' AND (categorie='Doyen' OR categorie='secretaire general') ";
    }
    if($ctg=="Secretaire general"){
        $where = "categorie='Responsable' AND bloque='0'";
    }
    if($ctg =="Responsable"){
        $where = "categorie='Agent' AND bloque='0'";
    }

    $this->db->where($where);
    $query = $this->db->get('Utilisateur');
    return $query->result();
}
    public function trouver_utilisateurs_bloques($ctg){

        $this->db->select('email, nom, prenom, categorie');
        $where="categorie!='x'";
        if($ctg=="Doyen")
        { $where = "bloque='1' AND (categorie='Doyen' OR categorie='secretaire general')";
        }
        if($ctg=="Secretaire general"){
            $where = "categorie='Responsable' AND bloque='1'";
        }
        if($ctg =="Responsable"){
            $where = "categorie='Agent' AND bloque='1'";
        }

        $this->db->where($where);
        $query = $this->db->get('Utilisateur');
        return $query->result();
    }
    public function ajouter_question($QS){

        $this->db->trans_start();
        $this->db->set('enonce', $QS);
        $this->db->insert('QuestionS');
        $id= $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
public function verifier_utilisateur($email){
    $this->db->select('email, nom, prenom, mdp,questionS,reponseS');
    $this->db->where('email', $email);
    $query = $this->db->get('Utilisateur');
    return $query->result();

}

    public function consulter($email){
        $this->db->select('nom, prenom, categorie,reponseS,reponseS');
        $this->db->where('email', $email);
        $query = $this->db->get('Utilisateur');
        return $query->result();
    }
    public function modifier_mdp($email,$mdp)
    {
        $data=array(
            'mdp'=>$mdp

        );
        $this->db->where('email', $email);
        $this->db->update('Utilisateur', $data);
    }
    public function ajouter($nom,$prenom,$email,$mdp,$idQ,$RS,$ctg)
    {
        $this->db->set('nom', $nom);
        $this->db->set('prenom', $prenom);
        $this->db->set('email', $email);
        $this->db->set('mdp', $mdp);
        $this->db->set('reponseS', $RS);
        $this->db->set('categorie', $ctg);
        $this->db->set('questionS', $idQ);
        $this->db->insert('Utilisateur');
        $data= array(
            'questionS'=>$idQ
        );
        $this->db->where('email', $email);
        $this->db->update('Utilisateur', $data);
    }
    public function ajouterdoyen($nom,$prenom,$email,$mdp,$idQ,$RS)
    {
        $this->db->delete('Utilisateur', array('categorie' => "Doyen"));
        $this->db->set('nom', $nom);
        $this->db->set('prenom', $prenom);
        $this->db->set('email', $email);
        $this->db->set('mdp', $mdp);
        $this->db->set('reponseS', $RS);
        $this->db->set('categorie', "Doyen");
        $this->db->set('questionS', $idQ);
        $this->db->insert('Utilisateur');
        $data= array(
            'questionS'=>$idQ
        );
        $this->db->where('email', $email);
        $this->db->update('Utilisateur', $data);
    }
    public function modifier($id,$nom,$prenom,$categorie)
    {
        $data=array(
            'nom'=>$nom,
            'prenom'=>$prenom,
            'categorie'=>$categorie
        );
        $this->db->where('email', $id);
        $this->db->update('Utilisateur', $data);
    }
    public function modifierprofil($mail,$idQ,$reponse,$mdp,$email)
    {
        if($mdp!="")
        {
        $data=array(
            'email'=>$mail,
            'reponseS'=>$reponse,
            'mdp'=>$mdp,
            'questionS'=>$idQ
        );
        }
        else
        {
            $data=array(
                'email'=>$mail,
                'reponseS'=>$reponse,
                'questionS'=>$idQ
            );
        }

        $this->db->where('email', $email);
        $this->db->update('Utilisateur', $data);
    }
    public function supprimer_utilisateur($email)
    {

        $data=array(
            'bloque'=>1

        );


$this->db->where('email', $email);
$this->db->update('Utilisateur', $data);

    }
    public function debloquer($email)
    {

        $data=array(
            'bloque'=>0

        );


        $this->db->where('email', $email);
        $this->db->update('Utilisateur', $data);

    }
}

?>
