﻿<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Facture extends CI_Model {
	
public function ajouter($id,$id_chapitre,$id_article,$montant,$fournisseur,$date, $auteur){
    $datee=date('Y-m-d');
    $this->db->set('n_facture',$id);
    $this->db->set('id_article',$id_article);
    $this->db->set('id_chapitre',$id_chapitre);
    $this->db->set('auteur',$auteur);
    $this->db->set('date_facture',$date);
    $this->db->set('date_etablissement',$datee);
    $this->db->set('montant',$montant);
    $this->db->set('fournisseur',$fournisseur);
    $this->db->set('confirme',0);
    $this->db->set('valide',0);
    $this->db->insert('Facture');
}

public function verifier($id){
    $this->db->where('id', $id);
    $query = $this->db->get('Facture');
    return $query->result();
}

public function consulter($confirme,$valide){
    $this->db->where('confirme', $confirme);
    $this->db->where('valide', $valide);
    $query = $this->db->get('Facture');
    return $query->result();
}

public function consulter_facture($id){
    $this->db->select('n_facture');
    $this->db->select('id_article');
    $this->db->select('date_facture');
    $this->db->select('montant');
    $this->db->select('fournisseur');
    $this->db->select('id_chapitre, auteur');
    $this->db->where('id', $id);
    $query = $this->db->get('Facture');
    return $query->result();
    }

public function nom_fourn($id){
    $this->db->select('nom');
    $this->db->select('type_fournisseur');
    $this->db->select('rib_fournisseur');
    $this->db->where('id', $id);
    $query = $this->db->get('Fournisseur');
    return $query->result();
}

public function modifier($id,$id_societe,$num,$somme){
    $data = array(
    'n_facture'=>$num,
    'montant'=>$somme,
    'fournisseur'=>$id_societe
    );
    $this->db->where('id', $id);
    $this->db->update('Facture', $data);
}

public function confirmer($id){
    $data = array(
    'confirme'=>1
    );
    $this->db->where('id', $id);
    $this->db->update('Facture', $data);
}

public function valider ($id){
    $data = array(
    'valide'=>1
    );
    $this->db->where('id', $id);
    $this->db->update('Facture', $data);
}

public function annuler ($id){
    $this->db->delete('Fiche', array('id_facture' => $id));
    $this->db->delete('Facture', array('id' => $id));
}

public function getFacture(){
    $query = $this->db->get('Facture');
    return $query->result();
}

public function recupD($dStart,$dEnd){

	$this->db->where(' date_facture >= date("'.$dStart.'")');
	$this->db->where( 'date_facture <= date("'.$dEnd.'")');
	return $this->db->get('Facture')->result();
}
    public function paiment ($id,$id_chapitre,$datedebut,$datefin)
      {
          $this->db->select('id');
          $this->db->select('montant');
          $this->db->select('n_facture');
          $this->db->select('date_facture');
          $this->db->select('date_etablissement');
          $this->db->select('fournisseur');
          $this->db->where('id_article', $id);
            $this->db->where('id_chapitre', $id_chapitre);
            $this->db->where('date_facture<=', $datefin);
            $this->db->where('date_facture>=',$datedebut);
          $this->db->where('valide', 1);
            $query = $this->db->get('Facture');
          return $query->result();
    }
    public function paimentchap ($id_chapitre,$datedebut,$datefin)
    {
        $this->db->select('montant');
        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->where('date_facture<=', $datefin);
        $this->db->where('date_facture>=',$datedebut);
        $this->db->where('valide', 1);
        $query = $this->db->get('Facture');
        return $query->result();
    }
    public function paimentchapf ($id_chapitre)
    {
        $this->db->select('montant');
        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->where('valide', 1);
        $query = $this->db->get('Facture');
        return $query->result();
    }
    public function paimentf ($id,$id_chapitre)
    {
        $this->db->select('montant');
        $this->db->where('id_article', $id);
        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->where('valide', 1);
        $query = $this->db->get('Facture');
        return $query->result();
    }
    public function verifiefacture ($id,$id_chapitre)
    {

        $this->db->where('id_article', $id);
        $this->db->where('id_chapitre', $id_chapitre);
        $query = $this->db->get('Facture');
        return $query->result();
    }
    public function cons_sem1 ($idc, $ida, $datefin, &$total){
        $this->db->select('*');
        $this->db->where('valide',1);
        $this->db->where('id_chapitre', $idc);
        $this->db->where('id_article', $ida);
        $this->db->where('date_etablissement<=', $datefin);
        $query = $this->db->get('Facture');
        $this->db->select('sum(montant) as `s`');
        $this->db->where('valide',1);
        $this->db->where('id_chapitre', $idc);
        $this->db->where('id_article', $ida);
        $this->db->where('date_etablissement<=', $datefin);
        $total = $this->db->get('Facture')->result()[0]->s;
        return $query->result();
    }
    
    public function cons_sem2 ($idc, $ida, $datefin, &$total){
        $this->db->select('*');
        $this->db->where('valide',1);
        $this->db->where('id_chapitre', $idc);
        $this->db->where('id_article', $ida);
        $this->db->where('date_etablissement >', $datefin);
        $query = $this->db->get('Facture');
        $this->db->select('sum(montant) as `s`');
        $this->db->where('valide',1);
        $this->db->where('id_chapitre', $idc);
        $this->db->where('id_article', $ida);
        $this->db->where('date_etablissement >', $datefin);
        $total = $this->db->get('Facture')->result()[0]->s;
        return $query->result();
    }
    
    public function facturesem ($id,$id_chapitre,$datefin)
    {
        $this->db->select('montant');
        $this->db->where('id_article', $id);
        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->where('date_facture<=', $datefin);
        $this->db->where('valide', 1);
        $query = $this->db->get('Facture');
        return $query->result();
    }

}
 ?>
