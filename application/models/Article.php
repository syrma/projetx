<?php
/**
 * Created by PhpStorm.
 * User: souad
 * Date: 04/03/2016
 * Time: 13:47
 */
class Article extends CI_Model {
    public function verifier($idc){

        $this->db->where('id_chapitre', $idc);
        $query = $this->db->get('Article');
        return $query->result();

    }
    public function budgcompl($idc, $ida, $budg,$email)
    {
        $datee=date('Y-m-d');
        $this->db->set('id_chapitre',$idc);
        $this->db->set('id_article',$ida);
        $this->db->set('montant',$budg);
        $this->db->set('auteur',$email);
        $this->db->set('date_transaction',$datee);
        $this->db->insert('BudgetComp');


    }
    public function consulter(){
        $this->db->select('id');

        $query = $this->db->get('Article');
        return $query->result();
    }

    public function liste($id_chapitre)
    {
        $this->db->select('id');
        $this->db->where('id_chapitre', $id_chapitre);
        $query = $this->db->get('Article');
        return $query->result();
    }
    public function ajouter($nom,$id,$budget,$ida){

        $this->db->set('id',$ida);
        $this->db->set('nom',$nom);
        $this->db->set('id_chapitre',$id);
        $this->db->set('budget_initial',$budget);
        $this->db->set('budget',$budget);
        $this->db->insert('Article');


    }
    public function monbudget($id,$idc)
    {
        $this->db->select('budget_initial');
        $this->db->where('id', $id);
        $this->db->where('id_chapitre', $idc);
        $query = $this->db->get('Article');
        return $query->result();
    }
    public function budget ($id,$idc){
        $this->db->select('id');
        $this->db->select('budget');
        $this->db->select('nom');
        $this->db->select('budget_initial');
        $this->db->where('id', $id);
        $this->db->where('id_chapitre', $idc);
        $query = $this->db->get('Article');
        return $query->result();
    }
    public function transferer($montant,$idcd,$idca,$idad,$idaa,$dt,$montantad,$montantaa,$email){
        $this->db->set('chapitre_source', $idcd);
        $this->db->set('article_source', $idad);
        $this->db->set('chapitre_cible', $idca);
        $this->db->set('article_cible', $idaa);
        $this->db->set('date_transfert', $dt);
        $this->db->set('montant_transfert', $montant);
        $this->db->set('auteur', $email);
        $this->db->insert('Transfert');
        $data = array(
            'budget' => $montantad
        );
        $dataa = array(
            'budget' => $montantaa
        );
        $this->db->where('id', $idad);
        $this->db->where('id_chapitre', $idcd);
        $this->db->update('Article', $data);
        $this->db->where('id', $idaa);
        $this->db->where('id_chapitre', $idca);
        $this->db->update('Article', $dataa);
    }
    public function budget_chapitre (){
        $this->db->select('id_chapitre');
        $this->db->select('budget');
        $query = $this->db->get('Article');
        return $query->result();

    }
    public function budget_article ($idc){
        $this->db->select('id');
        $this->db->select('budget');
        $this->db->where('id_chapitre', $idc);
        $query = $this->db->get('Article');
        return $query->result();

    }
    public function budget_initial_chapitre ($idc){
        $this->db->select('budget_initial');
        $this->db->where('id_chapitre', $idc);
        $query = $this->db->get('Article');
        return $query->result();

    }

    public function budget_initial (){
        $this->db->select('budget_initial');
        $query = $this->db->get('Article');
        return $query->result();

    }

    public function retrait($id_chapitre,$id_article,$montant){
        $data = array(
            'budget'=>$montant
        );
        $this->db->where('id', $id_article);
        $this->db->where('id_chapitre', $id_chapitre);
        $this->db->update('Article', $data);
    }

    public function modifier($id,$nom,$budget_initial,$idb,$idc,$bdgc)
    {
        $data = array(
            'id'=>$id,
            'nom'=>$nom,
            'budget_initial' => $budget_initial,
            'budget'=>$bdgc
        );
        $this->db->where('id', $idb);
        $this->db->where('id_chapitre', $idc);
        $this->db->update('Article', $data);

        $d = array(
            'id_article'=>$id,
        );
        $this->db->where('id_article', $idb);
        $this->db->where('id_chapitre', $idc);
        $this->db->update('BudgetComp', $d);
        $this->db->where('id_article', $idb);
        $this->db->where('id_chapitre', $idc);
        $this->db->update('Facture', $d);
        $this->db->where('id_article', $idb);
        $this->db->where('id_chapitre', $idc);
        $this->db->update('Fiche', $d);
        $data = array(
            'article_source'=>$id,

        );
        $this->db->where('article_source', $idb);
        $this->db->where('chapitre_source', $idc);
        $this->db->update('Transfert', $data);
        $data = array(
            'article_cible'=>$id,

        );
        $this->db->where('article_cible', $idb);
        $this->db->where('chapitre_cible', $idc);
        $this->db->update('Transfert', $data);



    }
    

    public function select(){
        $this->db->select('id');
        $this->db->select('nom');
        $this->db->select('id_chapitre');
        $this->db->select('budget');
        $this->db->select('budget_initial');

        $query = $this->db->get('Article');
        return $query->result();
    }

    public function consulter_transferts_source($id,$idc)
    {
        $this->db->select('montant_transfert');
        $this->db->select('article_cible');
        $this->db->select('chapitre_cible');
        $this->db->where('article_source', $id);
        $this->db->where('chapitre_source', $idc);
        $query = $this->db->get('Transfert');
        return $query->result();

    }

    public function consulter_transferts_cible($id,$idc)
    {
        $this->db->select('montant_transfert');
        $this->db->select('article_source');
        $this->db->select('chapitre_source');
        $this->db->where('article_cible', $id);
        $this->db->where('chapitre_cible', $idc);
        $query = $this->db->get('Transfert');
        return $query->result();
    }

    public function consulter_article($id,$idc)
    {
        $this->db->select('nom');
        $this->db->select('budget_initial');
        $this->db->select('budget');
        $this->db->where('id', $id);
        $this->db->where('id_chapitre', $idc);
        $query = $this->db->get('Article');
        return $query->result();
    }
    public function supprimer($id,$idc)
    {
        $this->db->delete('Article', array('id' => $id, 'id_chapitre'=>$idc));
    }
	public function consulterTransfert()
    {	$this->db->select('id');
        $query= $this->db->get('Transfert');
        return $query->result();
    }

}


?>
