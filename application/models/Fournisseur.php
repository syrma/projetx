﻿<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fournisseur extends CI_Model {
	
 public function ajouter_rib($nom,$adr,$nom_banque,$numero_banque,$type,$rib){
    $this->db->trans_start();
    $this->db->set('nom',$nom);
    $this->db->set('adr',$adr);
    $this->db->set('nom_banque',$nom_banque);
    $this->db->set('numero_banque',$numero_banque);
    $this->db->set('type_fournisseur',$type);
    $this->db->set('rib_fournisseur',$rib);
    $this->db->insert('Fournisseur');
    $id= $this->db->insert_id();
    $this->db->trans_complete();
    return $id;
    }
    public function ajouter_ribp($nom,$adr,$nom_banque,$numero_banque,$type,$rib,$prenom){
        $this->db->trans_start();
        $this->db->set('nom',$nom);
        $this->db->set('prenom',$prenom);
        $this->db->set('adr',$adr);
        $this->db->set('nom_banque',$nom_banque);
        $this->db->set('numero_banque',$numero_banque);
        $this->db->set('type_fournisseur',$type);
        $this->db->set('rib_fournisseur',$rib);
        $this->db->insert('Fournisseur');
        $id= $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
    public function ajouter($nom,$adr,$nom_banque,$numero_banque,$type){
        $this->db->trans_start();
        $this->db->set('nom',$nom);
        $this->db->set('adr',$adr);
        $this->db->set('nom_banque',$nom_banque);
        $this->db->set('numero_banque',$numero_banque);
        $this->db->set('type_fournisseur',$type);
        $this->db->insert('Fournisseur');
        $id= $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
    public function ajouterp($nom,$adr,$nom_banque,$numero_banque,$type,$prenom){
        $this->db->trans_start();
        $this->db->set('nom',$nom);
        $this->db->set('prenom',$prenom);
        $this->db->set('adr',$adr);
        $this->db->set('nom_banque',$nom_banque);
        $this->db->set('numero_banque',$numero_banque);
        $this->db->set('type_fournisseur',$type);
        $this->db->insert('Fournisseur');
        $id= $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
	public function verifier($nom){
		$this->db->select('id');
        $this->db->where('nom', $nom);
        $query = $this->db->get('Fournisseur');
        return $query->result();
    }
    public function modifier_rib($rib, $id_societe){
        $data = array(
            'rib_fournisseur'=>$rib,
        );
        $this->db->where('id', $id_societe);
        $this->db->update('Fournisseur', $data);
    }
    
    public function consulter(){
        $query = $this->db->get('Fournisseur');
        return $query->result();
    }
	
    public function consulter_id($id){
        $this->db->select('nom');
        $this->db->select('prenom');
        $this->db->select('adr');
        $this->db->select('nom_banque');
        $this->db->select('rib_fournisseur');
        $this->db->select('type_fournisseur');
        $this->db->select('numero_banque');
        $this->db->select('id');
        $this->db->where('id', $id);
        $query = $this->db->get('Fournisseur');
        return $query->result();
    }

    public function idfournisseur(){
        $this->db->select('nom');
        $this->db->select('id');
        $this->db->where('type_fournisseur', 1);
        $query = $this->db->get('Fournisseur');

        return $query->result();
    }
    public function idpersonnes(){
        $this->db->select('nom');
        $this->db->select('id');
        $this->db->where('type_fournisseur', 0);
        $query = $this->db->get('Fournisseur');

        return $query->result();
    }
    public function modifier($id,$nom,$nomb,$numb,$rib,$adr,$type)
    {
            $data=array(
                'nom'=>$nom,
                'nom_banque'=>$nomb,
                'numero_banque'=>$numb,
                'rib_fournisseur'=>$rib,
                'adr'=>$adr,
                'type_fournisseur'=>$type
            );
        $this->db->where('id', $id);
        $this->db->update('Fournisseur', $data);
    }

}
 ?>
