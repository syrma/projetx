<?php
class Etat extends CI_Model {
    public function rib($rib)
    {
        $data = array(
            'rib_univ' => $rib,
            'semestre' => 1,
            'annee' => date('Y')
        );

        $this->db->insert('Etat', $data);
    }
    public function verifier()
    {
        $query = $this->db->get('Etat');
        return $query->result();
    }
    public function finpremier()
    {
    date_default_timezone_set("Africa/Algiers");
        $data = array(
            'semestre'=>2,
            'date_fin_sem1'=> date("Y-m-d")
        );
        $this->db->where('semestre',1);
        $this->db->update('Etat', $data);
    }
    public function finannee()
    {
        $datee=date('Y-m-d');
        $data = array(

            'date_fin_annee'=> $datee
        );

        $this->db->update('Etat', $data);
    }
    public function init_budget($budget)
    {
        $data = array(
            'budget_initial' => $budget
        );

        $this->db->where('semestre',1);
        $this->db->update('Etat', $data);
    }



}

?>
