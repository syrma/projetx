<?php
class Transferts extends CI_Model {

    public function consulter()
    {
        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectsource($id,$id_chapitre,$datedebut,$datefin)
    {
        $this->db->where('article_source', $id);
        $this->db->where('chapitre_source', $id_chapitre);
        $this->db->where('date_transfert<=', $datefin);
        $this->db->where('date_transfert>=',$datedebut);
        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectcible($id,$id_chapitre,$datedebut,$datefin)
    {
        $this->db->where('article_cible', $id);
        $this->db->where('chapitre_cible', $id_chapitre);
        $this->db->where('date_transfert<=', $datefin);
        $this->db->where('date_transfert>=',$datedebut);
        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectsourcechap($id_chapitre,$datedebut,$datefin)
    {

        $this->db->where('chapitre_source', $id_chapitre);
        $this->db->where('date_transfert<=', $datefin);
        $this->db->where('date_transfert>=',$datedebut);
        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectciblechap($id_chapitre,$datedebut,$datefin)
    {

        $this->db->where('chapitre_cible', $id_chapitre);
        $this->db->where('date_transfert<=', $datefin);
        $this->db->where('date_transfert>=',$datedebut);
        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectsourcechapf($id_chapitre)
    {

        $this->db->where('chapitre_source', $id_chapitre);

        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectciblechapf($id_chapitre)
    {

        $this->db->where('chapitre_cible', $id_chapitre);

        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectsourcef($id,$id_chapitre)
    {
        $this->db->where('article_source', $id);
        $this->db->where('chapitre_source', $id_chapitre);

        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function selectciblef($id,$id_chapitre)
    {
        $this->db->where('article_cible', $id);
        $this->db->where('chapitre_cible', $id_chapitre);

        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function transfertsourcesem($id,$id_chapitre,$datefin)
    {
        $this->db->where('article_source', $id);
        $this->db->where('chapitre_source', $id_chapitre);
        $this->db->where('date_transfert<=', $datefin);
        $query= $this->db->get('Transfert');
        return $query->result();
    }
    public function transfertciblesem($id,$id_chapitre,$datefin)
    {
        $this->db->where('article_cible', $id);
        $this->db->where('chapitre_cible', $id_chapitre);
        $this->db->where('date_transfert<=', $datefin);
        $query= $this->db->get('Transfert');
        return $query->result();
    }
}

?>
