﻿<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fiche extends CI_Model {
	
 public function ajouter($nom,$semestre,$numero,$id_chapitre,$id_article,$id_facture,$id_transfert){
	 $this->db->trans_start();
		$annee=date('Y');
	    $this->db->set('nom',$nom);
        $this->db->set('semestre',$semestre);
		$this->db->set('numero',$numero);
		$this->db->set('annee',$annee);
		$this->db->set('id_chapitre',$id_chapitre);
		$this->db->set('id_article',$id_article);
		$this->db->set('id_facture',$id_facture);
		$this->db->set('id_transfert',$id_transfert);
        $this->db->insert('Fiche');
	 $id= $this->db->insert_id();
	 $this->db->trans_complete();
	 return $id;
	 
 }
public function consulter(){
        $query = $this->db->get('Fiche');
        return $query->result();
 }
	public function consulterfiche($idc,$id,$nom)
	{
		$this->db->where('nom',$nom);
		$this->db->where('id_chapitre',$idc);
		$this->db->where('id_article',$id);

		$query = $this->db->get('Fiche');

		return $query->result();
	}
 public function mandat($nmandat){
		$this->db->where('numero',$nmandat);
	 $this->db->where('nom',"MANDAT DE PAIEMENT");

	 $query = $this->db->get('Fiche');
        return $query->result();
 }
  public function avis($n){
		$this->db->where('id_facture',$n);
        $query = $this->db->get('Fiche');
        return $query->result();
 }
	public function consulterrat($idc,$id)
	{
	$this->db->where('nom',"FICHE DE RATTACHEMENT");
	$this->db->where('id_chapitre',$idc);
	$this->db->where('id_article',$id);

	$query = $this->db->get('Fiche');

	return $query->result();
   }
	public function supprimerrat($idc,$id)
	{
		$this->db->where('nom',"FICHE DE RATTACHEMENT");
		$this->db->where('id_chapitre',$idc);
		$this->db->where('id_article',$id);

		$this->db->delete('Fiche', array('id_chapitre' => $idc, 'id_article'=>$id, 'nom'=>"FICHE DE RATTACHEMENT"));
		$this->db->delete('Fiche', array('id_chapitre' => $idc, 'id_article'=>$id, 'nom'=>"FICHE D'ENGAGEMENT PROVISIONNEL"));

	}
public function numero($id_facture){
	$this->db->select('numero');
	$this->db->where('nom',"MANDAT DE PAIEMENT");
	$this->db->where('id_facture',$id_facture);
	$query = $this->db->get('Fiche');
	return $query->result();
}
	public function consulterunefiche($id_facture){
		$this->db->where('id_facture',$id_facture);
		$query = $this->db->get('Fiche');
		return $query->result();
	}
	public function modifier_num($nummandat,$id){
		$data=array(
			'numero'=>$nummandat
		);
		$this->db->where('id_facture', $id);
		$this->db->update('Fiche', $data);
	}
	public function find($id_chap,$id_art){
		
		$this->db->select('numero');
		$this->db->where('id_chapitre',$id_chap);
		$this->db->where('id_article',$id_art);
        $query = $this->db->get('Fiche');

        return $query->result();
 }
	public function findtrans($id_chap,$id_art){

		$this->db->select_max('numero');
		$this->db->where('id_chapitre',$id_chap);
		$this->db->where('id_article',$id_art);
		$this->db->where('nom',"FICHE DE TRANSFERT");
		$query = $this->db->get('Fiche');

		return $query->result();
	}
 	public function get_FT(){
		
		$this->db->select('id_chapitre');
		$this->db->select('id_article');
		$this->db->where('nom',"FICHE DE TRANSFERT");
        $query = $this->db->get('Fiche');

        return $query->result();
 }
        public function nummandat($id_facture){
            $this->db->select('numero');
            $this->db->where('id_facture', $id_facture);
            $this->db->where('nom', 'MANDAT DE PAIEMENT');
            $mandat = $this->db->get('Fiche')->result();
            if($mandat) 
                return $mandat[0];
            else
                return null; 
        }
        
        public function mandatsBetweenDates($startDate, $endDate){
            $this->db->distinct();
            $this->db->select('id_facture');
            $this->db->from('Facture');
            $this->db->join('Fiche', 'Facture.id = Fiche.id_facture', 'inner');
            $this->db->where('nom', "MANDAT DE PAIEMENT");
            $this->db->where("date_etablissement BETWEEN '".$startDate."' AND '".$endDate."'");
            return $this->db->get()->result();
            
//            SELECT DISTINCT nom, numero, id_facture, date_etablissement FROM Facture INNER JOIN Fiche ON Facture.id = Fiche.id_facture WHERE nom = "MANDAT DE PAIEMENT" AND date_etablissement BETWEEN '2016-05-22' AND '2016-05-24';
            }
	public function supprimerch($id_facture){
		$this->db->delete('Fiche', array('id_facture' => $id_facture,'nom'=>'CH50'));

	}
	public function supprimeravetm($id_facture){
		$this->db->delete('Fiche', array('id_facture' => $id_facture,'nom'=>'MANDAT DE PAIEMENT'));
		$this->db->delete('Fiche', array('id_facture' => $id_facture,'nom'=>'AVIS DE VIREMENT'));


	}
}
 ?>
