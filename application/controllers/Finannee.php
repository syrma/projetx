<?php

class FinAnnee extends CI_Controller {

   function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');
        $this->load->helper('file');  
        } 
        $this->load->model('Archivage');
        $this->load->model('Etat');
        }  
    
    public function index(){
        $this->Archivage->archiver();
        $this->Etat->finannee();
        $data = array(
           'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert"> Les données ont été archivées. Fin de l\'année effectuée.</div>'
           );
        $this-> session-> set_flashdata('msg',$data);
	redirect('parametres', 'refresh');

   }
}
?>
