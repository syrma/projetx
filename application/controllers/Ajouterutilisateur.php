<?php

class Ajouterutilisateur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh'); 
        } 
        $this->load->model ('Utilisateurs');
        $this->load->model ('Questions');

    }
    
    public function index(){

        $data['msg']='';
        $data['ctg']=$this->session->userdata('categorie');
        $data['qsts']=$this->Questions->consulter();
        $this->load->view('ajouter_utilisateur',$data);
    }
    
    public function ajouter(){
        $nom = $_POST["nom"];
        $prenom = $_POST["prenom"];
        $email = $_POST["email"];
        $mdp = $_POST["pwd"];
        $QS = $_POST["qs"];
        $RS = $_POST["rs"];
        $ctg=$_POST["ctg"];
        $autre=$_POST["autre"];
        $qsa=$_POST["qsa"];
        if($autre=="autre")
        {
            if(trim($qsa)==""||trim($nom)==""||trim($prenom)==""||trim($email)==""||trim($mdp)==""||trim($QS)==""||trim($RS)==""||trim($ctg)=="")
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
                );
                $this-> session-> set_flashdata('msg',$data);
                redirect('ajouterutilisateur','refresh');
            }
            else
            {
                $mdp = password_hash($mdp,PASSWORD_DEFAULT);

                $tableau=$this->Utilisateurs->verifier_utilisateur($email);
                $b=true;
                foreach ($tableau as $row)
                {
                    $b=false;


                }
                if($b){
                    $idQ= $this->Utilisateurs->ajouter_question($qsa);

                   if($ctg!="Doyen"){ $this->Utilisateurs->ajouter($nom,$prenom,$email,$mdp,$idQ,$RS,$ctg);}
                    else{$this->Utilisateurs->ajouterdoyen($nom,$prenom,$email,$mdp,$idQ,$RS);}
                    redirect('utilisateur','refresh');
                }
                else{
                    $data = array(
                        'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">L\'adresse entrée est déjà utilisée.</div>'
                    );
                    $this-> session-> set_flashdata('msg',$data);
                    redirect('ajouterutilisateur','refresh');
                }
            }
        }
        else
        {
        if(trim($nom)==""||trim($prenom)==""||trim($email)==""||trim($mdp)==""||trim($QS)==""||trim($RS)==""||trim($ctg)=="")
        {

            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('ajouterutilisateur','refresh');
        }
        else{

            $mdp = password_hash($mdp,PASSWORD_DEFAULT);

        $tableau=$this->Utilisateurs->verifier_utilisateur($email);
        $b=true;
        foreach ($tableau as $row)
        {
            $b=false;


        }
        if($b){

            if($ctg!="Doyen"){ $this->Utilisateurs->ajouter($nom,$prenom,$email,$mdp,$QS,$RS,$ctg);}
            else{$this->Utilisateurs->ajouterdoyen($nom,$prenom,$email,$mdp,$QS,$RS);}

       redirect('utilisateur','refresh');
      }
        else{
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">L\'adresse entrée est déjà utilisée.</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('ajouterutilisateur','refresh');
        }

    }
    
}}
}
?>
