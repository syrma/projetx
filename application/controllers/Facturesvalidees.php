<?php

class Facturesvalidees extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model('Facture');
    }

    public function index(){
        $tableau=$this->Facture->consulter(1,1);
        $tabb=array();
        foreach($tableau as $row){
            $facture = new Mafacture();
            $facture->id=$row->n_facture;
            $facture->num=$row->id;
            $facture->id_chapitre=$row->id_chapitre;
            $facture->id_article=$row->id_article;
            $facture->date=$row->date_facture;
            $facture->montant=$row->montant;
            $tableauu=$this->Facture->nom_fourn($row->fournisseur);
            foreach($tableauu as $roow){
                $facture->fournisseur=$roow->nom;
            }
            $tabb []=$facture;
        }

        $tableauu['records']=$tabb;
        $this->load->view('factures_validees',$tableauu);
    }
}
class Mafacture {
    var $id;
    var$num;
    var $id_article;
    var $id_chapitre;
    var $date;
    var $montant;
    var $fournisseur;
}
?>
