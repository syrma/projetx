<?php

class Gestionchapitres extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
        $this->load->model('Chapitre');
        $this->load->model('Article');
        $this->load->model('Etat');

    }   
    
    public function index(){
        $en=(int)$_GET["en"];

        $tableau=$this->Chapitre->consulter();
        $this->load->model ('Article');
        $tableaubudget=$this->Article->budget_chapitre();
        $tableauchapitres  = array();
        foreach($tableau as $roow) {
            $chap = new chapitres();
            $chap->id=$roow->id;
            $chap->budget=0;
            foreach ($tableaubudget as $row) {
                if($chap->id==$row->id_chapitre){
                    $chap->budget+=$row->budget;
                }
            }
            $tableauchapitres[]=$chap;
        }

            $tabb ['records']=  $tableauchapitres;
        $etat=$this->Etat->verifier();

        if($en==1){$tabb['en']=1;}
        else{$tabb['en']=0;}
        if($etat!=null)
        {$tabb['sem']=$etat[0]->semestre;
        }else{
            $tabb['sem']=0;
        }
        $this->load->view("gestion_chapitres",$tabb);
    }
    public function supprimer()
    {

        $id_chapitre=$_GET["id"];
        $tableau=$this->Article->liste($id_chapitre);

        if($tableau==null)
        {
            $this->Chapitre->supprimer($id_chapitre);
            $url='gestionchapitres?en=0';
            redirect($url,'refresh');
        }
        else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Ipossible de supprimer ce chapitre !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            $url='gestionchapitres?en=0';
            redirect($url,'refresh');
        }

    }
}
class chapitres {
    var $id;
    var $budget;
}
?>
