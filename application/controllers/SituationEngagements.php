﻿<?php

class SituationEngagements extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        } 
	$this->load->model ('Chapitre');
    $this->load->model ('Article');
        $this->load->model ('Transferts');
        $this->load->model ('Budgetcomp');
        $this->load->model ('Facture');



    }  
    
    public function index(){
       $this->load->view('fiches/situation_engagements',$this->data, true);
       
    }
	public function imprimerSE()
    {
       $datedebut=$_POST["startDate"];
        $datefin=$_POST["endDate"];

        $chapitres1=$this->Chapitre->consultersec1();
        $sec1=new masection();
        $sec1->budget_initial=0;
        $sec1->budget_complementaire=0;
        $sec1->transs=0;
        $sec1->transc=0;
        $sec1->definitif=0;
        $sec1->solde=0;
        $sec1->taux=0;
        $tabb=array();
        foreach($chapitres1 as $row)
        {
            $c=new monchapitre();
            $c->id=$row->id;
            $c->nom=$row->nom;
            $budget=$this->Article->budget_initial_chapitre($row->id);
            $budget_initial=0;
            foreach($budget as $rrow)
            {
                $budget_initial+=$rrow->budget_initial;
            }
            $c->budget_initial=$budget_initial;
            $sec1->budget_initial+=$budget_initial;
            $budget_c=$this->Budgetcomp->selectchap($row->id,$datedebut,$datefin);
            $budget_complementaire=0;
            foreach($budget_c as $rrow)
            {
                $budget_complementaire+=$rrow->montant;
            }
            $c->budget_complementaire=$budget_complementaire;
            $sec1->budget_complementaire+=$budget_complementaire;
            $transfertssource=$this->Transferts->selectsourcechap($row->id,$datedebut,$datefin);
            $transs=0;
            foreach($transfertssource as $trans)
            {
                $transs+=$trans->montant_transfert;
            }
            $c->transs=$transs;
            $sec1->transs+=$transs;
            $transfertscible=$this->Transferts->selectciblechap($row->id,$datedebut,$datefin);
            $transc=0;
            foreach($transfertscible as $trans)
            {
                $transc+=$trans->montant_transfert;
            }
            $c->transc=$transc;
            $sec1->transc+=$transc;
            $c->definitif=$c->budget_initial + $c->budget_complementaire+$transc-$transs;
            $sec1->definitif+=$c->definitif;
            $facture=$this->Facture-> paimentchap($row->id,$datedebut,$datefin);
            $paiement=0;
            foreach($facture as $rrow)
            {
                $paiement+=$rrow->montant;
            }
            $c->solde=$c->definitif- $paiement;
            $sec1->solde+=$c->solde;
            if($c->budget_initial!=0)
            {$c->taux=($paiement*100)/$c->definitif;}else$c->taux=0;
            $sec1->taux+=$c->taux;
            $tabb[]=$c;

        }
        $this->data['chapitre1']=$tabb;
        $this->data['section1']=$sec1;
        $chapitres2=$this->Chapitre->consultersec2();
        $sec2=new masection();
        $sec2->budget_initial=0;
        $sec2->budget_complementaire=0;
        $sec2->transs=0;
        $sec2->transc=0;
        $sec2->definitif=0;
        $sec2->solde=0;
        $sec2->taux=0;
        $tabbb=array();
        foreach($chapitres2 as $row)
        {
            $c=new monchapitre();
            $c->id=$row->id;
            $c->nom=$row->nom;
            $budget=$this->Article->budget_initial_chapitre($row->id);
            $budget_initial=0;
            foreach($budget as $rrow)
            {
                $budget_initial+=$rrow->budget_initial;
            }
            $c->budget_initial=$budget_initial;
            $sec2->budget_initial+=$budget_initial;
            $budget_c=$this->Budgetcomp->selectchap($row->id,$datedebut,$datefin);
            $budget_complementaire=0;
            foreach($budget_c as $rrow)
            {
                $budget_complementaire+=$rrow->montant;
            }
            $c->budget_complementaire=$budget_complementaire;
            $sec2->budget_complementaire+=$budget_complementaire;
            $transfertssource=$this->Transferts->selectsourcechap($row->id,$datedebut,$datefin);
            $transs=0;
            foreach($transfertssource as $trans)
            {
                $transs+=$trans->montant_transfert;
            }
            $c->transs=$transs;
            $sec2->transs+=$transs;
            $transfertscible=$this->Transferts->selectciblechap($row->id,$datedebut,$datefin);
            $transc=0;
            foreach($transfertscible as $trans)
            {
                $transc+=$trans->montant_transfert;
            }
            $c->transc=$transc;
            $sec2->transc+=$transc;
            $c->definitif=$c->budget_initial + $c->budget_complementaire+$transc-$transs;
            $sec2->definitif+=$c->definitif;
            $facture=$this->Facture-> paimentchap($row->id,$datedebut,$datefin);
            $paiement=0;
            foreach($facture as $row)
            {
                $paiement+=$row->montant;
            }
            $c->solde=$c->definitif- $paiement;
            $sec2->solde+=$c->solde;
            if($c->budget_initial!=0)
            {$c->taux=($paiement*100)/$c->definitif;}else$c->taux=0;
            $sec2->taux+=$c->taux;
            $tabbb[]=$c;

        }
        $this->data['chapitre2']=$tabbb;
        $this->data['section2']=$sec2;

        $articles=$this->Article->select();
        $tab=array();
       foreach($articles as $row)
        {
            $a = new monarticle();
            $a->id=$row->id;
            $a->id_chapitre=$row->id_chapitre;
            $a->nom=$row->nom;
            $a->budget_initial=$row->budget_initial;
            $budgetcomp=$this->Budgetcomp->select($row->id,$row->id_chapitre,$datedebut,$datefin);
            $budgetcomplementaire=0;
            foreach($budgetcomp as $bud)
            {
               $budgetcomplementaire+=$bud->montant;
            }
            $a->budget_complementaire=$budgetcomplementaire;
            $transfertssource=$this->Transferts->selectsource($row->id,$row->id_chapitre,$datedebut,$datefin);
           $transs=0;
            foreach($transfertssource as $trans)
            {
               $transs+=$trans->montant_transfert;
            }
          $a->transs=$transs;
            $transfertscible=$this->Transferts->selectcible($row->id,$row->id_chapitre,$datedebut,$datefin);
            $transc=0;
            foreach($transfertscible as $trans)
            {
                $transc+=$trans->montant_transfert;
            }
            $a->transc=$transc;

            $a->definitif=$a->budget_initial + $a->budget_complementaire+$transc-$transs;
            $facture=$this->Facture-> paiment($row->id,$row->id_chapitre,$datedebut,$datefin);
            $paiement=0;
            foreach($facture as $row)
            {
                $paiement+=$row->montant;
            }
            $a->solde=$a->definitif- $paiement;
            if($a->budget_initial!=0)
            {$a->taux=($paiement*100)/$a->definitif;}else$a->taux=0;
            $tab[]=$a;
        }
        $this->data['article'] = $tab;
        $somme=$sec1->solde+$sec2->solde;
        $this->data['montantL']=doubleToLetters((double)$somme);

        $html=$this->load->view('fiches/situation_engagements',$this->data, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
		$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	    $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;
    }
}
class monchapitre
{
    var $id;
    var $nom;
    var $budget_initial;
    var $budget_complementaire;
    var $transs;
    var $transc;
    var $definitif;
    var $solde;
    var $taux;

}
class monarticle
{
    var $id;
    var $id_chapitre;
    var $nom;
    var $budget_initial;
    var $budget_complementaire;
    var $transs;
    var $transc;
    var $definitif;
    var $solde;
    var $taux;
}
class masection
{
    var $budget_initial;
    var $budget_complementaire;
    var $transs;
    var $transc;
    var $definitif;
    var $solde;
    var $taux;
}
include('nel.php');

?>
