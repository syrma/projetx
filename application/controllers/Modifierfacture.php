<?php

class Modifierfacture extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Fournisseur');
        $this->load->model ('Facture');
        $this->load->model ('Chapitre');
        $this->load->model ('Article');
        $this->load->model ('Fiche');
        $this->load->model ('Etat');


    }

    public function index(){
        $id=$_GET["id"];
        $tabb=array();
        $tableau=$this->Facture->consulter_facture($id);
        foreach($tableau as $row){
            $facture = new Mafacture();
            $facture->num=$id;
            $facture->id=$row->n_facture;
            $facture->id_chapitre=$row->id_chapitre;
            $facture->id_article=$row->id_article;
            $facture->date=$row->date_facture;
            $facture->montant=$row->montant;
           $numero_mandat=$this->Fiche->numero($id);
            if($numero_mandat!=null)
            {$facture->num_mandat=$numero_mandat[0]->numero;}
            else{$facture->num_mandat=0;}

            $facture->id_fournisseur=$row->fournisseur;
            $tableauu=$this->Facture->nom_fourn($row->fournisseur);
            foreach($tableauu as $roow)
            {
                $facture->rib=$roow->rib_fournisseur;
                if($roow->type_fournisseur==1)
                    $facture->fournisseur=$roow->nom;
                else  $facture->personne=$roow->nom;

            }
            $tabb []=$facture;
        }
        $tableauu['records']=$tabb;
        $tableauu['recordsc']=$this->Chapitre->consulter();

        if(isset($tableauu['recordsc']))
            $tableauu['recordsa']=$this->Article->liste($tableauu['recordsc'][0]->id);

        if(isset($tableauu['recordsc']) &&isset($tableauu['recordsa']))
        {
            $tableauu['recordss']=$this->Fournisseur->idfournisseur();
            $tableau['recordsp']=$this->Fournisseur->idpersonnes();
            $this->load->view('modifier_facture',$tableauu);
        }
        else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Il n\'existe aucun article !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('facturee','refresh');
       }
    }
    public function modifier(){
        $id=$_POST["id"];
        $id_article=$_POST["ida"];
        $id_chapitre=$_POST["idc"];
        $num = $_POST["NFacture"];
        $montant=str_replace(' ','',$_POST["montant"]);
        $montant = str_replace(',','.',$montant);
        $montant = str_replace('D.A','',$montant);
        $nom_societe=$_POST["societe"];
        $adr= $_POST["adrS"];
        $nom_banque= $_POST["nomB"];
        $numero_banque= $_POST["compte"];
        $autre=$_POST ["autresoc"];
       $nummandat=$_POST["mendat"];
        $rib=$_POST["rib"];
        $type=$_POST["type"];
        $fiche=$_POST["opt"]  ;
        $montanta=$_POST["montanta"];

        if($type=="societe"){ $id_societe = $_POST["soc"];}else{ $id_societe = $_POST["per"];}
        //tester si les champs sont vide au cas ou une société existante a été sélectionnée

        if($fiche==null)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez sélectionner une methode d\'impression !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            //redirect('facturee','refresh');
        }
        else
        {
        if($montant<=0||trim($id_societe)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('facturee','refresh');
        }
        else{

            $budget = $this->Article->budget($id_article, $id_chapitre);
            //effectuer l'opération ancien budget - montant de la facture
            foreach ($budget as $row) {
                $budg = $row->budget;
            }
            $somme=$budg+$montanta-$montant;
            if ($somme <= 0) {
                //si le budget est insuffisant message d'erreur
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Budget insuffisant !!</div>'
                );
                $this->session->set_flashdata('msg', $data);
                redirect('facturee', 'refresh');
            }
            if($autre=="autre")
            {
                //tester si les champs sont vides si on a choisi d'entrer une nouvelle société
                if (trim($adr) != "" && trim($nom_banque) != "" && trim($numero_banque) != "" && trim($nom_societe) != "")
                {
                    if ($type == "societe") {
                        $typefournisseur = 1;
                    } else {
                        $typefournisseur = 0;
                    }
                    if ($fiche == "mandat")
                    {
                        if (trim($rib) == "" ||strlen($rib)!=20||!(float)$rib|| trim($nummandat) == "") {
                            $data = array(
                                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
                            );
                            $this->session->set_flashdata('msg', $data);
                            redirect('facturee', 'refresh');
                        } else {
                            $id_societe = $this->Fournisseur->ajouter_rib($nom_societe, $adr, $nom_banque, $numero_banque, $typefournisseur, $rib);

                        }
                    }
                    else
                    {
                        $id_societe = $this->Fournisseur->ajouter($nom_societe, $adr, $nom_banque, $numero_banque, $typefournisseur);
                    }
                }
                else
                {
                    $data = array(
                        'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
                    );
                    $this->session->set_flashdata('msg', $data);
                    redirect('facturee', 'refresh');
                }
            }
            if($fiche=="mandat")
            {
                if(trim($rib)==""||strlen($rib)!=20||!(float)$rib||trim($nummandat)=="")
                {
                    $data = array(
                        'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
                    );
                    $this-> session-> set_flashdata('msg',$data);
                    redirect('facturee','refresh');
                }
                else
                {
                    $this->Facture->modifier($id,$id_societe,$num,$montant);
                    $this->Article->retrait($id_chapitre, $id_article, $somme);
                    $fiche=$this->Fiche->numero($id);
                    $this->Fournisseur->modifier_rib($rib,$id_societe);
                    if($fiche!=null)
                    {$this->Fiche->modifier_num($nummandat,$id);
                   }
                    else{
                        $tab=$this->Etat->verifier();
                        $this->Fiche->ajouter("MANDAT DE PAIEMENT",$tab[0]->semestre,$nummandat,$id_chapitre,$id_article,$id,NULL);
                        $this->Fiche->ajouter("AVIS DE VIREMENT",$tab[0]->semestre,0,$id_chapitre,$id_article,$id,NULL);
                        $this->Fiche->supprimerch($id);
                    }

                    $data = array(
                        'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez modifié une facture.</div>'
                    );
                    $this->session->set_flashdata('msg', $data);
                    redirect('facturee', 'refresh');
                }
            }
            else{
                $tab=$this->Etat->verifier();

                $this->Fiche->ajouter("CH50",$tab[0]->semestre,0,$id_chapitre,$id_article,$id,null);
                $this->Fiche->supprimeravetm($id);

                //ajouter une facture et retirer son montant du budget de l'article


                $this->Facture->modifier($id,$id_societe,$num,$montant);

                $this->Article->retrait($id_chapitre, $id_article, $somme);

                $data = array(
                    'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez modifié une facture.</div>'
                );
                $this->session->set_flashdata('msg', $data);
                redirect('facturee', 'refresh');



            }
        }


    }}


}
class Mafacture {
    var $id;
    var $num;
    var $id_article;
    var $id_chapitre;
    var $date;
    var $montant;
    var $fournisseur;
    var $personne;
    var $num_mandat;
    var $rib;
    var $id_fournisseur;
}
?>
