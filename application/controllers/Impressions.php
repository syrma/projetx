<?php

class Impressions extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');

        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model('Etat');
        $tableau['etat']= $this->Etat->verifier();

        if($tableau==null)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord le RIB de l\'université !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('parametres','refresh');
        }
        $this->load->model('Article');
    }

    public function index()
    {


        $tableau['etat']= $this->Etat->verifier();
        $tableau['articles']=$this->Article->consulter();
        $this->load->view('impressions',$tableau);

    }




}
?>
