<?php

class Utilisateur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
        $this->load->model ('Utilisateurs');
    }  
    
    public function index(){
        $ctg=$this->session->userdata('categorie');
        $tableau ['records']= $this->Utilisateurs->trouver_utilisateurs($ctg);
        $tableau['recordss']= $this->Utilisateurs->trouver_utilisateurs_bloques($ctg);
        $this->load->view('utilisateur',$tableau);
    }

    public function parametres(){
        $this->load->view('parametres');
    }
    
    public function profile(){
        $this->load->view('profile');
    }
    public function debloquer (){
        $id=$_GET['id'];
        $this->Utilisateurs->debloquer($id);
        redirect('utilisateur','refresh');

    }
}
?>
