<?php

class Profil extends CI_Controller {

    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
    );
           $this-> session-> set_flashdata('errauth',$data);
           redirect('test','refresh');  
           }
           
        $this->load->model('Utilisateurs');
            $this->load->model('Questions');

        }  
    
    public function index(){
        $mail = $this->session->userdata('email');
        $data = $this->Utilisateurs->verifier_utilisateur($mail);
        $tabb=array();
        if($data!=null)
        {
            $profil=new MonProfil();
            $profil->nom=$data[0]->nom;
            $profil->prenom=$data[0]->prenom;
            $qst=$this->Questions->verifier($data[0]->questionS);
            $profil->question=$qst[0]->enonce;
            $profil->mail=$mail;
            $profil->reponse=$data[0]->reponseS;
            $tabb[]=$profil;

            $tab['profil']=$tabb;


        }
        $this->load->view('profile', $tab);
    }
    public function modifier()
    {
        $mail=$_POST["email"];
        $qst=$_POST["qs"];
        $rep=$_POST["rep"];
        $mdp1=$_POST["mdp1"];
        $mdp2=$_POST["mdp2"];
        $email=$this->session->userdata('email');

        if(trim($mail)==""||trim($qst)==""||trim($rep)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-8 col-md-offset-2" role="alert">Veuillez remplir tous les champs !!</div>',

            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('profil','refresh');
        }
        else
        {
            if($mdp1==$mdp2)
            {
                if(trim($mdp1)==""||trim($mdp2)=="")
                {
                    $idQ= $this->Utilisateurs->ajouter_question($qst);

                    $this->Utilisateurs->modifierprofil($mail,$idQ,$rep,"",$email);
                    $sessionData = array(
                        'email' => $mail
                    );

                    $this->session->set_userdata($sessionData);
                    redirect('accueil','refresh');
                }
                else
                {
                    $idQ= $this->Utilisateurs->ajouter_question($qst);
                    $mdp1 = password_hash($mdp1,PASSWORD_DEFAULT);
                    $this->Utilisateurs->modifierprofil($mail,$idQ,$rep,$mdp1,$email);
                    $sessionData = array(
                        'email' => $mail
                    );

                    $this->session->set_userdata($sessionData);
                    redirect('accueil','refresh');
                }
            }
            else
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-8 col-md-offset-2" role="alert">Mots de passes non-similaires !!</div>',

                );
                $this-> session-> set_flashdata('msg',$data);
                redirect('profil','refresh');
            }
        }
    }
}
class MonProfil {
    var $nom;
    var $prenom;
    var $mail;
    var $question;
    var $reponse;
}
?>
