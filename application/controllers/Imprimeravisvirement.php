<?php

class Imprimeravisvirement  extends CI_Controller {
    private $donnee = array();
    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }

        $this->load->model ('Facture');
        $this->load->model ('Fournisseur');
        $this->load->model ('Fiche');
        $this->load->model ('Etat');


    }

    public function index(){
        $id_facture=$_GET["id"];
        $rib=$this->Etat->verifier();
        $tableau["ribuniv"]=$rib[0]->rib_univ;
        $facture=$this->Facture->verifier($id_facture);
        $tableau["numfact"]=$facture[0]->n_facture;
        $tableau["datefact"]=$facture[0]->date_facture;
        $tableau["montant"]=$facture[0]->montant;
        $montant=$facture[0]->montant;
       $tableau["montantL"]=doubleToLetters((double)$montant);
        $fournisseur=$this->Fournisseur-> consulter_id($facture[0]->fournisseur);
        $tableau["fournisseur"]=$fournisseur[0]->nom;
        if($fournisseur[0]->type==0)
       {$tableau["prenom"]=$fournisseur[0]->prenom;}else{$tableau["prenom"]="";}
        $tableau["adresse"]=$fournisseur[0]->adr;
        $tableau["rib"]=$fournisseur[0]->rib_fournisseur;


        $html=$this->load->view('fiches/avis_virement_rempli',$tableau, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
        $this->m_pdf->pdf->autoScriptToLang = true;
        $this->m_pdf->pdf->autoLangToFont = true;
        $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");

        exit;
    }


}

include('nel.php');
?>
