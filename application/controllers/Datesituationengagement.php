<?php

class Datesituationengagement extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Chapitre');
        $this->load->model ('Article');

        $this->data['chapitre1'] =$this->Chapitre->consultersec1();
        $this->data['chapitre2'] =$this->Chapitre->consultersec2();

        $this->data['article'] = $this->Article->select();
    }

    public function index(){
        $this->load->view('date_situation_engagement');

    }

}
?>
