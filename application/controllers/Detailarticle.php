<?php

class Detailarticle extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Article');
        $this->load->model ('Chapitre');

    }

    public function index(){
        $id=$_GET["id"];
        $idc=$_GET["idc"];
        $transferts = $this->Article->consulter_transferts_source($id, $idc);
        $tableau=array();
        foreach($transferts as $row){
            $transfert=new Transfert();
            $tab=$this->Article->consulter_article($row->article_cible,$row->chapitre_cible);
            $transfert->article=$tab[0]->nom;
            $tabb=$this->Chapitre->consulterunchapitre($row->chapitre_cible);
            $transfert->chapitre=$tabb[0]->nom;
            $transfert->montant=$row->montant_transfert;
            $tableau[]=$transfert;
        }
        $transfertss = $this->Article->consulter_transferts_cible($id, $idc);
        $table=array();
        foreach($transfertss as $row){
            $transfert=new Transfert();
            $tab=$this->Article->consulter_article($row->article_source,$row->chapitre_source);
            $transfert->article=$tab[0]->nom;
            $tabb=$this->Chapitre->consulterunchapitre($row->chapitre_source);
            $transfert->chapitre=$tabb[0]->nom;
            $transfert->montant=$row->montant_transfert;
            $table[]=$transfert;
        }
        $records["cible"]=$table;
        $records["source"]=$tableau;
        $records["details"]=$this->Article->consulter_article($id, $idc);
        $this->load->view('detail_article',$records);
    }



}
class Transfert {
    var $article;
    var $chapitre;
    var $montant;
}
?>
