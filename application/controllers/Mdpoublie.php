<?php

class Mdpoublie extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('Utilisateurs');
        $this->load->model('Questions');


    }

    public function index(){
        $email=$_POST['email'];
        $tableau=$this->Utilisateurs->verifier_utilisateur($email);
       $tabb=array();
        if($tableau!=null)
        {
            $profil=new MonProfil();
            $profil->nom=$tableau[0]->nom;
            $profil->prenom=$tableau[0]->prenom;
            $qst=$this->Questions->verifier($tableau[0]->questionS);
            $profil->question=$qst[0]->enonce;
            $tabb[]=$profil;
            $tab["id"]=$email;
            $tab['profil']=$tabb;
            $this->load->view('modifier_mdp',$tab);

        }
        else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Email introuvable !!</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }


    }
    public function mdp ()
    {
        $reponse=$_POST['rep'];

        $mdp1=$_POST['mdp1'];
        $mdp2=$_POST['mdp2'];
        $email=$_POST['email'];
        if(trim($reponse)==""||trim($mdp1)==""||trim($mdp2)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');

        }
        else
        {
            if($mdp1==$mdp2)
            {
            $tab=$this->Utilisateurs->consulter($email);
            $rep=$tab[0]->reponseS;
            if($rep==$reponse)
            {
                $mdp1 = password_hash($mdp1,PASSWORD_DEFAULT);
                $this->Utilisateurs->modifier_mdp($email,$mdp1);
                redirect('test','refresh');
            }
            else
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Réponse incorrecte !!</div>'
                );
                $this-> session-> set_flashdata('errauth',$data);
                redirect('test','refresh');
            }
            }
            else
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Mots de passes non-similaires !!</div>'
                );
                $this-> session-> set_flashdata('errauth',$data);
                redirect('test','refresh');
            }
        }


    }
}
class MonProfil {
    var $nom;
    var $prenom;
    var $question;
}
?>
