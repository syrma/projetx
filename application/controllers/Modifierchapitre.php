<?php
/**
 * Created by PhpStorm.
 * User: souad
 * Date: 24/04/2016
 * Time: 18:07
 */
class Modifierchapitre extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Chapitre');
        $this->load->model ('Etat');
    }

    public function index(){
        $id=$_GET["id"];
        $tableau['id']=$id;
        $tableau['records']=$this->Chapitre->consulterunchapitre($id);
        $this->load->view('modifier_chapitre',$tableau);
    }
    public function modifier (){
        $tableau= $this->Etat->verifier();
        if($tableau==null)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord le RIB de l\'université !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('parametres','refresh');
        }
        else
        {
            if($tableau[0]->budget_initial==0)
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer le budget initial !!</div>'
                );
                $this-> session-> set_flashdata('msg',$data);
                redirect('engagement','refresh');
            }
        }
        $id=(int)$_POST["code"];
        $nom=$_POST["nom"];
        $idc=(int)$_POST["idc"];
        if(trim($id)==""||trim($nom)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>',

            );
            $this-> session-> set_flashdata('msg',$data);

            redirect('gestionchapitres','refresh');
        } else{
            $tableau=$this->Chapitre->verifier($id);
            $i=0;

            foreach ($tableau as $row)
            {
                $i++;


            }
            if(($i==1 && $idc==$id)||($i==0 && $idc!=$id ))
            {
        $tableau['records']=$this->Chapitre->modifier($id,$nom,$idc);
                $data = array(
                    'msg' => '<div class="alert alert-success col-md-8 col-md-offset-2" role="alert">Vous avez modifié un chapitre.</div>',

                );
                $this-> session-> set_flashdata('msg',$data);
                $url='gestionchapitres?en=0';
                redirect($url,'refresh');
            }
            else{

                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Ce code existe déjà !</div>',

                );
                $this-> session-> set_flashdata('msg',$data);
                redirect('gestionchapitres','refresh');
            }
        }
    }
}
?>
