﻿<?php

class FAvisDeVirement extends CI_Controller {
private $donnee = array();
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Facture');
	   $this->load->model ('Fournisseur');
	   $this->load->model ('Fiche');
	    $this->load->model ('Etat');
		
	 
        } 
	
   public function index(){  
   $size=$_GET['t'];
	for($i = 0; $i < $size; $i++){
		$ab='n'.$i;
		$nAvis[]=$_GET[$ab];
		$Avis[]=$this->Fiche->avis($_GET[$ab]);
		$fact[]=$this->Facture->verifier($_GET[$ab]);
	}

	foreach($fact as $ligne){
		foreach($ligne as $l){
			 $somme[]= doubleToLetters((double) $l->montant);
			 $four[]=$this->Fournisseur->consulter_id($l->fournisseur);
	}}
	
		$this->donnee['montantL']=$somme;
		$this->donnee['fournisseur']=$four;
		$this->donnee['facture']=$fact;
		$this->donnee['Avis']=$Avis;		
		$this->donnee['NAvis']=$nAvis;			
		$this->donnee['Etat']=$this->Etat->verifier();	
	    $this->donnee['AvisVide']="0";
		
		$html=$this->load->view('fiches/AvisDeVirement',$this->donnee, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
		$this->m_pdf->pdf->autoScriptToLang = true;
		$this->m_pdf->pdf->autoLangToFont = true;
	    $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;   
    }
	
	public function imprimerAV()
    {
        $html=$this->load->view('fiches/AvisV',$this->data, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
		$this->m_pdf->pdf->autoScriptToLang = true;
		$this->m_pdf->pdf->autoLangToFont = true;
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	    $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;
    }
	
}
include('nel.php');
?>
