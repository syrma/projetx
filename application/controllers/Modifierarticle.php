<?php
/**
 * Created by PhpStorm.
 * User: souad
 * Date: 24/04/2016
 * Time: 18:06
 */
class Modifierarticle extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Article');
        $this->load->model ('Etat');
        $this->load->model ('Fiche');
        $this->load->model ('Facture');
        $this->load->model ('Transferts');


    }

    public function index(){

        $id=trim($_GET["id"]);
        $idc=trim($_GET["idc"]);

        $tableau['records']=$this->Article->budget($id,$idc);
        $tableau['id']=$id;
        $tableau['idc']=$idc;
        $this->load->view('modifier_article',$tableau);
    }
public function modifier (){
    $tableau= $this->Etat->verifier();
    if($tableau==null)
    {
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">veuillez entrer d abord le RIB de l universite !!</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        redirect('parametres','refresh');
    }
    else
    {
        if($tableau[0]->budget_initial==0)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer le budget initial !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('engagement','refresh');
        }
    }
    $id=$_POST['code'];
    $nom=$_POST['nom'];
    $budget_initial=$_POST['montant'];
    $budget_initial = str_replace(' ','',$budget_initial);
    $budget_initial = str_replace('D.A','',$budget_initial);
    $budget_initial = (int) str_replace(',','.',$budget_initial);
    $budget_complementaire = $_POST['montantc'];
    $budget_complementaire = str_replace(' ','',$budget_complementaire);
    $budget_complementaire = str_replace('D.A','',$budget_complementaire);
    $budget_complementaire = (int) str_replace(',','.',$budget_complementaire);
    $idb=$_POST['idb'];
    $idc=$_POST['idc'];
    $budg=(int)$_POST['budg'];
    if(trim($id)==""||trim($nom)==""||($budget_initial<=0&&$budget_complementaire<=0))
    {
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>',

        );
        $this-> session-> set_flashdata('msg',$data);
        $url='gestionarticles?id='.$idc."&&en=0";
        redirect($url,'refresh');
    } else{
        $facture=$this->Facture->verifiefacture (trim($idb),trim($idc));
        $transferts=$this->Transferts->selectsourcef(trim($idb),trim($idc));
        if($facture!=null||$transferts!=null)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Transaction impossible !!</div>',

            );
            $this-> session-> set_flashdata('msg',$data);
            $url='modifierarticle?id='.$id."&idc=".$idc."&&en=0";
            redirect($url,'refresh');
        }
        $montableau=$this->Article->verifier(trim($idc));
    $i=0;


    foreach ($montableau as $row)
    {

        if($row->id==$id)
        $i++;
    }

        if(($i==1 && $idb==$id)||($i==0 && $idb!=$id ))
    {

        $b=$budget_initial-$budg;
        $bdg=$budg+$budget_complementaire;
        $bdgc=$bdg+$b;
        $tab=$this->Etat->verifier();
        if($tab[0]->budget_initial-$budget_initial<0)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Budget insuffisant !</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            $url='ajouterarticle?id='.$id;
            redirect($url,'refresh');
        }

        else
        {
            $ficherat=$this->Fiche->consulterrat($idc,$id);
            if($ficherat!=null)
            {
                $this->Fiche->supprimerrat($idc,$id);
            }

            else{

    $this->Article->modifier($id,$nom,$budget_initial,trim($idb),trim($idc),$bdgc);

        if($budget_complementaire!=0)
        {
            $this->Article->budgcompl(trim($idc),trim($id),$budget_complementaire, $this->session->userdata('email'));
        }
                $this->Fiche->ajouter("FICHE DE RATTACHEMENT",$tab[0]->semestre,1,trim($idc),$id,NULL,NULL);
                $this->Fiche->ajouter("FICHE D'ENGAGEMENT PROVISIONNEL",$tab[0]->semestre,2,trim($idc),$id,NULL,NULL);
        $data = array(
            'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez modifié un article.</div>',
        );
        $this-> session-> set_flashdata('msg',$data);
        $url='gestionarticles?id='.trim($idc)."&&en=0";
       redirect($url,'refresh');
    }}}
    else{

        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Ce code existe déjà !</div>',

        );
        $this-> session-> set_flashdata('msg',$data);
        $url='gestionarticles?id='.$idc."&&en=0";
      redirect($url,'refresh');
    }

}}
}
?>
