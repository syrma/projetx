﻿<?php

class F_EProvi extends CI_Controller {
private $donnee = array();
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Chapitre');
       $this->load->model ('Article');
	

        } 
	
   public function index(){
	    $tableau['recordsc']=$this->Chapitre->consulter();
        $tableau['recordsa']=$this->Article->consulter();
       $tableau['sem']=$_GET["sem"];

        $this->load->view('Impression-EP',$tableau);
       
    }


	
}

?>
