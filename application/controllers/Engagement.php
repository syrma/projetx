<?php

class Engagement extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Etat');
        $this->load->model ('Article');
    }

    public function index()
    {

        $tableau['etat']= $this->Etat->verifier();
        $tab=$this->Article->budget_initial();

        $budget=0;
        foreach($tab as $row)
        {

          $budget+=$row->budget_initial;
        }
        if($budget!=0)
        {
           if($tableau['etat']!=null){
               $tableau['budget']=
                   $tableau['etat'][0]->budget_initial-$budget;
           }
        }else
        {
           if($tableau['etat']!=null)
           {
               if($tableau['etat'][0]->budget_initial!=0)
               {
                   $tableau['budget']=$tableau['etat'][0]->budget_initial;

               }
               else
               {

                   $tableau['budget']="0000";
                   $tableau['etat'][0]->budget_initial="0000";
               }
           }
        }
            $this->load->view('engagement',$tableau);

    }
    public function init()
    {
        $budget = $_POST["budget"];
        $budget = str_replace(' ','',$budget);
        $budget = str_replace('D.A','',$budget);
        $budget = str_replace(',','.',$budget);
        $tableau= $this->Etat->verifier();
        if($tableau!=null)
        {
            $this->Etat->init_budget($budget);

            redirect('engagement','refresh');
        }
        else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord le RIB de l\'université !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('parametres','refresh');
        }




    }
    public function modifier()
    {
        $budget=$_POST["budget"];
        $budget = str_replace(' ','',$budget);
        $budget = str_replace('D.A','',$budget);
        $budget = str_replace(',','.',$budget);
        $this->Etat->init_budget($budget);

        redirect('engagement','refresh');

    }



}
?>
