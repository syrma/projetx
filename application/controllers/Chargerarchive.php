<?php
include "Classes/Mafacture.php";

class Chargerarchive extends CI_Controller {

   function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
           $this-> session-> set_flashdata('errauth',$data);
           redirect('test','refresh');
           }
        }  
    
   public function index(){
        $this->load->model('Archivage');
        if(!isset($_POST["annee"]))
            redirect('archivage', 'refresh');
        $filename = $_POST["annee"];
        
        if(!$this->Archivage->charger($filename)){
        $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Une erreur est survenue.</div>'
           );
        $this-> session-> set_flashdata('msg',$data);
        redirect('archivage','refresh');
        }
        
        $transferts = $this->Archivage->consTransferts();
        $rec['transferts'] = array();
        foreach($transferts as $row){
            $auteur = $this->Archivage->consUtilisateur($row->auteur);
            $nom = $auteur[0]->nom;
            $prenom = $auteur[0]->prenom;
            $articlecible = $this->Archivage->consArticle($row->article_cible,$row->chapitre_cible);
            $chapitrecible = $this->Archivage->consChapitre($row->chapitre_cible);
            $articlesource = $this->Archivage->consArticle($row->article_source,$row->chapitre_source);
            $chapitresource = $this->Archivage->consChapitre($row->chapitre_source);
            $trans=new Transfert($row, $articlecible, $chapitrecible, $articlesource, $chapitresource, $nom, $prenom);
            $rec['transferts'][] = $trans;
        }
        
        $rec['factures'] = array();
        $factures = $this->Archivage->consFactures();
        foreach($factures as $row){
            $tableau2 = $this->Archivage->consFournisseur($row->fournisseur);
            $facture = new Mafacture($row, $tableau2);
            $rec['factures'][] = $facture;
        }

 	$this->load->view('archivage', $rec);
   }
}

include "Classes/Transfert.php";

?>
