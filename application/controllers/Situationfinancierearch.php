<?php

class Situationfinancierearch extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');

        }
        $this->load->model ('Archivage');
}

    public function index(){
        $this->load->view('fiches/situation_financiere',$this->data);
    }
	public function imprimerSF()
    {
        $datedebut=$_POST["startDate"];
        $datefin=$_POST["endDate"];
        $chapitres1=$this->Archivage->consulterChapsec1();
        $sec1=new masection();
        $sec1->budget_initial=0;
        $sec1->budget_complementaire=0;
        $sec1->transs=0;
        $sec1->transc=0;
        $sec1->definitif=0;
        $sec1->paiement=0;
        $sec1->solde=0;
        $sec1->taux=0;
        $tabb=array();
        foreach($chapitres1 as $row)
        {
            $c=new monchapitre();
            $c->id=$row->id;
            $c->nom=$row->nom;
            $budget_initial=$this->Archivage->budget_initial_chapitre($row->id);
            $c->budget_initial=$budget_initial;
            $sec1->budget_initial+=$budget_initial;
            $budget_c=$this->Archivage->selectchap($row->id,$datedebut,$datefin);
            $budget_complementaire=0;
            foreach($budget_c as $rrow)
            {
                $budget_complementaire+=$rrow->montant;
            }
            $c->budget_complementaire=$budget_complementaire;
            $sec1->budget_complementaire+=$budget_complementaire;
            $transfertssource=$this->Archivage->selectsourcechap($row->id,$datedebut,$datefin);
            $transs=0;
            foreach($transfertssource as $trans)
            {
                $transs+=$trans->montant_transfert;
            }
            $c->transs=$transs;
            $sec1->transs+=$transs;
            $transfertscible=$this->Archivage->selectciblechap($row->id,$datedebut,$datefin);
            $transc=0;
            foreach($transfertscible as $trans)
            {
                $transc+=$trans->montant_transfert;
            }
            $c->transc=$transc;
            $sec1->transc+=$transc;
            $c->definitif=$c->budget_initial + $c->budget_complementaire+$transc-$transs;
            $sec1->definitif+=$c->definitif;
            $facture=$this->Archivage-> paimentchap($row->id,$datedebut,$datefin);
            $paiement=0;
            foreach($facture as $rrow)
            {
                $paiement+=$rrow->montant;
            }
            $c->paiement=$paiement;
            $sec1->paiement=$paiement;
            $c->solde=$c->definitif- $paiement;
            $sec1->solde+=$c->solde;
            if($c->budget_initial!=0)
            {$c->taux=($paiement*100)/$c->definitif;}else$c->taux=0;
            $sec1->taux+=$c->taux;
            $tabb[]=$c;

        }
        $this->data['chapitre1']=$tabb;
        $this->data['section1']=$sec1;
        $chapitres2=$this->Archivage->consultersec2();
        $sec2=new masection();
        $sec2->budget_initial=0;
        $sec2->budget_complementaire=0;
        $sec2->transs=0;
        $sec2->transc=0;
        $sec2->definitif=0;
        $sec2->paiement=0;
        $sec2->solde=0;
        $sec2->taux=0;
        $tabbb=array();
        foreach($chapitres2 as $row)
        {
            $c=new monchapitre();
            $c->id=$row->id;
            $c->nom=$row->nom;
            $budget_initial=$this->Archivage->budget_initial_chapitre($row->id);
            $c->budget_initial=$budget_initial;
            $sec2->budget_initial+=$budget_initial;
            $budget_c=$this->Archivage->selectchap($row->id,$datedebut,$datefin);
            $budget_complementaire=0;
            foreach($budget_c as $rrow)
            {
                $budget_complementaire+=$rrow->montant;
            }
            $c->budget_complementaire=$budget_complementaire;
            $sec2->budget_complementaire+=$budget_complementaire;
            $transfertssource=$this->Archivage->selectsourcechap($row->id,$datedebut,$datefin);
            $transs=0;
            foreach($transfertssource as $trans)
            {
                $transs+=$trans->montant_transfert;
            }
            $c->transs=$transs;
            $sec2->transs+=$transs;
            $transfertscible=$this->Archivage->selectciblechap($row->id,$datedebut,$datefin);
            $transc=0;
            foreach($transfertscible as $trans)
            {
                $transc+=$trans->montant_transfert;
            }
            $c->transc=$transc;
            $sec2->transc+=$transc;
            $c->definitif=$c->budget_initial + $c->budget_complementaire+$transc-$transs;
            $sec2->definitif+=$c->definitif;
            $facture=$this->Archivage-> paimentchap($row->id,$datedebut,$datefin);
            $paiement=0;
            foreach($facture as $row)
            {
                $paiement+=$row->montant;
            }
            $c->paiement=$paiement;
            $sec2->paiement=$paiement;
            $c->solde=$c->definitif- $paiement;
            $sec2->solde+=$c->solde;
            if($c->budget_initial!=0)
            {$c->taux=($paiement*100)/$c->definitif;}else$c->taux=0;
            $sec2->taux+=$c->taux;
            $tabbb[]=$c;

        }
        $this->data['chapitre2']=$tabbb;
        $this->data['section2']=$sec2;


        $articles=$this->Archivage->selectA();
        $tab=array();
        foreach($articles as $row)
        {
            $a = new monarticle();
            $a->id=$row->id;
            $a->id_chapitre=$row->id_chapitre;
            $a->nom=$row->nom;
            $a->budget_initial=$row->budget_initial;
            $budgetcomp=$this->Archivage->selectB($row->id,$row->id_chapitre,$datedebut,$datefin);
            $budgetcomplementaire=0;
            foreach($budgetcomp as $bud)
            {
                $budgetcomplementaire+=$bud->montant;
            }
            $a->budget_complementaire=$budgetcomplementaire;
            $transfertssource=$this->Archivage->selectsource($row->id,$row->id_chapitre,$datedebut,$datefin);
            $transs=0;
            foreach($transfertssource as $trans)
            {
                $transs+=$trans->montant_transfert;
            }
            $a->transs=$transs;
            $transfertscible=$this->Archivage->selectcible($row->id,$row->id_chapitre,$datedebut,$datefin);
            $transc=0;
            foreach($transfertscible as $trans)
            {
                $transc+=$trans->montant_transfert;
            }
            $a->transc=$transc;

            $a->definitif=$a->budget_initial + $a->budget_complementaire+$transc-$transs;
            $facture=$this->Archivage-> paiment($row->id,$row->id_chapitre,$datedebut,$datefin);
            $paiement=0;
            foreach($facture as $row)
            {
                $paiement+=$row->montant;
            }
            $a->paiment=$paiement;
            $a->solde=$a->definitif- $paiement;
            if($a->budget_initial!=0)
            {$a->taux=($paiement*100)/$a->definitif;}else$a->taux=0;
            $tab[]=$a;
        }
        $this->data['article'] = $tab;
        $somme=$sec1->solde+$sec2->solde;
        $this->data['montantL']=doubleToLetters((double)$somme);

        $html=$this->load->view('fiches/situation_financiere',$this->data,true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
	$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
	$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");
        exit;
    }
}
class monarticle
{
    var $id;
    var $id_chapitre;
    var $nom;
    var $budget_initial;
    var $budget_complementaire;
    var $transs;
    var $transc;
    var $definitif;
    var $solde;
    var $taux;
    var $paiment;
}
class monchapitre
{
    var $id;
    var $nom;
    var $budget_initial;
    var $budget_complementaire;
    var $transs;
    var $transc;
    var $definitif;
    var $paiement;
    var $solde;
    var $taux;

}
class masection
{
    var $budget_initial;
    var $budget_complementaire;
    var $transs;
    var $transc;
    var $definitif;
    var $paiement;
    var $solde;
    var $taux;
}
include('nel.php');

?>
