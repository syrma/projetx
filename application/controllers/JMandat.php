﻿<?php

class JMandat extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
		$this->load->model ('Fiche');
		$this->load->model('Facture');
    }  
    
    public function index(){
	$donnee['Fiche']=$this->Fiche->consulter();

	$startDate=$_POST["startDate"];
	$endDate=$_POST["endDate"];

	$donnee['Facture']=$this->Facture->recupD($startDate,$endDate);
	$this->load->view('journal',$donnee);
	
    }
	
		
	
}
?>
