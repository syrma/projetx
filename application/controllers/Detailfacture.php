<?php
/**
 * Created by PhpStorm.
 * User: souad
 * Date: 28/04/2016
 * Time: 19:55
 */
class Detailfacture extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model('Facture');
        $this->load->model('Fournisseur');
        $this->load->model('Utilisateurs');
    }

    public function index(){
        $id=$_GET["id"];
        $tab=$this->Facture->consulter_facture($id);
        $tableau=array();
        foreach($tab as $row)
        {
            $mafacture=new MaFacture();
            $mafacture->num=$row->n_facture;
            $mafacture->chap=$row->id_chapitre;
            $mafacture->art=$row->id_article;
            $mafacture->mont=$row->montant;
            $mafacture->date=$row->date_facture;
            $mafacture->id_fournisseur=$row->fournisseur;
            $tabb=$this->Fournisseur->consulter_id($row->fournisseur);
            $mafacture->four=$tabb[0]->nom;
            $author = $this->Utilisateurs->consulter($row->auteur);
            foreach ($author as $auth) {
                $mafacture->auteur = $auth->nom." ".$auth->prenom;
            }  
            $tableau[]=$mafacture;

        }
        $factures['records']=$tableau;
       $this->load->view('deatil_facture',$factures);
    }



}
class MaFacture {
    var $num;
    var $chap;
    var $art;
    var $mont;
    var $four;
    var $date;
    var $id_fournisseur;
    var $auteur;
}
?>
