﻿<?php

class FicheRegularisationS2 extends CI_Controller {
private $donnee = array();
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Chapitre');
       $this->load->model ('Article');
       $this->load->model ('Fiche');
       $this->load->model ('Facture');
       $this->load->model ('Budgetcomp');
       $this->load->model('Etat');
       $this->load->model('Fournisseur');
       $this->load->model('Transferts');        
        } 
	
    public function index(){
	$tableau['recordsc']=$this->Chapitre->consulter();
        $tableau['recordsa']=$this->Article->consulter();
        $this->load->view('Impression-Reg2',$tableau);
    }
    
    public function imprimerFReg(){
        $ida = $_POST['article'];
        $idc = $_POST['chapitre'];
        $etat = $this->Etat->verifier();
        if($etat && !$etat[0]->date_fin_annee)
            redirect('impressions', 'refresh');
            
        $valeurs = array();
        
        $date_fin_sem1 = $etat[0]->date_fin_sem1;
        $factures = $this->Facture->cons_sem2($idc, $ida, $date_fin_sem1, $total);
        
        if(!$factures)
            redirect('impressions', 'refresh');
        
        foreach($factures as $facture){
            $fournisseurs = $this->Fournisseur->consulter_id($facture->fournisseur);
            $F = new Mafacture($facture, $fournisseurs);
            $valeurs['factures'][] = $F;        
        }
        
        $article = $this->Article->budget($ida, $idc);
        $chapitre = $this->Chapitre->consulterunchapitre($idc);
        $valeurs['total'] = $total;
        $valeurs['narticle'] = $article[0]->nom;
        $valeurs['article'] = $ida;
        $valeurs['chapitre'] = $idc;
        $valeurs['annee'] = $etat[0]->annee;
        $valeurs['nom'] = 'REGULARISATION 2ème SEMESTRE';
        $valeurs['totalLettres'] = doubleToLetters((double) $total);
        $valeurs['section'] = $chapitre[0]->sect;
        $valeurs['ancien_solde'] = $article[0]->budget_initial + $this->incidence_budget_sem2($ida, $idc, $date_fin_sem1);
        $valeurs['nouveau_solde'] = $valeurs['ancien_solde'] - $valeurs['total'];
        
        $html = $this->load->view('fiches/regularisation',$valeurs, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
	$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
	$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
        exit;
    }
    
    function incidence_budget_sem2($id, $idc, $date_fin_sem){

            $budgetcomp=0;
            $budgetcomplementaire=$this->Budgetcomp->selectsem($id,$idc,$date_fin_sem);
            foreach($budgetcomplementaire as $row)
            {
                $budgetcomp+=$row->montant;
            }
            $transs=0;
            $transfertsource=$this->Transferts->transfertsourcesem($id,$idc,$date_fin_sem);
            foreach($transfertsource as $row)
            {
                $transs+=$row->montant_transfert;
            }
            $transc=0;
            $transfertcible=$this->Transferts->transfertciblesem($id,$idc,$date_fin_sem);
            foreach($transfertcible as $row)
            {
                $transc+=$row->montant_transfert;
            }
            $paiement=0;
            $paiementfacture=$this->Facture->facturesem($id,$idc,$date_fin_sem);
            foreach($paiementfacture as $row)
            {
                $paiement+=$row->montant;
            }

           return $budgetcomp-$transs+$transc-$paiement;
    }
}
include('nel.php');
include('Classes/Mafacture.php');
?>
