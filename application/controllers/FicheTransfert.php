﻿<?php

class FicheTransfert extends CI_Controller {
private $donnee = array();

    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
			$this->load->model ('Chapitre');
			$this->load->model ('Article');
	   		$this->load->model ('Fiche');
	   		$this->load->model ('Transferts');
			$this->load->model ('Budgetcomp');
			$this->load->model ('Facture');




		}
	
   public function index(){
    }
	function timestampToDate($mon_timestamp) {
		return date('Ymd', $mon_timestamp);
	}
	function getNewDate($ma_date, $decalage) {
		return  $ma_date + ($decalage * 3600 * 24);
	}
	public function imprimerFT()
    {
		$idTransfert=$_GET['i']; 
		
		$transfert=$this->Transferts->consulter();
			$this->donnee['nom'] = 'TRANSFERT';
	foreach($transfert as $t){
		if(($t->id)==$idTransfert){
			$this->donnee['id_chapitre'] =$t->chapitre_cible;
			$this->donnee['id_article'] =$t->article_cible;
			$this->donnee['mOperation'] = $t->montant_transfert;
			$section=$this->Chapitre->consulterunchapitre($t->chapitre_source);
			$this->donnee['section']=$section[0]->sect;
			$this->donnee['chap_source'] = $t->chapitre_source;
			$this->donnee['art_source'] = $t->article_source;
			$this->donnee['montantL']= doubleToLetters((double)$t->montant_transfert);
			$get=$this->Article->budget($t->article_cible,$t->chapitre_cible);
			foreach($get as $raw){
	$this->donnee['nomAc']=$raw->nom;
	}
	$a=$this->Article->budget($t->article_source,$t->chapitre_source);
	foreach($a as $raw){
		$this->donnee['nomAs']=$raw->nom;
	}
	}


            $budgetcomp=0;
            $budgetcomplementaire=$this->Budgetcomp->selectsem($t->article_cible,$t->chapitre_cible,$t->date_transfert);
            foreach($budgetcomplementaire as $row)
            {
                $budgetcomp+=$row->montant;
            }
            $transs=0;
            $transfertsource=$this->Transferts->transfertsourcesem($t->article_cible,$t->chapitre_cible,$t->date_transfert);
            foreach($transfertsource as $row)
            {
                $transs+=$row->montant_transfert;
            }
            $transc=0;
		$date = $this->timestampToDate($this->getNewDate($t->date_transfert, -1));

		$transfertcible=$this->Transferts->transfertciblesem($t->article_cible,$t->chapitre_cible,$date);
            foreach($transfertcible as $row)
            {
                $transc+=$row->montant_transfert;
            }
            $paiement=0;
            $paiementfacture=$this->Facture->facturesem($t->article_cible,$t->chapitre_cible,$t->date_transfert);
            foreach($paiementfacture as $row)
            {
                $paiement+=$row->montant;
            }

           $definitif=$get[0]->budget_initial+$budgetcomp-$transs+$transc-$paiement;

		$this->donnee['budget_initial']=$definitif;
		
	}	

	$this->donnee['newBudget']=$this->donnee['budget_initial']+$this->donnee['mOperation'];
	
	$recup=$this->Fiche->consulter();
	foreach($recup as $raw){
		foreach($transfert as $t){
			if(($raw->nom)=='FICHE DE TRANSFERT'){
		
			if((($raw->id_transfert)==$t->id)&&(($t->montant_transfert)==$this->donnee['mOperation'])){
				$this->donnee['annee'] = $raw->annee;
				$this->donnee['nFiche']=$raw->numero;}}
	}}
        $html=$this->load->view('fiches/fiche_transfert',$this->donnee, true);
        $pdfFilePath = "Fiche de transfert.pdf";
        $this->load->library('M_pdf');
		$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	    $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;
    
}

	}
	include('nel.php');
?>
