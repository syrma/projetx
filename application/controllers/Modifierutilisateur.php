<?php
class Modifierutilisateur extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if (!isset($mail)) {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this->session->set_flashdata('errauth', $data);
            redirect('test', 'refresh');
        }
        $this->load->model('Utilisateurs');
    }

    public function index()
    {
        $id = $_GET["id"];
        $tableau['utilisateur'] = $this->Utilisateurs->consulter($id);
        $tableau['id'] = $id;
        $this->load->view('modifier_utilisateur', $tableau);
    }
    public function modifier()
    {
            $id=$_GET["id"];
            $nom=$_POST["nom"];
            $prenom=$_POST["prenom"];
            $categorie=$_POST["ctg"];
        if(trim($nom)==""||trim($prenom)==""||trim($categorie)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
            );
            $this->session->set_flashdata('msg', $data);
            $url='modifierutilisateur?id='.$id;
            redirect($url, 'refresh');
        }
        else
        {
            $this->Utilisateurs->modifier($id,$nom,$prenom,$categorie);
            if($id==$this->session->userdata('email'))
            { $sessionData = array(
                'categorie' => $categorie
            );

            $this->session->set_userdata($sessionData);}
            $data = array(
                'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez modifié un utilisateur.</div>'
            );
            $this->session->set_flashdata('msg', $data);
            $url='modifierutilisateur?id='.$id;
            redirect($url, 'refresh');
    }}
}
?>
