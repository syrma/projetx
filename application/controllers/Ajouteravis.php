<?php

class Ajouteravis extends CI_Controller
{

    function __construct()
    {
        parent::__construct();


        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Fournisseur');
        $this->load->model ('Facture');
        $this->load->model ('Chapitre');
        $this->load->model ('Article');
        $this->load->model ('Fiche');
        $this->load->model ('Etat');

    }

    public function index(){
        $tableauu['recordsc']=$this->Chapitre->consulter();

        if($tableauu['recordsc']!=null)
        { $tableauu['recordsa'] =$this->Article->liste($tableauu['recordsc'][0]->id);}else{$tableauu['recordsa']=null;}

        if($tableauu['recordsc']!=null || $tableauu['recordsa']!=null)
        {
            $tableauu['recordss']=$this->Fournisseur->idfournisseur();
            $tableauu['recordsp']=$this->Fournisseur->idpersonnes();
            $this->load->view('ajouter_avis_virement',$tableauu);
        }
        else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Il n\'existe aucun article !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('ajouteravis','refresh');
        }



    }
    public function ajouter()

    {

        $tableau= $this->Etat->verifier();
        if($tableau==null)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord le RIB de l\'université !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('parametres','refresh');
        }
        else
        {
            if($tableau[0]->budget_initial==0)
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer le budget initial !!</div>'
                );
                $this-> session-> set_flashdata('msg',$data);
                redirect('engagement','refresh');
            }
        }
        // récupération des champs depuis le formulaire
        $id_chapitre=$_POST["chapitre"];
        $id = $_POST["NFacture"];
        $id_article= $_POST["article"];
        $montant= $_POST["montant"];
        $date=$_POST["startDate"];
        $tableau=$this->Fournisseur->idfournisseur();
        if($tableau!=null)
        {   $id_societe = $_POST["soc"];
            $autre=$_POST ["autresoc"];
        }
        else
        {$id_societe="autre";
            $autre="autre";
        }
        $nom_societe=$_POST["societe"];
        $adr= $_POST["adrS"];
        $nom_banque= $_POST["nomB"];
        $numero_banque= $_POST["compte"];

        $type=$_POST["options"]  ;
        $nummandat=$_POST["mendat"];
        $rib=$_POST["rib"];
        $fiche=$_POST["opt"]  ;
        $budg=0;
        //tester si les champs sont vide au cas ou une société exustante a été seléctionnée
        if(trim($id_chapitre)==""||trim($id)==""||trim($id_article)==""||$montant<=0||trim($id_societe)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs  !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('ajouterfacture','refresh');
        }
        else{
            $budget = $this->Article->budget($id_article, $id_chapitre);
            //effectuer l'opération ancien budget - montant de la facture
            foreach ($budget as $row) {
                $budg = $row->budget - $montant;
            }
            //tester si le budget est suffisant
            if ($budg <= 0)
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Budget insuffisant !!</div>'
                );
                $this->session->set_flashdata('msg', $data);
                redirect('ajouterfacture', 'refresh');
            }
            if($autre=="autre")
            {
                //tester si les champs sont vides si on a choisis d'entrer une nouvelle société
                if (trim($adr) != "" && trim($nom_banque) != "" && trim($numero_banque) != "" && trim($nom_societe) != "")
                {
                    if ($type == "societe")
                    {
                        $typefournisseur = 1;
                    }
                    else
                    {
                        $typefournisseur = 0;
                    }
                    if ($fiche == "mandat")
                    {
                        if ( $typefournisseur==1&&(trim($rib) == "" ||strlen($rib)!=20||!(float)$rib|| trim($nummandat) == ""))
                        {
                            $data = array(
                                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
                            );
                            $this->session->set_flashdata('msg', $data);
                            redirect('ajouterfacture', 'refresh');
                        }
                        else
                        {
                            $id_societe = $this->Fournisseur->ajouter_rib($nom_societe, $adr, $nom_banque, $numero_banque, $typefournisseur, $rib);

                        }
                    }
                    else
                    {
                        $id_societe = $this->Fournisseur->ajouter($nom_societe, $adr, $nom_banque, $numero_banque, $typefournisseur);
                    }

                }
                else
                {
                    $data = array(
                        'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs  !!</div>'
                    );
                    $this->session->set_flashdata('msg', $data);
                    redirect('ajouterfacture', 'refresh');
                }
            }
            if($typefournisseur==1&&$fiche=="mandat")
            {

                if($typefournisseur==1&&(trim($rib)==""||strlen($rib)!=20||!(float)$rib||trim($nummandat)==""))
                {
                    $data = array(
                        'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
                    );
                    $this-> session-> set_flashdata('msg',$data);
                    redirect('facturee','refresh');
                }
                else
                {

                    $auteur = $this->session->userdata('email');
                    $identifiant=$this->Facture->ajouter($id, $id_chapitre, $id_article, $montant, $id_societe, $date, $auteur);

                    $this->Article->retrait($id_chapitre, $id_article, $budg);

                    $tab=$this->Etat->verifier();

                    $nFact=$this->Facture-> getFacture();
                    foreach($nFact as $l){
                        if (($l->n_facture)==$id){
                            $this->Fiche->ajouter("MANDAT DE PAIEMENT",$tab[0]->semestre,$nummandat,$id_chapitre,$id_article,$l->id,$date);
                            $this->Fiche->ajouter("AVIS DE VIREMENT",$tab[0]->semestre,$nummandat,$id_chapitre,$id_article,$l->id,$date);
                        }
                    }


                    $data = array(
                        'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez ajouté une facture.</div>'
                    );
                    $this->session->set_flashdata('msg', $data);
                    //redirect('facturee', 'refresh');


                }
            }

            else {
                //ajouter une facture et retirer son montant du budget de l'article
                $auteur = $this->session->userdata('email');
                $this->Facture->ajouter($id, $id_chapitre, $id_article, $montant, $id_societe,$date, $auteur);
                $this->Article->retrait($id_chapitre, $id_article, $budg);
                $tab=$this->Etat->verifier();
                //ajouter ch50
                $nFact=$this->Facture-> getFacture();
                foreach($nFact as $l){
                    if (($l->n_facture)==$id){
                        $this->Fiche->ajouter("CH50",$tab[0]->semestre,33,$id_chapitre,$id_article,$l->id);
                        //afficher ch50
                        $this->load->library('session');

                        $getFournisseur=$this->Fournisseur->consulter_id($id_societe);
                        foreach($getFournisseur as $g){
                            $nomFournisseur=$g->nom;
                            $Ncompte=$g->numero_banque;}

                        $this->session->set_userdata(array(
                            'montant'=>$montant,
                            'nom_societe'=> $nomFournisseur,
                            'numero_banque'=>$Ncompte,
                        ));
                        redirect('CH50/imprimerCH');
                    }
                }

                $data = array(
                    'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez ajouté une facture.</div>'
                );
                $this->session->set_flashdata('msg', $data);
                // redirect('facturee', 'refresh');


            }
        }


    }

}

?>
