<?php

class Ajouterfournisseur extends CI_Controller{
function __construct()
{
    parent::__construct();
    $mail = $this->session->userdata('email');
    if(!isset($mail)){
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
        );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');
    }

    $this->load->model ('Fournisseur');


}
public function index(){
    $data = array(
        'msg' => ''
    );
    $this->load->view('ajouter_Fournisseur',$data);
}
 public function ajouter(){

    $nom_societe=$_POST["nom"];
    $adr= $_POST["adresse"];
    $nom_banque= $_POST["nomb"];
    $numero_banque= $_POST["numb"];

    if (trim($adr )!="" &&trim( $nom_banque)!="" && trim($numero_banque) != "" && trim( $nom_societe)!="")
    {

        $this->Fournisseur->ajouter( $nom_societe,$adr,$nom_banque,$numero_banque);
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous avez ajouté un fournisseur.</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        redirect('ajouterfournisseur','refresh');

    }else{

        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
       redirect('ajouterfournisseur','refresh');
    }
}}
?>
