<?php

class Ajouterchapitre extends CI_Controller {

    function __construct()
        {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
    }
            $this->load->model('Etat');
        }  
    
    public function index(){
        $this->load->view('ajouter_chapitre');
    }


    public function ajouter(){
        $tableau= $this->Etat->verifier();
        if($tableau==null)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord le RIB de l\'université !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('parametres','refresh');
        }
        else
        {
            if($tableau[0]->budget_initial==0)
            {
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer le budget initial !!</div>'
                );
                $this-> session-> set_flashdata('msg',$data);
                redirect('engagement','refresh');
            }
        }

        $nom = $_POST["nom"];
        $id = $_POST["id"];
        $sec = $_POST["sec"];
        $this->load->model ('Chapitre');
        if(trim($nom)==""||trim($id)==""||trim($sec)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('ajouterchapitre','refresh');
        }
        else{
        $tableau=$this->Chapitre->verifier($id);
        $b=true;
        foreach ($tableau as $row)
        {
            $b=false;


        }
        if($b)
        {
            $this->Chapitre->ajouter($nom,$id,$sec);

        $data = array(
            'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez ajouté un chapitre.</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        redirect('ajouterchapitre','refresh');
    }
    else{
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Ce code existe déjà !</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        redirect('ajouterchapitre','refresh');
    }

    }}
}
?>
