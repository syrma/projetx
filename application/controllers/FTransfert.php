﻿<?php

class  FTransfert extends CI_Controller {
private $donnee = array();

    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
	    $this->load->model ('Transferts');
	   

        } 
	
   public function index(){
	    $tableau['record']=$this->Transferts->consulter();
        $this->load->view('ViewTransfert',$tableau);
    }
	
}
?>
