<?php

class Transfert {

    public function __construct($row, $articlecible, $chapitrecible, $articlesource, $chapitresource,$nom,$prenom){

        $this->article_source=$articlesource[0]->nom;
        $this->chapitre_source=$chapitresource[0]->nom;
        $this->article_cible=$articlecible[0]->nom;
        $this->chapitre_cible=$chapitrecible[0]->nom;
        $this->montant_transfert=$row->montant_transfert;
        $this->date_transfert=$row->date_transfert;
        $this->nom=$nom;
        $this->prenom=$prenom;
    }
    var $article_source;
    var $chapitre_source;
    var $article_cible;
    var $chapitre_cible;
    var $montant_transfert;
    var $date_transfert;
    var $nom;
    var $prenom;
}

?>
