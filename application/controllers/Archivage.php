<?php

class Archivage extends CI_Controller {

   function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');
        } 
        $this->load->helper('file');  
        }  
    
   public function index(){
        
	$archivedir = "./assets/archives";
	$fichiers = scandir($archivedir);
	$archives=array();
	foreach($fichiers as $fich)  
	    if(preg_match("/backup-(\d{1,2})-(\d{1,2})-(\d{4}).sql/", $fich, $matches)){
	           $archives[$matches[3]] = $fich;
	           }
	$data['archives']=$archives;
        $this-> load->view('anneesarch',$data);
   }
}
?>
