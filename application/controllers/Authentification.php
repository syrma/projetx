<?php
/**
 * Created by PhpStorm.
 * User: souad
 * Date: 28/02/2016
 * Time: 20:28
 */
class Authentification extends CI_Controller {

    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(isset($mail)){
          redirect('accueil','refresh');
        } 
        }  

    public function index(){
    
        if(!isset($_POST["username"]) || !isset($_POST["password"]))
          redirect('test','refresh');

        $username = $_POST["username"];
        $password = $_POST["password"] ;

        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Failed</div>'
        );
        
        if(isset($username)&&isset($password)){
            $this->load->model ('Connexion');
            $tableau= $this->Connexion->authentification($username);
            $b=true;
            foreach ($tableau as $row)
            {
                $b=false;
		if(password_verify($password,$row->mdp)&&$row->bloque==0)
        {
		    $sessionData = array(
		               'email' => $row->email,
		               'categorie' => $row->categorie,
                       'nom'=> $row->nom,
                       'prenom'=>$row->prenom
		               );

		    $this->session->set_userdata($sessionData);
                   redirect('accueil','refresh');
        }
        else{
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Failed !!</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
           redirect('test','refresh');
                }
            }
           if($b){

                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Failed !!</div>'
                );
                $this-> session-> set_flashdata('errauth',$data);
                redirect('test','refresh');

            }else{
                $data = array(
                    'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Failed !!</div>'
                );
                $this-> session-> set_flashdata('errauth',$data);
               redirect('test','refresh');
            }
        }
    }
}
?>
