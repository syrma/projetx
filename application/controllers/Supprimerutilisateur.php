<?php
class Supprimerutilisateur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }

        $this->load->model ('Utilisateurs');
    }

    public function index()
    {   $email=$_GET["id"];
        $ctg=$this->session->userdata('categorie');

        $this->Utilisateurs->supprimer_utilisateur($email);
        $tableau ['records']= $this->Utilisateurs->trouver_utilisateurs($ctg);
        $tableau['recordss']= $this->Utilisateurs->trouver_utilisateurs_bloques($ctg);
        $this->load->view('utilisateur',$tableau);
    }


}
?>