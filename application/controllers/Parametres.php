<?php

class Parametres extends CI_Controller {

   function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
            $this->load->model('Etat');
            $this->load->model('Archivage');

        }  
    
   public function index(){
       $tableau['etat']= $this->Etat->verifier();
        $this->load->view('parametres',$tableau);
    }
    public function rib()
    {
        $rib=$_POST["rib"];
        if(trim($rib)!="" && strlen($rib)==20&&(float)$rib)
        {
            $this->Archivage->reinitialiser();
            $this->Etat->rib($rib);
            
            $data = array(
                'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Le RIB de l\'université est :'. $rib.'</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('accueil','refresh');
        }
        else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer une chaine de 20 chiffres !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('parametres','refresh');
        }



    }
    public function finpremier()
    {

        $this->Etat->finpremier();
        $data = array(
            'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous êtes passé au deuxième semestre.</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        redirect('accueil','refresh');
    }
}
?>
