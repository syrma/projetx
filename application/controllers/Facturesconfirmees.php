<?php

class Facturesconfirmees extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous 
authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
         $this->load->model('Facture');
         $this->load->model('Article');
		 $this->load->model('Fournisseur');
		 $this->load->model('Fiche');

    }

    public function index(){
        $tableau=$this->Facture->consulter(1,0);
        $tabb=array();
        foreach($tableau as $row){
            $facture = new Mafacture();
            $facture->id=$row->n_facture;
            $facture->num=$row->id;
            $facture->id_chapitre=$row->id_chapitre;
            $facture->id_article=$row->id_article;
            $facture->date=$row->date_facture;
            $facture->montant=$row->montant;
            $tableauu=$this->Facture->nom_fourn($row->fournisseur);
            foreach($tableauu as $roow){
                $facture->fournisseur=$roow->nom;
            }
            $tabb []=$facture;
        }

        $tableauu['records']=$tabb;
        $this->load->view('factures_confirmees',$tableauu);
    }
    public function valider()
    {
        $id=$_GET["id"];
        $this->Facture->valider($id);
			//afficher les fiches

        $mesfiches=$this->Fiche->consulterunefiche($id);

       if($mesfiches[0]->nom!="CH50")
       {
           $tableau["fiches"]=$mesfiches;
           redirect('fAvisDeVirement','refresh');


       }





		$fiche=$this->Fiche-> consulter();
			foreach($fiche as $l){
				if ((($l->id_facture)==$id)&&(($l->nom)=='CH50')){
					//recuperer le montant de la facture et id fournisseur
					$getFacture=$this->Facture->consulter_facture($id);
					foreach($getFacture as $g){
					$montant=$g->montant;
					$idFournisseur=$g->fournisseur;
				}	
					//recuperer le nom du fournisseur et le compte
					$getFournisseur=$this->Fournisseur->consulter_id($idFournisseur);
					foreach($getFournisseur as $g){
						
					$nomFournisseur=$g->nom;
					$Ncompte=$g->numero_banque;}
				
					$this->session->set_userdata(array(
							'montant'=>$montant,
							'nom_societe'=> $nomFournisseur,
							'numero_banque'=>$Ncompte,
						));
					redirect('CH50/imprimerCH');
				}
				//elseif((($l->id_facture)==$id)&&(($l->nom)=='MANDAT DE PAIEMENT')){
				//	redirect('journal');
				//}
			}
        redirect('facturesconfirmees','refresh');
    }
    public function annuler ()
    {
        $id=$_GET["id"];
        $tableau= $this->Facture->consulter_facture($id);
        $id_article=$tableau[0]->id_article;
        $montant=$tableau[0]->montant;
        $id_chapitre=$tableau[0]->id_chapitre;
        $budget=$this->Article->budget($id_article,$id_chapitre);
        $argent=$budget[0]->budget+$montant;
        $this->Article->retrait($id_article,$id_chapitre,$argent);
        $this->Facture->annuler($id);
        redirect('facturesconfirmees','refresh');
    }
}
class Mafacture {
    var $id;
    var$num;
    var $id_article;
    var $id_chapitre;
    var $date;
    var $montant;
    var $fournisseur;
}
?>
