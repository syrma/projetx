<?php

class Gestionarticles extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
        $this->load->model ('Article');
        $this->load->model ('Chapitre');
    }  
    
    public function index(){

        $id=$_GET["id"];
        $en=$_GET["en"];
        $tableaubudget['records']=$this->Article->budget_article($id);
        $tableaubudget['id']=$id;
        $tableaubudget['chapitree']=$this->Chapitre->consulterunchapitre($id);
        $tableaubudget['en']=$en;
        $this->load->view('gestion_article',$tableaubudget);
    }
    public function supprimer()
    {
        $id_article=$_GET["id"];
        $id_chapitre=$_GET["idc"];
        $tableaubudget=$this->Article->budget($id_article,$id_chapitre);
        if($tableaubudget[0]->budget_initial==0)
        {
            $this->Article->supprimer($id_article,$id_chapitre);
            $url='gestionarticles?id='.$id_chapitre.'&en=0';
            redirect($url,'refresh');
        }
        else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">impossible de supprimer cet article !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            $url='gestionarticles?id='.$id_chapitre.'&en=0';
            redirect($url,'refresh');
        }

    }
}
?>
