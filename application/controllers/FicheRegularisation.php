﻿<?php

class FicheRegularisation extends CI_Controller {
private $donnee = array();
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Chapitre');
       $this->load->model ('Article');
       $this->load->model ('Fiche');
       $this->load->model ('Facture');

        } 
	
    public function index(){
	$tableau['recordsc']=$this->Chapitre->consulter();
        $tableau['recordsa']=$this->Article->consulter();
        $this->load->view('Impression-Reg',$tableau);
    }
    
    public function imprimerFReg(){
        $ida = $_POST['article'];
        $idc = $_POST['chapitre'];
        $this->load->model('Etat');
        $this->load->model('Fournisseur');
        $etat = $this->Etat->verifier();
        if($etat && $etat[0]->semestre == '1')
            redirect('impressions', 'refresh');
            
        $valeurs = array();
        $factures = $this->Facture->cons_sem1($idc, $ida, $etat[0]->date_fin_sem1, $total);
        
        if(!$factures)
            redirect('impressions', 'refresh');
        
        foreach($factures as $facture){
            $fournisseurs = $this->Fournisseur->consulter_id($facture->fournisseur);
            $F = new Mafacture($facture, $fournisseurs);
            $valeurs['factures'][] = $F;        
        }
        
        $article = $this->Article->consulter_article($ida, $idc);
        $chapitre = $this->Chapitre->consulterunchapitre($idc);
        $valeurs['total'] = $total;
        $valeurs['article'] = $ida;
        $valeurs['narticle'] = $article[0]->nom;
        $valeurs['chapitre'] = $idc;
        $valeurs['ancien_solde'] = $article[0]->budget_initial;
        $valeurs['nouveau_solde'] = $valeurs['ancien_solde'] - $valeurs['total'];
        $valeurs['annee'] = $etat[0]->annee;
        $valeurs['nom'] = 'REGULARISATION 1 er SEMESTRE';

        $valeurs['totalLettres'] = doubleToLetters((double) $total);
        $valeurs['section'] = $chapitre[0]->sect;

        $html = $this->load->view('fiches/regularisation',$valeurs, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
	$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
	$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
        exit;

    }

}
include('nel.php');
include('Classes/Mafacture.php');
?>
