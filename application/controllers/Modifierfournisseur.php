<?php

class Modifierfournisseur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Fournisseur');
    }

    public function index()
    {
        $id=$_GET["id"];
        $tableau['fournisseur']=$this->Fournisseur->consulter_id($id);
        $tableau['id']=$id;
        $this->load->view('modifier_fournisseur',$tableau);
    }
    public function modifier()
    {
        $id=$_GET["id"];
        $rib=$_POST["rib"];
        $adr=$_POST["adr"];
        $nomb=$_POST["nomb"];
        $numb=$_POST["numb"];
        $nom=$_POST["nom"];
        $type=$_POST["type"];


        if(trim($adr)==""||trim($nomb)==""||trim($numb)==""||trim($nom)=="")
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">veuillez remplir tous les champs !!</div>'
            );
            $this->session->set_flashdata('msg', $data);
            $url='modifierfournisseur?id='.$id;
            redirect($url, 'refresh');
        }
        else
        {
            if($type=="societe")
            {
                if(trim($rib)=="")
                {
                    $data = array(
                        'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
                    );
                    $this->session->set_flashdata('msg', $data);
                    $url='modifierfournisseur?id='.$id;
                    redirect($url, 'refresh');
                }
                else
                {
                    $this->Fournisseur->modifier($id,$nom,$nomb,$numb,$rib,$adr,1);
                    $data = array(
                        'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez modifié un fournisseur.</div>'
                    );
                    $this->session->set_flashdata('msg', $data);
                    $url='modifierfournisseur?id='.$id;
                    redirect($url, 'refresh');
                }
            }
            $this->Fournisseur->modifier($id,$nom,$nomb,$numb,$rib,$adr,0);
            $data = array(
                'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez modifié un fournisseur.</div>'
            );
            $this->session->set_flashdata('msg', $data);
            $url='modifierfournisseur?id='.$id;
            redirect($url, 'refresh');

        }

    }
}
?>
