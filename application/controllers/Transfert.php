<?php

class Transfert extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier.</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
        $this->load->model('Chapitre');
        $this->load->model('Article');
	$this->load->model('Fiche');
        $this->load->model('Etat');
     }  
    
    public function index(){

        $tableau['chapitres']=$this->Chapitre->consulter();

        if($tableau['chapitres']!=null)
        $tableau['articles']= $this->Article->liste($tableau['chapitres'][0]->id);
        $this->load->view('transfert',$tableau);
    }
public function transferer(){
    $tableau= $this->Etat->verifier();
    if($tableau==null)
    {
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">veuillez entrer d abord le RIB de l universite !!</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        redirect('parametres','refresh');
    }
    else
    {
        if($tableau[0]->budget_initial==0)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">veuillez entrer le budget initial !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('engagement','refresh');
        }
    }
    $montant=str_replace(' ','',$_POST["montant"]);
    $montant = str_replace(',','.',$montant);
    $montant = str_replace('D.A','',$montant);
    $idchapd=$_POST["idcd"];
    $idchapa=$_POST["idca"];
    $idartd=$_POST["idad"];
    $idarta=$_POST["idaa"];
   if(trim($montant)==""){
       $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs.</div>'
       );
       $this-> session-> set_flashdata('msg',$data);
       redirect('transfert','refresh');
   }else{
    $budget=$this->Article->budget($idartd,$idchapd);
    $k= (double)$montant;
    
   foreach($budget as $row){
       $b= $row->budget;
   }
    $montantad=$b-$k;
    if($montantad<0){
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Transfert impossible.</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
       redirect('transfert','refresh');
    }
   else{
       $budget=$this->Article->budget($idarta, $idchapa);
       foreach($budget as $row){
           $b= $row->budget;
       }
       $montantaa=$b+(double)$k;
    $dt = new DateTime();

$dt=date("Y-m-d",time($dt));

    $this->Article->transferer($montant,$idchapd, $idchapa,$idartd, $idarta,$dt,$montantad,$montantaa,$this->session->userdata('email'));

	$get=$this->Fiche->findtrans($idchapa,$idarta);
       $n=0;
       if($get==null){$n=3;}else{
	 foreach($get as $row){

		 $n=$row->numero+1;

		}}

	 $tab=$this->Etat->verifier();
	 $transfert=$this->Article->consulterTransfert();
	
	foreach($transfert as $t){
	$idT=$t->id;}
	
	  $this->Fiche->ajouter("FICHE DE TRANSFERT",$tab[0]->semestre,$n,$idchapa,$idarta,NULL,$idT);
	
       $data = array(
           'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Transfert effectué.</div>'
       );
       $this-> session-> set_flashdata('msg',$data);
     redirect('transfert','refresh');
}}
}}
?>
