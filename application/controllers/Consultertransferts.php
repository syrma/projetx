<?php

class Consultertransferts extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model('Transferts');
        $this->load->model('Chapitre');
        $this->load->model('Article');
        $this->load->model('Utilisateurs');

    }

    public function index(){
        $transfert=$this->Transferts->consulter();
        $table=array();
        if($transfert!=null)
        {
            foreach($transfert as $row)
            {    
                $auteur=$this->Utilisateurs->verifier_utilisateur($row->auteur);
                $nom=$auteur[0]->nom;
                $prenom=$auteur[0]->prenom;
                $articlecible = $this->Article->consulter_article($row->article_cible,$row->chapitre_cible);
                $chapitrecible = $this->Chapitre->consulterunchapitre($row->chapitre_cible);
                $articlesource = $this->Article->consulter_article($row->article_source,$row->chapitre_source);
                $chapitresource = $this->Chapitre->consulterunchapitre($row->chapitre_source);
                $trans=new Transfert($row, $articlecible, $chapitrecible, $articlesource, $chapitresource,$nom,$prenom);
                $table[]=$trans;
            }
            $tableau['transfert']=$table;

        }
        else
        {
            $tableau['transfert']=$transfert;
        }

        $this->load->view('consultation_transfert',$tableau);
    }
}

include "Classes/Transfert.php";

?>
