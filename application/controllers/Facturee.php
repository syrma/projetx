<?php

class Facturee extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model('Facture');
    }

    public function index(){
        $tableau=$this->Facture->consulter(0,0);
        $tabb=array();
        foreach($tableau as $row){
            $tableau2 = $this->Facture->nom_fourn($row->fournisseur);
            $facture = new Mafacture($row, $tableau2);
            $tabb []=$facture;
        }

        $tableauu['records']=$tabb;
        $this->load->view('facture',$tableauu);
    }
    public function confirmer()
    {
        $id=$_GET["id"];
        $this->Facture->confirmer($id);
        redirect('facturee','refresh');


    }

}

include "Classes/Mafacture.php";

?>
