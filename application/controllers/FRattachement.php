﻿<?php

class FRattachement extends CI_Controller {
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Chapitre');
       $this->load->model ('Article');
	

        } 
	
   public function index(){
	    $tableau['recordsc']=$this->Chapitre->consulter();
        $tableau['recordsa']=$this->Article->consulter();
	
        $this->load->view('Impression-A',$tableau);
       
    }


	
}

?>
