<?php
class Fournisseurs extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Fournisseur');
    }

    public function index(){
        $id=$_GET["id"];
        if($id==0)
        {
            $tableau['records']=$this->Fournisseur->consulter();
        }
        else
        {
            $tableau['records']=$this->Fournisseur->consulter_id($id);
        }

        $this->load->view('fournisseur',$tableau);
    }


}
?>