<?php

class Impressionetatpaiement extends CI_Controller {
    private $donnee = array();
    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }

        $this->load->model ('Fournisseur');
        $this->load->model ('Fiche');
        $this->load->model ('Facture');
		$this->load->model('Etat');
    }

    public function index()
    {
        $datedebut = $_POST["startDate"];
        $datefin = $_POST["endDate"];
        $idc = $_POST["chapitre"];
        $id = $_POST["article"];
        $factres = $this->Facture->paiment($id, $idc, $datedebut, $datefin);
        $tab = array();
        $total = 0;
        if($factres !=null){
            foreach ($factres as $row) {

            $mandats = $this->Fiche->numero($row->id);
            if ($mandats != null) {
                $f = new mafacture();
                $f->num = $row->n_facture;
                $f->montant = $row->montant;
                $total += $f->montant;
                $f->datefacture = $row->date_facture;
                $f->datemandat = $row->date_etablissement;
                $fournisseurs = $this->Fournisseur->consulter_id($row->fournisseur);
                $f->fournisseur = $fournisseurs[0]->nom . "  " . $fournisseurs[0]->prenom;
                $f->nummandat = $mandats[0]->numero;
                $tab[] = $f;
            }
        }
    }else{
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Il n\'existe aucun mandat pour cet article !!</div>'
            );
            $this->session->set_flashdata('msg', $data);
            redirect('dateetatpaiement', 'refresh');
        }
		$this->data["annee"]=$this->Etat->verifier();
        $this->data["total"]=$total;
        $this->data["montantL"]=doubleToLetters((double)$total);
        $this->data["paiements"]=$tab;
		$this->data["chap"]=$idc;
		$this->data["art"]=$id;
      // $this->load->view('fiches/EtatDePaiement',$this->data);
       $html=$this->load->view('fiches/EtatDePaiement',$this->data, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
        $this->m_pdf->pdf->autoScriptToLang = true;
        $this->m_pdf->pdf->autoLangToFont = true;
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");

        exit;

    }




}
class mafacture
{

    var$nummandat;
    var $fournisseur;
    var $datemandat;
    var $datefacture;
    var $num;
    var $montant;
}
include('nel.php');
?>
