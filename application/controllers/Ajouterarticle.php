<?php

class Ajouterarticle extends CI_Controller {

    function __construct()
    {
 
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        } 
        $this->load->model ('Chapitre');
        $this->load->model ('Article');
	$this->load->model ('Fiche');
	$this->load->model ('Etat');
    }  
    
    public function index(){
        $tableau['idc']=$_GET['id'];
        $tableau['records']=$this->Chapitre->consulter();
        $this->load->view('ajouter_article',$tableau);
    }

public function ajouter(){
    $tableau= $this->Etat->verifier();
    if($tableau==null)
    {
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord le RIB de l\'université !!</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        redirect('parametres','refresh');
    }
    else
    {
        if($tableau[0]->budget_initial==0)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer le budget initial !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            redirect('engagement','refresh');
        }
    }
    $nom=$_POST["nom"];
    $id=$_GET ["idc"];
    $ida=$_POST ["ida"];
    $budget = str_replace(' ', '',$_POST["budget"]);
    $budget = str_replace(',', '.',$budget);
    $budget = str_replace('D.A','',$budget);
    if(trim($nom)==""||trim($id=="")||trim($ida)==""||$budget<0)
    {
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez remplir tous les champs !!</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        $url='ajouterarticle?id='.$id;
        redirect($url,'refresh');
    }
    else{
    $tableau=$this->Article->verifier($ida);
    $b=true;
    foreach ($tableau as $row)
    {
        $b=false;


    }
    if($b)
    {
	$tab=$this->Etat->verifier();
        if($tab[0]->budget_initial-$budget<0)
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Budget insuffisant !</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            $url='ajouterarticle?id='.$id;
            redirect($url,'refresh');
        }

        else
        {
    $this->Article->ajouter($nom,$id,$budget,$ida);
	$this->Fiche->ajouter("FICHE DE RATTACHEMENT",$tab[0]->semestre,1,trim($id),$ida,NULL,NULL);
	$this->Fiche->ajouter("FICHE D'ENGAGEMENT PROVISIONNEL",$tab[0]->semestre,2,trim($id),$ida,NULL,NULL);
	
    $data = array(
        'msg' => '<div class="alert alert-success col-md-6 col-md-offset-3" role="alert">Vous avez ajouté un article.</div>'
    );
    $this-> session-> set_flashdata('msg',$data);
        $url='ajouterarticle?id='.$id;
        redirect($url,'refresh');
}}
    else {
        $data = array(
            'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Ce code existe déjà !</div>'
        );
        $this-> session-> set_flashdata('msg',$data);
        $url='ajouterarticle?id='.$id;
        redirect($url,'refresh');
    }

}
}
public function supprimer()
{


}


}
?>
