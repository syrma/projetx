<?php

class FicheEngagProvi extends CI_Controller {
    private $donnee = array();
    function __construct()
    {
        parent::__construct();

        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }

        $this->load->model ('Chapitre');
        $this->load->model ('Article');
        $this->load->model ('Fiche');
        $this->load->model ('Etat');
        $this->load->model ('Budgetcomp');
        $this->load->model ('Facture');
        $this->load->model ('Transferts');

    }

    public function index(){
        $this->load->view('Fiches');
    }
    public function F_EProvi(){
        $tableau['recordsc']=$this->Chapitre->consulter();
        $tableau['recordsa']=$this->Article->consulter();
        $tableau['sem']=$_GET["sem"];
        $this->load->view('Impression-EP',$tableau);

    }
    public function imprimerFEP()
    {
        $sem=$_GET["sem"];
        $this->donnee['id_chapitre'] =$_POST["chapitre"];
        $this->donnee['id_article'] =$_POST["article"];
        $id=$_POST["article"];
        $idc=$_POST["chapitre"];
        $chap=$this->Chapitre->consulterunchapitre($idc);
        $this->donnee["section"]=$chap[0]->sect;
        $recup=$this->Fiche->consulterfiche($idc,$id,"FICHE D'ENGAGEMENT PROVISIONNEL");
        if($recup==null)
        {

            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord un budget initial à cet article !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            $url="modifierarticle?id=".$_POST["article"]."&idc=".$_POST["chapitre"];
            redirect($url,'refresh');
        }
        foreach($recup as $raw){

                $this->donnee['annee'] = $raw->annee;
                $this->donnee['nFiche']=$raw->numero;

        }
        if($sem==2)
        {
            $this->donnee['nom'] = 'ENGAGEMENT PROVISIONNEL 2eme semestre';
            $get=$this->Article->budget ($_POST["article"],$_POST["chapitre"]);
            $this->donnee['nomart']=$get[0]->nom;

            $etat=$this->Etat->verifier();
            $date_fin_sem=$etat[0]->date_fin_sem1;
            $budgetcomp=0;
            $budgetcomplementaire=$this->Budgetcomp->selectsem($id,$idc,$date_fin_sem);
            foreach($budgetcomplementaire as $row)
            {
                $budgetcomp+=$row->montant;
            }
            $transs=0;
            $transfertsource=$this->Transferts->transfertsourcesem($id,$idc,$date_fin_sem);
            foreach($transfertsource as $row)
            {
                $transs+=$row->montant_transfert;
            }
            $transc=0;
            $transfertcible=$this->Transferts->transfertciblesem($id,$idc,$date_fin_sem);
            foreach($transfertcible as $row)
            {
                $transc+=$row->montant_transfert;
            }
            $paiement=0;
            $paiementfacture=$this->Facture->facturesem($id,$idc,$date_fin_sem);
            foreach($paiementfacture as $row)
            {
                $paiement+=$row->montant;
            }

           $definitif=$get[0]->budget_initial+$budgetcomp-$transs+$transc-$paiement;
                $this->donnee['budget_initial'] = $definitif;
                $this->donnee['montantL']= doubleToLetters((double) $definitif);

        }
        else {
            $this->donnee['nom'] = 'ENGAGEMENT PROVISIONNEL 1er semestre';
            $get=$this->Article->budget ($_POST["article"],$_POST["chapitre"]);
            $this->donnee['nomart']=$get[0]->nom;

            foreach($get as $raw){
                $this->donnee['budget_initial'] = $raw->budget_initial;
                $this->donnee['montantL']= doubleToLetters((double) $raw->budget_initial);
            } }


        //s'il y a des transferts
        $get=$this->Article->consulter_transferts_cible($_POST["article"],$_POST["chapitre"]);
        if($get==null){
            $this->donnee['chap_source'] =null;
            $this->donnee['art_source'] =null;
        }
        foreach($get as $raw){
            $this->donnee['mOperation'] = $raw->montant_transfert;
            $this->donnee['chap_source'] = $raw->chapitre_source;
            $this->donnee['art_source'] = $raw->article_source;

        }
      // $this->load->view('fiches/engagement_provisionnel',$this->donnee);
        $html=$this->load->view('fiches/engagement_provisionnel',$this->donnee, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
        $this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");

        exit;
    }

}
include('nel.php');
?>
