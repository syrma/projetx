﻿<?php

class CH50 extends CI_Controller {
private $donnee = array();
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Facture');
	   $this->load->model ('Fournisseur');
	 
        } 
	
   public function index(){ 
   $this->donnee['mFacture'] ='';
		$this->donnee['fournisseur']= '';
		$this->donnee['nCompte']= '';

   
		$html=$this->load->view('fiches/ch_50',$this->donnee, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
		$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
	    $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;   
    }
  public function imprimerCH()
    {

		$this->donnee['mFacture'] = $this->session->userdata('montant');
		$this->donnee['fournisseur']= $this->session->userdata('nom_societe');
		$this->donnee['nCompte']= $this->session->userdata('numero_banque');

		$html=$this->load->view('fiches/ch_50',$this->donnee, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
		$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	    $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;
    }
	
}

?>
