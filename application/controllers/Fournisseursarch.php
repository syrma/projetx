<?php
class Fournisseursarch extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
            );
            $this-> session-> set_flashdata('errauth',$data);
            redirect('test','refresh');
        }
        $this->load->model ('Archivage');
    }

    public function index(){
        $id=$_GET["id"];
        if($id==0)
        {
            $tableau['records']=$this->Archivage->consulterFournisseur();
        }
        else
        {
            $tableau['records']=$this->Archivage->consFournisseur($id);
        }
        $tableau['arch'] = 1;
        $this->load->view('fournisseur',$tableau);
    }
}
?>
