﻿<?php

class FicheRattachement extends CI_Controller {
private $donnee = array();
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Chapitre');
       $this->load->model ('Article');
	   $this->load->model ('Fiche');

        } 
	
   public function index(){
        $this->load->view('Fiches');
    }

	public function imprimerFR()
    {
	$this->donnee['nom'] = 'RATTACHEMENT';
    $this->donnee['id_chapitre'] =$_POST["chapitre"];
	$this->donnee['id_article'] =$_POST["article"];
        $id=$_POST["article"];
        $idc=$_POST["chapitre"];
        $chap=$this->Chapitre->consulterunchapitre($_POST["chapitre"]);
        $this->donnee["section"]=$chap[0]->sect;
	$get=$this->Article->budget ($_POST["article"],$_POST["chapitre"]);
    $this->donnee['nomart']=$get[0]->nom;
     if($get[0]->budget_initial!=0){
         foreach($get as $raw){
             $this->donnee['budget_initial'] = $raw->budget_initial;
             $this->donnee['montantL']= doubleToLetters((double) $raw->budget_initial);
         }

        $recup=$this->Fiche->consulterfiche($idc,$id,"FICHE DE RATTACHEMENT");
        if($recup==null)
        {

            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord un budget initial à cet article !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            $url="modifierarticle?id=".$_POST["article"]."&idc=".$_POST["chapitre"];
            redirect($url,'refresh');
        }
        foreach($recup as $raw){

            $this->donnee['annee'] = $raw->annee;
            $this->donnee['nFiche']=$raw->numero;

        }

	
        $html=$this->load->view('fiches/rattachement',$this->donnee, true);
        $pdfFilePath = "Fiche de rattachement.pdf";
        $this->load->library('M_pdf');
		$this->m_pdf->pdf->AddPage('L', '', '', '', '',10,10,10,10,0,0);
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	    $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;
    }else
        {
            $data = array(
                'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Veuillez entrer d\'abord un budget initial à cet article !!</div>'
            );
            $this-> session-> set_flashdata('msg',$data);
            $url="modifierarticle?id=".$_POST["article"]."&idc=".$_POST["chapitre"];
            redirect($url,'refresh');
        }}
	
}
include('nel.php');
?>
