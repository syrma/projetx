﻿<?php

class FMandat extends CI_Controller {
	private $donnee = array();
    function __construct()
        {
        parent::__construct();
        
        $mail = $this->session->userdata('email');
        if(!isset($mail)){
           $data = array(
           'msg' => '<div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">Vous devez vous authentifier</div>'
           );
        $this-> session-> set_flashdata('errauth',$data);
        redirect('test','refresh');  
        }
 
       $this->load->model ('Facture');
       $this->load->model ('Fournisseur');
	   $this->load->model ('Fiche');
	   $this->load->model('Article');

        } 
	
   public function index(){

	$startDate=$_POST["startDate"];
	$endDate=$_POST["endDate"];
	$factures=$this->Fiche->mandatsBetweenDates($startDate,$endDate);
        $html = "";
        foreach($factures as $facture){
            $html .= $this->remplirM($facture->id_facture);
            echo $facture->id_facture;
        }
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
	$this->m_pdf->pdf->autoScriptToLang = true;
	$this->m_pdf->pdf->autoLangToFont = true;
	$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
	exit;
    }
    
    public function mandatVide(){
    $html=$this->load->view('fiches/mandatV',$valeurs, true);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
	$this->m_pdf->pdf->autoScriptToLang = true;
	$this->m_pdf->pdf->autoLangToFont = true;
	$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;
    }
    
    public function imprimerMand(){
        $idf = $_GET['id'];
        $html = $this->remplirM($idf);
        $pdfFilePath = "fiche.pdf";
        $this->load->library('M_pdf');
        $this->m_pdf->pdf->autoScriptToLang = true;
        $this->m_pdf->pdf->autoLangToFont = true;
        $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
		
		exit;
    }
    
    public function remplirM($idf){
        $this->load->model('Article');
        $this->load->model('Facture');
        $this->load->model('Fournisseur');
        $this->load->model('Fiche');
        $this->load->model('Etat');
        $valeurs = array();
        $numeromandat = $this->Fiche->nummandat($idf);
        if(!$numeromandat)
            return "";
        $valeurs['mandat'] = $numeromandat->numero;
        $facture = $this->Facture->consulter_facture($idf)[0];
        $idc = $facture->id_chapitre;
        $ida = $facture->id_article;
        $valeurs['chapitre'] = $idc;
        $valeurs['article'] = $ida;
        $article = $this->Article->consulter_article($ida, $idc)[0];
        $valeurs['articlen'] = $article->nom;
        $valeurs['montantf'] = $facture->montant;
        $fournisseur = $this->Fournisseur->consulter_id($facture->fournisseur)[0];
        $valeurs['nomfournisseur'] = $fournisseur->nom;
        $valeurs['prenomfournisseur'] = $fournisseur->prenom;
        $valeurs['adressef'] = $fournisseur->adr;
        $valeurs['banquef'] = $fournisseur->nom_banque;
        $valeurs['ribf'] = $fournisseur->rib_fournisseur;
        $valeurs['n_fact'] = $facture->n_facture;
        $valeurs['datef'] = $facture->date_facture;
        $valeurs['montantl'] = doubleToLetters((double) $valeurs['montantf']);
        $valeurs['annee'] = $this->Etat->verifier()[0]->annee;
        
        return $this->load->view('fiches/mandatV2',$valeurs, true);

    }


	
}
include('nel.php');
?>
