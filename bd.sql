﻿USE SYST_FINANCES;

DROP TABLE IF EXISTS Etat, Fiche, Transfert, Facture, Fournisseur, Permissions, Utilisateur, QuestionS, Banque, BudgetComp, Article, Chapitre;

CREATE TABLE Chapitre(
       id VARCHAR(8) NOT NULL ,
       nom VARCHAR(150) NOT NULL,
       nom_arabe VARCHAR(150) NOT NULL,
       sect INT(1) NOT NULL,
       PRIMARY KEY(id)
       );

CREATE TABLE QuestionS(
       id INT NOT NULL AUTO_INCREMENT,
       enonce VARCHAR(100) NOT NULL,
       PRIMARY KEY(id)
       );

CREATE TABLE Utilisateur(
       nom VARCHAR(25) NOT NULL,
       prenom VARCHAR(25) NOT NULL,
       email VARCHAR(50) NOT NULL,
       mdp VARCHAR(200) NOT NULL,
       categorie VARCHAR(20) NOT NULL,
       questionS INT NOT NULL,
       reponseS VARCHAR(200) NOT NULL,
       bloque TINYINT(1) NOT NULL,
       PRIMARY KEY(email),
       CONSTRAINT questionS FOREIGN KEY(questionS) REFERENCES QuestionS(id) ON DELETE CASCADE ON UPDATE CASCADE
       );

CREATE TABLE Article(
       id VARCHAR(8) NOT NULL,
       nom VARCHAR(50) NOT NULL,
       nom_arabe VARCHAR(50) NOT NULL,
       id_chapitre VARCHAR(8) NOT NULL,
       budget DOUBLE NOT NULL,
       budget_initial DOUBLE NOT NULL,
       PRIMARY KEY(id, id_chapitre),
       CONSTRAINT chapitre FOREIGN KEY(id_chapitre) REFERENCES Chapitre(id) ON DELETE CASCADE ON UPDATE CASCADE
       );

CREATE TABLE BudgetComp(
       id INT NOT NULL AUTO_INCREMENT,
       auteur VARCHAR(50) NOT NULL,
       id_article VARCHAR(8) NOT NULL,
       id_chapitre VARCHAR(8) NOT NULL,
       montant DOUBLE NOT NULL,
       date_transaction DATE NOT NULL,
       PRIMARY KEY(id),
       CONSTRAINT art1 FOREIGN KEY(id_chapitre, id_article) REFERENCES Article(id_chapitre, id) ON DELETE CASCADE ON UPDATE CASCADE
       );
       
       CREATE TABLE Etat(
       annee YEAR NOT NULL,
       semestre INT(1) NOT NULL,
       budget_initial DOUBLE,
       rib_univ VARCHAR(20),
       date_fin_sem1 DATE,
       date_fin_annee DATE,
       PRIMARY KEY(annee)
       );

CREATE TABLE Fournisseur(
       id INT NOT NULL AUTO_INCREMENT,
       nom VARCHAR(100) NOT NULL,
       prenom VARCHAR(100),
       adr VARCHAR(100) NOT NULL,
       nom_banque VARCHAR(50) NOT NULL,
       numero_banque INT NOT NULL,
       rib_fournisseur VARCHAR(20),
       type_fournisseur BOOLEAN NOT NULL,
       PRIMARY KEY(id)
       );

CREATE TABLE Facture(
       id INT NOT NULL AUTO_INCREMENT,
       n_facture VARCHAR(8) NOT NULL,
       id_chapitre VARCHAR(8) NOT NULL,
       id_article VARCHAR(8) NOT NULL,
       auteur VARCHAR(50) NOT NULL,
       date_facture DATE NOT NULL,
       date_etablissement DATE NOT NULL,
       montant DOUBLE NOT NULL,
       fournisseur INT NOT NULL,
       confirme TINYINT (1) NOT NULL,
       valide TINYINT (1) NOT NULL,
       PRIMARY KEY(id),
       CONSTRAINT fournisseur FOREIGN KEY(fournisseur) REFERENCES Fournisseur(id) ,
       CONSTRAINT article FOREIGN KEY(id_article, id_chapitre) REFERENCES Article(id, id_chapitre) ON DELETE CASCADE ON UPDATE CASCADE,
       CONSTRAINT auteur FOREIGN KEY(auteur) REFERENCES Utilisateur(email)
       );
       
CREATE TABLE Transfert(
       id INT NOT NULL AUTO_INCREMENT,
       chapitre_source VARCHAR(8) NOT NULL,
       article_source VARCHAR(8) NOT NULL,
       chapitre_cible VARCHAR(8) NOT NULL,
       article_cible VARCHAR(8) NOT NULL,
       date_transfert DATE NOT NULL,
       montant_transfert DOUBLE NOT NULL,
       auteur VARCHAR(50) NOT NULL,
       PRIMARY KEY(id),
       CONSTRAINT article4 FOREIGN KEY(auteur) REFERENCES Utilisateur(email) ON DELETE CASCADE ON UPDATE CASCADE,
       CONSTRAINT article2 FOREIGN KEY(article_source,chapitre_source) REFERENCES Article(id,id_chapitre) ON DELETE CASCADE ON UPDATE CASCADE,
       CONSTRAINT article3 FOREIGN KEY(article_cible,chapitre_cible) REFERENCES Article(id,id_chapitre) ON DELETE CASCADE ON UPDATE CASCADE
       );

CREATE TABLE Fiche(
       id INT NOT NULL AUTO_INCREMENT,
       nom VARCHAR(50) NOT NULL,
       semestre enum('1','2'),
       numero INT,
       annee YEAR(4) NOT NULL,
       id_chapitre VARCHAR(8) NOT NULL,
       id_article VARCHAR(8) NOT NULL,
       id_facture INT,
       id_transfert INT,
       PRIMARY KEY(id),
       CONSTRAINT article1 FOREIGN KEY(id_article, id_chapitre) REFERENCES Article(id, id_chapitre) ,
       CONSTRAINT facture FOREIGN KEY(id_facture) REFERENCES Facture(id) 
       
       );


INSERT INTO QuestionS(enonce) VALUES('question');
INSERT INTO Utilisateur VALUES('Sahnoun','Zaidi','zaidi.sahnoun@univ-constantine2.dz','$2y$10$l/4cpAz/JP.ka0iRkwTGyOVL1304edac7GUYjgW6aM4vQpD4DpNnu','Doyen',1,'mareponse',0);
INSERT INTO Utilisateur VALUES('Responsable','Responsable','res','$2y$10$l/4cpAz/JP.ka0iRkwTGyOVL1304edac7GUYjgW6aM4vQpD4DpNnu','Responsable',1,'mareponse',0);
INSERT INTO Chapitre VALUES('22/05','CHARGE SOCIALE ET FISCALE','',1);
INSERT INTO Chapitre VALUES('22/08','REMUNERATION DES ENSEIGNANTS (vacataires, associes, et invites discrets Ex  N° 01-293 et 01-294 et 01-295 du 01/10/2001)','',1);
INSERT INTO Chapitre VALUES('22/11','REMBOURSEMENT DES FRAIS','',2);
INSERT INTO Chapitre VALUES('22/12','MATERIEL ET MOBILIER DU BUREAU','',2);
INSERT INTO Chapitre VALUES('22/13','FOURNITURES','',2);
INSERT INTO Chapitre VALUES('22/14','DOCUMENTAITION','',2);
INSERT INTO Chapitre VALUES('22/15','CHARGES ANNEXES','',2);
INSERT INTO Chapitre VALUES('22/18','les travaux d\'entretien','',2);
INSERT INTO Chapitre VALUES('22/19','frais de formation, stage de courte durée à l\'étranger','',2);
INSERT INTO Chapitre VALUES('22/21','MATERIEL, FOURNITURES INFORMATIQUES','',2);
INSERT INTO Chapitre VALUES('22/22','MATERIEL ET MOBILIER DE LA PEDAGOGIE','',2);
INSERT INTO Chapitre VALUES('22/23','FRAIS LIES AUX ETUDES POST-GRADUES','',2);
INSERT INTO Chapitre VALUES('22/25','les dépenses de l\'organisation des manifestations scientifiques et techniques (séminaires, conférences, formus, journées d\'études)','',2);
INSERT INTO Chapitre VALUES('22/30','STAGE PROFESSIONEL DES ETUDIANTS','',2);
INSERT INTO Article VALUES('01','sécurité sociale 25%.......','','22/05',0,0);
INSERT INTO Article VALUES('01','Rémunération des enseignants vacataires','','22/08',0,0);
INSERT INTO Article VALUES('02','rémunération des enseignants associés','','22/08',0,0);
INSERT INTO Article VALUES('03','Rémunération des enseignants invités','','22/08',0,0);
INSERT INTO Article VALUES('01/1','Déplacement et missions en Algérie','','22/11',0,0);
INSERT INTO Article VALUES('01/2','Déplacement et missions à l\'étranger','','22/11',0,0);
INSERT INTO Article VALUES('02','Frais de réception','','22/11',0,0);
INSERT INTO Article VALUES('06','Dépenses coopération universitaire','','22/11',0,0);
INSERT INTO Article VALUES('01','Acquisition du matériel et mobilier de bureau','','22/12',0,0);
INSERT INTO Article VALUES('02','Entretion et réparation du Matériel et Mobilier de Bureau','','22/12',0,0);
INSERT INTO Article VALUES('03','Acquisition du matériel de prévention et de sécurité ','','22/12',0,0);
INSERT INTO Article VALUES('04','Entretien et réparation du matériel de prévention et de sécurité ','','22/12',0,0);
INSERT INTO Article VALUES('06','Entretien et réparation du matériel audiovisuel','','22/12',0,0);
INSERT INTO Article VALUES('08','Entretien et réparation du matériel de Reprographie et d\'imprimerie','','22/12',0,0);
INSERT INTO Article VALUES('01','Papetrie, fournitures de bureau','','22/13',0,0);
INSERT INTO Article VALUES('02','produits d\'entretien','','22/13',0,0);
INSERT INTO Article VALUES('05','frais de reluire et d\'imprimerie (Revue...)','','22/13',0,0);
INSERT INTO Article VALUES('08','papetrie, fournitures d\'enseignement','','22/13',0,0);
INSERT INTO Article VALUES('02','Ouvrages de bibliothèque','','22/14',0,0);
INSERT INTO Article VALUES('03','Abonnements scientifiques','','22/14',0,0);
INSERT INTO Article VALUES('02','FRAIS DES P,T,T','','22/15',0,0);
INSERT INTO Article VALUES('05','Frais de justice honoraires expertises','','22/15',0,0);
INSERT INTO Article VALUES('06','impôts et taxes diverses','','22/15',0,0);
INSERT INTO Article VALUES('07','publication et publicité','','22/15',0,0);
INSERT INTO Article VALUES('08','frais d\'internet','','22/15',0,0);
INSERT INTO Article VALUES('01','entretien et réparation des bâtiments administratifs et pédagogiques','','22/18',0,0);
INSERT INTO Article VALUES('02','entretien des espaces verts','','22/18',0,0);
INSERT INTO Article VALUES('03','dépenses d\'hygiène','','22/18',0,0);
INSERT INTO Article VALUES('01','Frais des stages par fonctionnement à l\'étranger','','22/19',0,0);
INSERT INTO Article VALUES('02','les frais des séjours scientifiques des hauts niveaux au courte durée','','22/19',0,0);
INSERT INTO Article VALUES('03','frais des participations aux manifestations scientifiques','','22/19',0,0);
INSERT INTO Article VALUES('04','Frais de visas et d\'assurance','','22/19',0,0);
INSERT INTO Article VALUES('01','Acquisition de matériel informatique','','22/21',0,0);
INSERT INTO Article VALUES('02','Fournitures informatiques et logiciels','','22/21',0,0);
INSERT INTO Article VALUES('01','Acquisition de matériel et mobilier pédagogique','','22/22',0,0);
INSERT INTO Article VALUES('03','Entretion et réparation du matériel pédagigique','','22/22',0,0);
INSERT INTO Article VALUES('03','logiciels spécialisés','','22/23',0,0);
INSERT INTO Article VALUES('04','Matériel et fournitures formation post-graduation','','22/23',0,0);
INSERT INTO Article VALUES('08','frais de transport hébergement des membres du jury des soutenances','','22/23',0,0);
INSERT INTO Article VALUES('09','Frais du doctorat','','22/23',0,0);
INSERT INTO Article VALUES('02','Hébergement et réstauration','','22/25',0,0);
INSERT INTO Article VALUES('03','transport','','22/25',0,0);
INSERT INTO Article VALUES('04','soutien logistique (photocopie, badges...)','','22/25',0,0);
INSERT INTO Article VALUES('01','prime quotidienne','','22/30',0,0);
INSERT INTO Article VALUES('02','Transport et déplacement','','22/30',0,0);

